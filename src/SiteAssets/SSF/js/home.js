function getMemoCards() {
    validateDMemo();
  if (load === 'init') {
    load = 'loaded';
    $('.loaderwrapper').fadeOut();
    loadInvestments();
    loadDraftBMemos();
    loadDraftDecMemos();
  } else {
    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, 
      { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    var batchRequest = new BatchRequest();
    batchRequest.endpoint = memourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = mydrafturl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = draftdecmemourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDraftDecMemos' });
    batchExecutor.executeAsync().done(function (result) {
      $.each(result, function (k, v) {
        var command = $.grep(commands, function (command) {
          return v.id === command.id;
        });
        if (command[0].title === 'getMyDraftMemos') {
          getMyDraftMemos(v.result.result.value);
        } else if (command[0].title === 'getAprInvestments') {
          getHomeAprInvestments(v.result.result.value);
        } else if (command[0].title === 'getDraftDecMemos') {
          getDraftDecMemos(v.result.result.value);
        }
      });
    }).fail(function (err) {
      onError(err);
    });
  }
}

function getHomeAprInvestments(d) {
  investments = [];
  $.each(d, function (i, j) {
    var tlname = "", soname = "", tldate = "", sodate = "";
    if (j.SO) {
      soname = j.SO.Title;     
    }
    if(j.SO_x0020_Date){
      sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
    }
    if (j.TL) {
      tlname = j.TL.Title;       
    }
    if(j.TL_x0020_Date){
      tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
    }
    investments.push({
      refcode: j.Title,
      title: j.Title0,
      id: j.Id,
      im1: getIM(j.IM_x0028_1_x0029_Id),
      im2: getIM(j.IM_x0028_2_x0029_Id),
      im1_id: parseInt(j.IM_x0028_1_x0029_Id),
      im2_id: parseInt(j.IM_x0028_2_x0029_Id),
      pillar: j.Pillar,
      state: j.Federal_x0020_State,
      district: j.Districts || '',
      proposal: j.Source_x0020_of_x0020_Proposal,
      output2: j.Output2,
      output3: j.Output3,
      output: j.Output || '',
      est_fund: j.Est_x002e__x0020_Fund_x0020_ceil || '',
      inv_type: j.Investee_x0020_Type,
      proc_type: j.Proc_x002e__x0020_Type,
      proc_cycle: j.Proc_x002e__x0020_Cycle,
      prob_stat: j.Problem_x0020_Statement,
      just: j.Justification,
      rec: j.Recommendation,
      approval_status: j.Approval_x0020_Status,
      attachments: getAttachmentLinks(j.AttachmentFiles),
      socomments: j.SO_x0020_Comments,
      tlcomments: j.TL_x0020_Comments,
      so: soname,
      tl: tlname,
      tl_date: tldate,
      so_date: sodate,
      tl_approval: j.TL_x0020_Approval,
      so_approval: j.SO_x0020_Approval
    });
  });
  loadInvestments();
}

function loadInvestments() {
  var invContent = '';
  for (i = 0; i < investments.length; i++) {
    var inv = investments[i];
    if (i % 3 === 0) {
      invContent += '<div class="row">';
    }
    var title = inv.title;
    if(title){
      if(title.length>120){
        title = title.substring(0, 120) + " ...";
      }
    }
    invContent += '<div class="col-md-4">';
    invContent += '<div class="box-height" onclick="loadInvestment(\'' + inv.refcode + '\')">';
    invContent += '<div class="col-md-4 h-full div-counter">';
    invContent += '<p class="p-count">' + pad((i + 1) + '', 2) + '</p>';
    invContent += '</div>';
    invContent += '<div class="col-md-8 h-full div-inv">';
    invContent += '<p class="p-ref mb5">' + inv.refcode + '</p>';
    invContent += '<p class="p-title mb5">' + inv.title + '</p>';
    invContent += '<p class="p-im mb5"><b>IM(1):</b>   ' + inv.im1 +
         '</p><p class="m0 p-im"><b>IM(2):</b>   ' + inv.im2 + '</p></p>';
    invContent += '</div>';
    invContent += '</div>';
    invContent += '</div>';
    if ((i - 2) % 3 === 0) {
      invContent += '</div>';
    }
    else if (investments.length - 1 === i) {
      invContent += '</div>';
    }
  }
  $('.loaderwrapper').fadeOut();
  $('#inv-contain').empty().append(invContent);

  var uniqueim1 = [];
  for (var i= 0; i<investments.length; i++) {
    if(investments[i].im1){
      uniqueim1.push(investments[i].im1);
    }
    if(investments[i].im2){
      uniqueim1.push(investments[i].im2);
    }
    }

    uniqueim1  = uniqueim1.filter( onlyUnique );

  uniqueim1.sort();
  $('#imfilter').append('<option val=""></option>');

  $(uniqueim1).each(function (i, j) {
    $('#imfilter').append('<option>' + j + '</option>');
  });

  $('select#statefilter').change(function () {
    var state = $(this);
    var selectedDistricts = $.grep(districts, function (element, index) {
      if ($(state).find('option:selected').attr('data-initial') === 'MR') {
        return true;
      } else {
        return element.state === $(state).find('option:selected').attr('data-initial');
      }
    });
    $('#districtfilter').empty();
    $('#districtfilter').append('<option val=""></option>');
    var content = '';
    for (var i = 0; i < selectedDistricts.length; i++) {
      content += '<option value="' + selectedDistricts[i].name + '">' + selectedDistricts[i].name + '</option>';
    }
    if(state === ''){
      for (var i = 0; i < districts.length; i++) {
          content += '<option value="' + districts[i].name + '">' + districts[i].name + '</option>';
      } 
    }
    $('#districtfilter').append(content).trigger('chosen:updated');
  });

  $('#attachFilesContainer input:file').MultiFile({ max: 4 });

  $("#invfilter select").chosen({ allow_single_deselect: true }).change(function () {
    filterInvestments();
  });
}

function filterInvestments() {
  var result = investments;
  if ($("#imfilter").val()) {
    result = investments.filter((el) =>
      typeof el.im1 === 'string' && el.im1.indexOf($("#imfilter").val()) > -1 ||
      typeof el.im2 === 'string' && el.im2.indexOf($("#imfilter").val()) > -1);
  }
  if ($("#statefilter").val()) {
    result = result.filter((el) =>
     typeof el.state === 'string' && el.state.indexOf($("#statefilter").val()) > -1);
  }
  if ($("#districtfilter").val()) {
    result = result.filter((el) =>
      typeof el.district === 'string' && el.district.indexOf($("#districtfilter").val()) > -1);
  }
  
  if ( $("#outputfilter").val() ) {
    result = result.filter((el) =>
      typeof el.output === 'string' && el.output.indexOf($("#outputfilter").val()) > -1);
  }
  loadfiltered(result);
}


function loadfiltered(results) {
  var invContent = '';
  for ( i = 0; i < results.length; i++ ) {
    var inv = results[i], title = inv.title;
    if(title){
      if(title.length>120){
        title = title.substring(0, 120) + " ...";
      }
    }
    if (i % 3 === 0) {
      invContent += '<div class="row">';
    }
    invContent += '<div class="col-md-4">';
    invContent += '<div class="box-height" onclick="loadInvestment(\'' + inv.refcode + '\')">';
    invContent += '<div class="col-md-4 h-full div-counter">';
    invContent += '<p class="p-count">' + pad((i + 1) + '', 2) + '</p>';
    invContent += '</div>';
    invContent += '<div class="col-md-8 h-full div-inv">';
    invContent += '<p class="p-ref mb5">' + inv.refcode + '</p>';
    invContent += '<p class="p-title mb5">' + title + '</p>';
    invContent += '<p class="p-im mb5"><b>IM(1):</b>   ' + inv.im1 + '</p><p class="m0 p-im"><b>IM(2):</b>   ' + inv.im2 + '</p></p>';
    invContent += '</div>';
    invContent += '</div>';
    invContent += '</div>';
    if ((i - 2) % 3 === 0) {
      invContent += '</div>';
    }
    else if (results.length - 1 === i) {
      invContent += '</div>';
    }
  }
  $('.loaderwrapper').fadeOut();
  $('#inv-contain').empty().append(invContent);

}

function getMemoForm() {
  $('#memo-form .modal-title').html('New Business Case Memo');
  $('#memo-form .save-memo').show();
  $('#memo-form .editatt').hide();

  $('#nextbcmemo').click(function () {
    $('#bcmemo').hide();
    $('#bcmemonext').removeClass('hidden').fadeIn();
  });
  $('#bcprevmemo').click(function () {
    $('#bcmemonext').hide();
    $('#bcmemo').removeClass('hidden').fadeIn();
  });
  $('.save-memo-draft').click(function () {
    $('#submit-type').val('Draft');
    $('#memo-form').formValidation('enableFieldValidators', 'investee_type', false);
    $('#memo-form').formValidation('enableFieldValidators', 'source_of_proposal', false);
    $('#memo-form').formValidation('enableFieldValidators', 'secondary_im', false);
    $('#memo-form').formValidation('enableFieldValidators', 'procurement_cycle', false);
    $('#memo-form').formValidation('enableFieldValidators', 'problem_statement', false);
    $('#memo-form').formValidation('enableFieldValidators', 'justification', false);
    $('#memo-form').formValidation('enableFieldValidators', 'recommendation_to_the_head_of_ssf', false);
    $("#memo-form").data('formValidation').validate();
  });

  $('.save-memo').click(function () {
    $('#submit-type').val('Final');
    $('#memo-form').formValidation('enableFieldValidators', 'investee_type', true);
    $('#memo-form').formValidation('enableFieldValidators', 'source_of_proposal', true);
    $('#memo-form').formValidation('enableFieldValidators', 'secondary_im', true);
    $('#memo-form').formValidation('enableFieldValidators', 'procurement_cycle', true);
    $('#memo-form').formValidation('enableFieldValidators', 'problem_statement', true);
    $('#memo-form').formValidation('enableFieldValidators', 'justification', true);
    $('#memo-form').formValidation('enableFieldValidators', 'recommendation_to_the_head_of_ssf', true);
    $("#memo-form").data('formValidation').validate();
  });


  $('#fundceiling').bind('textchange', function (event, previousText) {
    var value = removeCommas($(this).val());
    if ($.isNumeric(value)) {
      $(this).val(addCommas(value));
    } else {
      $(this).val('');
    }
  });

  $('#memo-form').modal('show');
    $("#memo-form").formValidation({
        framework: "bootstrap",
        excluded: ':disabled',
        err: { container: '.messages' },
        icon: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            title: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Title is required" }
                }
            },
            pillar: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Contract Type is required" }
                }
            },
            state: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Federal State is required" }
                }
            },
            district: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The District is required" },
                }
            },
            output: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Output is required" },
                }
            },
            estimated_fund_ceiling: { row: '.col-md-3', validators: { 
                notEmpty: { message: "The Estimated Fund is required" } } },
            investee_type: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Investee Type is required" }
                }
            },
            source_of_proposal: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Source Of Proposal is required" },
                }
            },
            secondary_im: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Secondary IM is required" },
                }
            },
            procurement_cycle: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Procurement Cycle is required" },
                }
            },
            procurement_type: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Procurement Type is required" },
                }
            },
            problem_statement: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Problem Statement is required" },
                }
            },
            justification: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Justification is required" },
                }
            },
            recommendation_to_the_head_of_ssf: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Recommendation to the head of SSF is required" },
                }
            },
        }
    }).on('success.form.fv', function (e) {
    e.preventDefault();
    if ($('#submit-type').val() === 'Draft') {
      saveMemoDraft();
    } else {
      saveMemo();
    }
  });
  commonBMemoDraftMemo();
}

function saveMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit business case memo for review and approval?',
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    submitMemo();
  });
}

function saveMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: '<p>Only the Primary and Secondary IM can view</p>',
    html: true,
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Save as Draft',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    submitMemo();
  });
}

function submitMemo() {
  var data = [], fileArray = [];
  $('#attachFilesContainer input:file').each(function () {
    if ($(this)[0].files[0]) {
      fileArray.push({ Attachment: $(this)[0].files[0] });
    }
  });

  var dist = $('#district').val();

  if (dist !== null) {
    dist = dist.join(', ');
  } else {
    dist = '';
  }

  data.push({
    Title: $('#refno-h').val() || '',
    Pillar: $('#pillar').val(),
    Title0: $('#bCasetitle').val(),
    Federal_x0020_State: $('#state').val(),
    Districts: dist,
    Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
    Output2: $('#output2').val(),
    Output3: $('#output3').val(),
    Output: $('#output').val(),
    Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
    Investee_x0020_Type: $('#invtype').val(),
    Proc_x002e__x0020_Cycle: $('#procurement_cycle').val(),
    Proc_x002e__x0020_Type: $('.procurement_type:checked').val(),
    Problem_x0020_Statement: $('#probstat').val(),
    Justification: $('#justification').val(),
    Recommendation: $('#recomhead').val(),
    IM_x0028_1_x0029_: parseInt($('#primaryIM').val()),
    IM_x0028_2_x0029_: parseInt($('#secondaryIM').val()),
    Status: $('#submit-type').val(),
    Approval_x0020_Status: 'Pending',
    Files: fileArray
  });

  createBMemoWithAttachments('Business Case Memo', data).then(
    function () {},
    function (sender, args) {
      swal('Error', 'Error occured' + args.get_message());
    });


}

var createBMemoWithAttachments = function (listName, listValues) {
  var fileCountCheck = 0;
  var context = new SP.ClientContext.get_current();
  var dfd = $.Deferred();
  var targetList = context.get_web().get_lists().getByTitle(listName);
  context.load(targetList);
  var itemCreateInfo = new SP.ListItemCreationInformation();
  var listItem = targetList.addItem(itemCreateInfo);
  listItem.set_item('Title', listValues[0].Title);
  listItem.set_item('Pillar', listValues[0].Pillar);
  listItem.set_item('Title0', listValues[0].Title0);
  listItem.set_item('Federal_x0020_State', listValues[0].Federal_x0020_State);
  listItem.set_item('Districts', listValues[0].Districts);
  listItem.set_item('Source_x0020_of_x0020_Proposal', listValues[0].Source_x0020_of_x0020_Proposal);
  listItem.set_item('Output2', listValues[0].Output2);
  listItem.set_item('Output3', listValues[0].Output3);
  listItem.set_item('Output', listValues[0].Output);
  listItem.set_item('Est_x002e__x0020_Fund_x0020_ceil', listValues[0].Est_x002e__x0020_Fund_x0020_ceil);
  listItem.set_item('Investee_x0020_Type', listValues[0].Investee_x0020_Type);
  listItem.set_item('Proc_x002e__x0020_Cycle', listValues[0].Proc_x002e__x0020_Cycle);
  listItem.set_item('Proc_x002e__x0020_Type', listValues[0].Proc_x002e__x0020_Type);
  listItem.set_item('Problem_x0020_Statement', listValues[0].Problem_x0020_Statement);
  listItem.set_item('Justification', listValues[0].Justification);
  listItem.set_item('Recommendation', listValues[0].Recommendation);
  listItem.set_item('IM_x0028_1_x0029_', listValues[0].IM_x0028_1_x0029_);
  listItem.set_item('IM_x0028_2_x0029_', listValues[0].IM_x0028_2_x0029_);
  listItem.set_item('Status', listValues[0].Status);
  listItem.set_item('Approval_x0020_Status', listValues[0].Pending);
  listItem.update();
  context.executeQueryAsync(function () {
    var id = listItem.get_id();
    if (listValues[0].Files.length !== 0) {
      if (fileCountCheck <= listValues[0].Files.length - 1) {
        loopFileUpload(listName, id, listValues, fileCountCheck).then(
          function () { },
          function (sender, args) {
            console.log('Error uploading');
            dfd.reject(sender, args);
          });
      }
    } else {
      swal({
        title: "Success!",
        text: "Business Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) { location.reload(); }
      });
      dfd.resolve(fileCountCheck);
    }
  },
  function (sender, args) {
    swal('Error', 'Error occured' + args.get_message(), 'error');
  });
  return dfd.promise();
};

function updateSaveBMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: '<p>Only the Primary and Secondary IM can view</p>',
    html: true,
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Save as Draft',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#submit-type').val('Draft');
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    updateBMemo();
  });
}

function updateSaveBMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit business case memo for review and approval?',
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#submit-type').val('Final');
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    updateBMemo();
  });
}

function loadInvestment(refcode) {
  $('.loaderwrapper').fadeIn();
  //if(getCache('inv_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'investment.html', function (response, status, xhr) {
    cache('inv_html', response);
    processInvestment(refcode);
  });
}

function processInvestment(refcode) {
  $('.loaderwrapper').fadeOut();
  loadTabs();
  $('input.upload').on('change', function () {
    var path = $(this).val();
    var filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });
  var inv = $.grep(investments, function (element, index) {
    return element.refcode === refcode;
  })[0];
  current_inv = inv;
  loadbatchInv(current_inv);
  fixIframe();
}

function loadbatchInv(current_inv) {
  var commands = [];
  var batchExecutor = new RestBatchExecutor(
      _spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Decision Memo\')/items?$select=*,TL/Title,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_2_x0029_/Title&$expand=AttachmentFiles,SO,TL,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Ref_x002e_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'prepInvForms' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Procurement\')/items?$select=FileLeafRef,EncodedAbsUrl,Description,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getProcurement' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Modification Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,SO/Title,SO/Id,TL/Title,TL/Title&$expand=TL,SO,AttachmentFiles&$filter=BMemoId eq \'' + current_inv.id + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getModMemos' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Performance Management\')/items?$select=Quarter,FileLeafRef,EncodedAbsUrl,Description,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPerformance' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'CloseOut\')/items?$select=*,TL/Title,SO/Title&$expand=SO,TL&$filter=RefCode eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getCloseOuts' });
  batchExecutor.executeAsync().done(function (result) {
    $.each(result, function (k, v) {
      var command = $.grep(commands, function (command) {
        return v.id === command.id;
      });
      if (command[0].title === 'prepInvForms') {
        prepInvForms(v.result.result.value);
      } else if (command[0].title === 'getProcurement') {
        getProcurement(v.result.result.value);
      } else if (command[0].title === 'getModMemos') {
        getModMemos(v.result.result.value);
      } else if (command[0].title === 'getPerformance') {
        getPerformance(v.result.result.value);
      } else if (command[0].title === 'getCloseOuts') {
        getCloseOuts(v.result.result.value);
      }
    });
  }).fail(function (err) {
    onError(err);
  });
}

function updateBMemo() {
  UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);

  var item = {
    __metadata: { type: 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
    Title: $('#refno-h').val(),
    Pillar: $('#pillar').val(),
    Title0: $('#bCasetitle').val(),
    Federal_x0020_State: $('#state').val(),
    Districts: $('#district').val().join(', '),
    Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
    Output2: $('#output2').val(),
    Output3: $('#output3').val(),
    Output: $('#output').val(),
    Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
    Investee_x0020_Type: $('#invtype').val(),
    Proc_x002e__x0020_Cycle: $('#procurement_cycle').val(),
    Proc_x002e__x0020_Type: $('.procurement_type:checked').val(),
    Problem_x0020_Statement: $('#probstat').val(),
    Justification: $('#justification').val(),
    Recommendation: $('#recomhead').val(),
    IM_x0028_1_x0029_Id: parseInt($('#primaryIM').val()),
    IM_x0028_2_x0029_Id: parseInt($('#secondaryIM').val()),
    Status: $('#submit-type').val(),
    Approval_x0020_Status: 'Pending',
  };
  fileArray = [];
  var listValues = [], listName = 'Business Case Memo', id = $('#bmemoid').val(), fileCountCheck = 0;

  $('#attachFilesContainer input:file').each(function () {
    if ($(this)[0].files[0]) {
      fileArray.push({ Attachment: $(this)[0].files[0] });
    }
  });

  updateJson(psitelst + '(\'' + listName + '\')/items(' + id + ')', item, success);
  function success(d) {
    delete item.__metadata;
    var dfd = $.Deferred();
    item.Files = fileArray;

    listValues.push(item);

    if (listValues[0].Files.length !== 0) {
      if (fileCountCheck <= listValues[0].Files.length - 1) {
        uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment).then(
          function (data) {
            fileCountCheck++;
            if (fileCountCheck <= listValues[0].Files.length - 1) {
              uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment);
            } else {
              swal({
                title: "Success!",
                text: "Business Memo submitted successfully.Refresh page",
                type: "success",
              },
              function (isConfirm) {
                if (isConfirm) { location.reload(); }
              });
            }
            dfd.resolve(fileCountCheck);
          },
          function (sender, args) {
            console.log('Not uploaded');
            dfd.reject(sender, args);
          });
      }
    }
    else {
      dfd.resolve(fileCountCheck);
      swal({
        title: "Success!",
        text: "Business Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) { location.reload(); }
      });
    }
    return dfd.promise();
  }
}

function loadDraftBMemos() {
  var s = '';
  $.each(draftmemos, function (i, j) {
    var btntext = 'Edit', btnclass = 'btn-primary';
    if (j.status === 'Final') {
      btntext = 'View';
      btnclass = 'btn-default';
    }
    s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' + 
    j.im1 + '</td><td>' + j.im2 + '</td><td>' + j.status + '</td><td>' + j.tl_approval + '</td><td>' +
        j.so_approval + '</td><td><a href="#" onclick="loadDraftBMemo(' + j.id + ')" class="btn ' + 
        btnclass + '">' + btntext + '</a></td></tr>';
  });
  tbdraftmemo = null;
  $('#tbdraftmemo>tbody').html(s);
  tbdraftmemo = $('#tbdraftmemo').DataTable({ responsive: true });
}

function validateDMemo(){
      
  $('#decision-memo-form .btn-success').click(function () {
    $('#submit-type').val('Final');
    $('#decision-memo-form').formValidation('enableFieldValidators', 'contact', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'introduction', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'key_activities', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'value_for_money', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'risk_assessment', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'sustainability_summary', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'conflict_sensitivity', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'recommendation', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'proposed_action', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'strategy_link', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'prob_desc', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'coordination', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'project_assets', true);
    $("#decision-memo-form").data('formValidation').validate();
  });

  $('#decision-memo-form .btn-primary').click(function () {
    $('#submit-type').val('Draft');
    $('#decision-memo-form').formValidation('enableFieldValidators', 'contact', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'introduction', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'key_activities', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'value_for_money', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'risk_assessment', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'sustainability_summary', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'conflict_sensitivity', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'recommendation', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'proposed_action', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'strategy_link', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'prob_desc', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'coordination', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'project_assets', false);
    $("#decision-memo-form").data('formValidation').validate();
  });

    $("#decision-memo-form").formValidation({
        framework: "bootstrap",
        excluded: ':disabled',
        err: { container: '.messages' },
        icon: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            estimated_fund: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Actual Fund is required" },
                }
            },
            start_date: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The field is required" },
                    date: { format: 'DD/MM/YYYY', message: 'The value is not a valid date' }
                }
            },
            end_date: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The field is required" },
                    date: { format: 'DD/MM/YYYY', message: 'The value is not a valid date' }
                }
            },
            d_email: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Investee Email is required" },
                    emailAddress: { message: 'The value is not a valid email address' }
                }
            },
            phone_number: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Phone number is required" },
                    regexp: {
                        message: 'The phone number can only contain the digits, spaces, -, (, ), + and .',
                        regexp: /^[0-9\s\-()+\.]+$/
                    }
                }
            },
            contact: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Investee Address is required" }
                }
            },
            introduction: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The Introduction/Background is required" },
                }
            },
            key_activities: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            value_for_money: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            risk_assessment: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            sustainability_summary: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            prob_desc: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            conflict_sensitivity: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            proposed_action: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            coordination: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            strategy_link: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            project_assets: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            recommendation: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
        },
    }).on('success.form.fv', (e) => {
        e.preventDefault();
        if ($('#submit-type').val() === 'Final') {
            updateSaveDecMemo();
        } else {
            updateSaveDecMemoDraft();
        }
    });

}

function loadDraftDecMemos() {
  var s = '';
  $.each(draftdecmemos, function (i, j) {
    var btntext = 'Edit', btnclass = 'btn-primary';
    if (j.status === 'Final') {
      btntext = 'View';
      btnclass = 'btn-default';
    }
    s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' +
         j.startdate + '</td><td>' + j.enddate + '</td><td>' + j.im1 + '</td><td>' + j.im2 + '</td><td>' +
        j.status + '</td><td>' + j.tl_approval + '</td><td>' + j.so_approval + 
        '</td><td><a href="#" onclick="loadDraftDecMemo(' + j.id + ')" class="btn ' + btnclass + '">' +
         btntext + '</a></td></tr>';
  });
  $('#tbdraftdecmemo>tbody').html(s);
  $('#tbdraftdecmemo').DataTable({ responsive: true });  
}



function loadDraftBMemo(id) {
  commonBMemoDraftMemo();
  var d = getItemFromArrayById(draftmemos, id);
  if (d === null) { return;}
  d = d[0];

  if(d.district.indexOf(",") === -1){
    $('#district').val(d.district);
  }else{            
      $('#district').val($.map(d.district.split(','), $.trim));
  }
  $('#refno').text(d.refcode);
  $('#refno-h').val(d.refcode);
  $('#bmemoid').val(id);
  $('#bCasetitle').val(d.title);
  $('#state').val(d.state.split(','));  
  $('#proposalSrc').val(d.proposal);
  $('#output').val(d.output);
  $('#output2').val(d.output2);
  $('#output3').val(d.output3);
  $('#pillar').val(d.pillar);
  $('#fundceiling').val(d.est_fund);
  if ($.isNumeric(d.est_fund)) {
    $('#fundceiling').val(addCommas(d.est_fund));
  }
  $('#invtype').val(d.inv_type);
  $('#primaryIM').val(d.im1_id);
  $('#secondaryIM').val(d.im2_id);
  $('#procurement_cycle').val(d.proc_cycle);
  $('.b_tl_comment').html(d.tlcomments);
  $('.b_so_comment').html(d.socomments);
  $('input[name=procurement_type][value=\'' + d.proc_type + '\']').prop('checked', 'true');
  $('#hiddentext').html(d.prob_stat);
  var prob_stat = $('#hiddentext').text();
  $('#probstat').val(prob_stat);
  $('#hiddentext').html(d.just);
  var just = $('#hiddentext').text();
  $('#justification').val(just);
  $('#hiddentext').html(d.rec);
  var rec = $('#hiddentext').text();
  $('#recomhead').val(rec);
  createAttachmentLink(d);
  $('.update-memo').click(function () {
    updateSaveBMemo();
  });
  $('.save-memo-draft').click(function () {
    updateSaveBMemoDraft();
  });
  if (d.tldate !== '') {
    $(".editbmemo").show();
  } else {
    $(".editbmemo").hide();
  }

  $("body").on('click', '.delatt', function () {
    deleteAttachment('Business Case Memo', $('#bmemoid').val(), $(this).data('file'), $(this));
  });

  $('#memo-form select').trigger('chosen:updated');
  $('#memo-form .modal-title').html('Edit Business Case Memo');
  $('#memo-form .save-memo').hide();
  $('#memo-form .editatt').show();
  if (d.status === 'Final') {
    $("#memo-form input,#memo-form textarea,#memo-form select").prop("disabled", true);
    $('#memo-form .save-memo-draft,#memo-form .update-memo,#memo-form .delatt').hide();
  } else {
    $("#memo-form input,#memo-form textarea,#memo-form select").prop("disabled", false);
    $('#memo-form .save-memo-draft,#memo-form .update-memo,#memo-form .delatt').show();
  }

  $('#memo-form').modal('show');
}

function deleteAttachment(listname, id, filename, elem) {
  swal({
    title: 'Delete Attachment',
    text: 'Are you sure you want to delete this attachment?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
  },
  function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: psitelst + '(\'' + listname + '\')/GetItemById(' + id +
         ')/AttachmentFiles/getByFileName(\'' + filename.trim() + '\')',
        type: 'Delete',
        contentType: 'application/json;odata=verbose',
        headers: {
          'X-RequestDigest': $('#__REQUESTDIGEST').val(),
          'X-HTTP-Method': 'Delete',
          'Accept': 'application/json;odata=verbose'
        },
        success: function (data) {
          swal({
            title: "Success!",
            text: 'Attachment removed successfully.',
            type: "success",
          },
          function (isConfirm) {
            if (isConfirm) { location.reload(); }
          });
        },
        error: onError
      });
    }
  });
}

function createAttachmentLink(d) {
  $('.editatt div').html(d.attachments);
  if (d.attachments.length > 0) {
    $('.editatt div a').html();
    $('.editatt div a').each(function (i, obj) {
      var p = $(obj).html();
      p = p.split('</span>');
      p = p[1].trim();
      $(this).after('<i class="fa fa-trash delatt text-danger" style="cursor:pointer" data-file="' + p + '"></i>');
    });
  }
}

function createDeleteAttachmentLink(d) {
  var link = "", att = d.attachments;
  if (att.length > 0) {
    var linkArr = att.split(","), newarr = [];
    if (linkArr[linkArr.length - 1] === 0) { linkArr.pop(); } 
    $.each(linkArr, function (i, j) {
      var linkbefore = j;
      j = $.parseHTML(j);
      var filename = $(j).text();
      linkbefore += '&nbsp;&nbsp;<i class="fa fa-trash delatt text-danger" style="cursor:pointer" data-file="' +
         filename + '"></i>';
      newarr.push(linkbefore);
    });
    link = newarr.join(' ');
    return link;
  }
}

function removeAttachment(filename, id) {
  var dmemo = getDraftBMemo(id);
  if (dmemo === null) { return; } 
  var d = dmemo[0];
  var links = d.attachments.split(",");
  for (var i = links.length - 1; i >= 0; i--) {
    var ind = links[i].indexOf(filename);
    if (ind !== -1 || links[i].trim() === '') {
      links.splice(i, 1);
    }
  }
  var datt = "", newdmemo = dmemo[0];
  if (links.length > 0) {
    datt = links.join(", ");
    newdmemo.attachments = datt;
  }

  var index = draftmemos.indexOf(dmemo);

  if (index !== -1) {
    draftmemos[index] = newdmemo;
  }

}

function loadDraftDecMemo(id) {
    var d = getItemFromArrayById(draftdecmemos, id);
    if (d === null) { return; }
    d = d[0];
    draftdecmemo = d;
    $('.bbs-ref').html(d.refcode);
    $('.bbs-title').html(d.title);
    $('.bbs-state').html(d.state);
    $('.bbs-districts').html(d.district);
    $('.bbs-im1').html(d.im1);
    $('.bbs-im2').html(d.im2);
    $('.bbs-pillar').html(d.pillar);
    $('.bbs-prop').html(d.proposal);
    $('.bbs-out').html(d.output);
    $('.bbs-fund').html(d.est_fund);
    $('.bbs-inv_type').html(d.inv_type);
    $('.bbs-proc_type').html(d.proc_type);
    $('.bbs-proc_cycle').html(d.proc_cycle);
    $('.bbs-prob').html(d.prob_stat);
    $('.bbs-just').html(d.just);
    $('.bbs-rec').html(d.rec);
    $('#tl_date').html(d.tldate);
    $('#so_date').html(d.sodate);
    $('.tlcomment_d').html(d.tl_comment);
    $('.socomment_d').html(d.so_comment);
    $('#phone').val(d.phone);
    $('.bbs-fund').val(d.est_fund);
    if ($.isNumeric(d.est_fund)) {
      $('.bbs-fund').val(addCommas(d.est_fund));
    }
    $('#email').val(d.email);
    $('#address').val(d.address);
    $('#risksummary').val(d.risk);
    $('#startdate').val(d.startdate);
    $('#enddate').val(d.enddate);
    $('#background').val(d.background);
    $('#keyact').val(d.activities);
    $('#valuemoney').val(d.valuemoney);
    $('#sustain').val(d.sustainability);
    $('#conflictsens').val(d.conflict);  
    $('#prob_desc').val($(d.prob_desc).text());
    $('#proposed_action').val($(d.proposed_action).text());
    $('#strategy_link').val($(d.strategy_link).text());
    $('#coordination').val($(d.coordination).text());
    $('#project_assets').val($(d.project_assets).text());
    $('#recomm').val(d.rec);
    $("#d_attachments").html(createDeleteAttachmentLink(d));
    //#TODO fix getting html 
  
    if (d.status === 'Final') {
      $("#decision-memo-form input,#decision-memo-form textarea,#decision-memo-form select").prop("disabled", true);
      $('#decision-memo-form .btn-primary,#decision-memo-form .btn-success,#decision-memo-form .delatt').hide();
    } else {
      $("#decision-memo-form input,#decision-memo-form textarea,#decision-memo-form select").prop("disabled", false);
      $('#decision-memo-form .btn-primary,#decision-memo-form .btn-success,#decision-memo-form .delatt').show();
    }
  
    $("body").on('click', '#d_attachments .delatt', function () {
      deleteAttachment('Decision Memo', draftdecmemo.id, $(this).data('file'), $(this));
    });
  
    prepDecModal();
  
  }


  function updateSaveDecMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    $('#submit-type').val('Final');
    submitUpdatedDecMemo();
  });
}

function updateSaveDecMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    $('#submit-type').val('Final');
    submitUpdatedDecMemo();
  });
}

function updateSaveDecMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: 'You want to save the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        
    if (draftdecmemo.status === "More Info") {
      $('#submit-type').val('More Info');
    }           
    else{
      $('#submit-type').val('Draft');
    }
    submitUpdatedDecMemo();
  });
}

function submitUpdatedDecMemo() {
  var item = {
    __metadata: { type: 'SP.Data.Decision_x0020_Memo_x0020_ListListItem' },
    Start_x0020_Date: moment($('#startdate').val() + ' ' + moment().format('hh:mm:ss'),
     'DD/MM/YYYY hh:mm:ss').toISOString(),
    End_x0020_Date: moment($('#enddate').val() + ' ' + moment().format('hh:mm:ss'),
     'DD/MM/YYYY hh:mm:ss').toISOString(),
    Email: $('#email').val(),
    Phone: $('#phone').val(),
    Address: $('#address').val(),
    Background: $('#background').val(),
    Key_x0020_Activities: $('#keyact').val(),
    Value_x0020_for_x0020_money: $('#valuemoney').val(),
    Risk_x0020_Summary: $('#risksummary').val(),
    Sustainability: $('#sustain').val(),
    Conflict_x0020_Sensitivity: $('#conflictsens').val(),
    Recommendation: $('#recomm').val(),
    Problem_x0020_Description: $('#prob_desc').val(),
    Proposed_x0020_Action: $('#proposed_action').val(),
    Strategy_x0020_Link: $('#strategy_link').val(),
    Coordination: $('#coordination').val(),
    Project_x0020_Assets: $('#project_assets').val(),
    Approval_x0020_Status: 'Pending',
    Status: $('#submit-type').val()
  };
   
  var listName = 'Decision Memo', fileArray = [];

  if ($('#evaluation_report').val() !== '') {
    fileArray.push({ Attachment: $('#evaluation_report')[0].files[0], fname: 'evaluation_report' });
  }

  updateJson(psitelst + '(\'' + listName + '\')/items(' + draftdecmemo.id + ')', item, success);
  var id = draftdecmemo.id;
  function success(d) {
    if (fileArray.length === 0) {

      swal({
        title: "Success!",
        text: "Decision Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) {
          $("#decision-memo-form").modal('hide');
          setTimeout(function () { loadHome(); }, 1000);
        }
      });

    } else {

      var dfd = $.Deferred().resolve();

      fileArray.forEach(function (f) {
        dfd = dfd.then(function () {
          return uploadFileSP(listName, id, f.Attachment, draftdecmemo.refcode + '_' + f.fname);
        });
      });

      dfd.done(function () {
        swal({
          title: "Success!",
          text: "Decision Memo submitted successfully.Refresh page",
          type: "success",
        },
        function (isConfirm) {
          if (isConfirm) {
            $("#decision-memo-form").modal('hide');
            setTimeout(function () { loadHome(); }, 1000);
          }
        });
      });
    }
  }
}

function commonBMemoDraftMemo() {
  $('#primaryIM').empty().append('<option value="' + _spPageContextInfo.userId + '">' +
   _spPageContextInfo.userDisplayName + '</option>');
  $('#secondaryIM').empty();
  $(ims).each(function (index, value) {
    $('#secondaryIM').append('<option value="' + value.id + '">' + value.name + '</option>');
  });
  $('select#state').change(function () {
    var state = $(this);
    var selectedDistricts = $.grep(districts, function (element, index) {
      if ($(state).find('option:selected').attr('data-initial') === 'MR') {
        return true;
      } else {
        return element.state === $(state).find('option:selected').attr('data-initial');
      }
    });
    $('select#district').empty();
    var content = '';
    for (var i = 0; i < selectedDistricts.length; i++) {
      content += '<option value="' + selectedDistricts[i].name + '">' + selectedDistricts[i].name + '</option>';
    }
    $('#district').append(content).trigger('chosen:updated');
  });
  $('#nextbcmemo').click(function () {
    $('#bcmemo').hide();
    $('#bcmemonext').removeClass('hidden').fadeIn();
  });
  $('#bcprevmemo').click(function () {
    $('#bcmemonext').hide();
    $('#bcmemo').removeClass('hidden').fadeIn();
  });
  $('#state').change(function () {
    getReference();
  });
  $('#pillar').change(function () {
    getReference();
  });

  $('input.upload').on('change', function () {
    var path = $(this).val();
    var filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });
  $('select').chosen();
  $('#memo-form').on('hidden.bs.modal', function () {
    $(this).data('modal', null);
    $("#memo-form input,#memo-form textarea").val("");
    $("#memo-form select").val("").trigger('chosen:updated');
    $("#refno").html("");
    $("#bcmemo").show();
    $('#bcmemonext').hide();
    $('#memo-form input:file').MultiFile('reset');
  });
}

