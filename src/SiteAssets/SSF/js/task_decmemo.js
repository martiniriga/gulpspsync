﻿function getDecMemoList() {
    decmemos = [],investments=[];
    $('.loaderwrapper').fadeIn();
    var dapurl = 'lists/GetByTitle(\'Decision Memo\')/items?$select=*,AttachmentFiles,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Status eq \'Final\' and Approval_x0020_Status eq \'Pending\'';
    if ( $.inArray("TL", user.access) !== -1 ) {
        dapurl += " and TL_x0020_Approval eq 'Pending'";
    }
    if ( $.inArray("SO", user.access) !== -1 ) {
        dapurl += " and SO_x0020_Approval eq 'Pending' and TL_x0020_Approval eq 'Approved'";
    }
    RestCalls(dapurl, function (d) {
        $.each(d.results, function (i, j) {
            var attachments = "";
            if (j.Attachments) {
                attachments = getAttachmentLinks(j.AttachmentFiles.results);
            }
            decmemos.push({
                refcode: j.Ref_x002e_Code,
                title: j.Title,
                id: j.Id,
                im1: getIM(j.IM_x0028_1_x0029_Id),
                im2: getIM(j.IM_x0028_2_x0029_Id),
                im1_id: parseInt(j.IM_x0028_1_x0029_Id),
                im2_id: parseInt(j.IM_x0028_2_x0029_Id),
                background: j.Background,
                state: j.Federal_x0020_State,
                district: j.District,
                proposal: j.Source_x0020_of_x0020_Proposal,
                activities: j.Key_x0020_Activities,
                email: j.Email,
                phone: j.Phone,
                address: j.Address,
                valuemoney: j.Value_x0020_for_x0020_money,
                risk: j.Risk_x0020_Summary,
                sustainability: j.Sustainability,
                conflict: j.Conflict_x0020_Sensitivity,
                status: j.Status,
                output: j.Output,
                output2: j.Output2,
                output3: j.Output3,
                startdate: moment(j.Start_x0020_Date).format('DD/MM/YYYY'),
                enddate: moment(j.End_x0020_Date).format('DD/MM/YYYY'),
                est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
                tl_approval: j.TL_x0020_Approval,
                tl_comment: j.TL_x0020_Comments,
                so_approval: j.SO_x0020_Approval,
                so_comment: j.SO_x0020_Comments,
                rec: j.Recommendation,
                approval_status: j.Approval_x0020_Status,
                attachments: attachments,
                prob_desc:j.Problem_x0020_Description,
                proposed_action:j.Proposed_x0020_Action,
                strategy_link:j.Strategy_x0020_Link,
                coordination:j.Coordination,
                project_assets: j.Project_x0020_Assets
            });
        });
        populateDecMemo();
    });
}

getDecMemoList();

function populateDecMemo() {
    $('.loaderwrapper').fadeOut();
    $('.date').datetimepicker({ format: 'DD/MM/YYYY' });
    var s = '';
    $.each(decmemos, function (i, j) {
        var status = "";
        if ( $.inArray("TL", user.access) !== -1) {
            status = j.tl_approval;
        }
        if ( $.inArray("SO", user.access) !== -1 ) {
            status = j.so_approval;
        }
        s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' +
            j.startdate + '</td><td>' + j.enddate + '</td><td>' + j.im1 + '</td><td>' + j.im2 + '</td><td>' + 
            status + '</td><td><a href="#" data-id="' + j.id + '" class="btn btn-orange">Review</a></td></tr>';
    });  
    $('#tbdec>tbody').html(s);
    tbdec = null;
    tbdec = $('#tbdec').DataTable({ responsive: true });   

    $('body').on('click', '#tbdec a', function () {
        var id = $(this).data('id');
        decmemo = getItemFromArrayById(decmemos,id)[0];
        $('.bbs-ref').text(decmemo.refcode);
        $('#bbs-title').text(decmemo.title);
        $('#state').text(decmemo.state);
        $('#district').text(decmemo.district);
        $('#primaryIM').text(decmemo.im1);
        $('#secondaryIM').text(decmemo.im2);
        $('#output').text(decmemo.output);
        $('#hiddentext').html(decmemo.valuemoney);
        var valuemoney = $('#hiddentext').text();
        $('#valuemoney').html(valuemoney);
        $('#enddate').val(decmemo.enddate);
        $('#startdate').val(decmemo.startdate);
        $('#phone').val(decmemo.phone);
        $('#email').val(decmemo.email);
        $('#prob_desc').val($(decmemo.prob_desc).text());
        $('#proposed_action').val($(decmemo.proposed_action).text());
        $('#strategy_link').val($(decmemo.strategy_link).text());
        $('#coordination').val($(decmemo.coordination).text());
        $('#project_assets').val($(decmemo.project_assets).text());
        $('#hiddentext').html(decmemo.background);
        var bg = $('#hiddentext').text();
        $('#background').val(bg);
        $('#address').val(decmemo.address);
        $('#hiddentext').html(decmemo.activities);
        var activities = $('#hiddentext').text();
        $('#activities').html(activities);
        $('#fundceiling').val(addCommas(decmemo.est_fund));
        $('#inv_type').val(decmemo.inv_type);
        $('#proc_cycle').val(decmemo.proc_cycle);
        $('#sustainability').val(decmemo.sustainability);
        $('#conflict').val(decmemo.conflict);
        $('#risk').val(decmemo.risk);        
        $('#recommend').val(decmemo.rec);
        $('#so_comment').html(decmemo.so_comment);
        $('#tl_comment').html(decmemo.tl_comment);
        $('#comment').html($(decmemo.comments).text());
        $('#attachments').html(decmemo.attachments);
        $('.bbs_case_table select').chosen();
        $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click');
        $('.bbs_case_table textarea').each(function () {
            $(this).height($(this).prop('scrollHeight'));
        });
        if ( decmemo.status === 'Approved' ) {
            $('.bbs_case_table button.btn').hide();
        }
    });
}

function submitApproveDecisionMemo() {
    if (user.access.length === 0) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }
    var item = {
        __metadata: { type: 'SP.Data.Decision_x0020_Memo_x0020_ListListItem' },
        Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
        Start_x0020_Date: moment($('#startdate').val() + ' ' + moment().format('hh:mm:ss'), 
            'DD/MM/YYYY hh:mm:ss').toISOString(),
        End_x0020_Date: moment($('#enddate').val() + ' ' + moment().format('hh:mm:ss'), 
            'DD/MM/YYYY hh:mm:ss').toISOString(),
        Email: $('#email').val(),
        Phone: $('#phone').val(),
        Address: $('#address').val(),
        Background: $('#background').val(),
        Key_x0020_Activities: $('#activities').val(),
        Value_x0020_for_x0020_money: $('#valuemoney').val(),
        Risk_x0020_Summary: $('#risksummary').val(),
        Sustainability: $('#sustain').val(),
        Conflict_x0020_Sensitivity: $('#conflictsens').val(),
        Problem_x0020_Description: $('#prob_desc').val(),
        Proposed_x0020_Action: $('#proposed_action').val(),
        Strategy_x0020_Link: $('#strategy_link').val(),
        Coordination: $('#coordination').val(),
        Project_x0020_Assets: $('#project_assets').val(),
        Recommendation: $('#recommend').val()
    };

    item = moreinfo(item, 'Approved');
    updateJson(psitelst + '(\'Decision Memo\')/items(' + decmemo.id + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Decision Memo approved successfully",
            type: "success",
        },
            function (isConfirm) {
                if (isConfirm) { loadDecMemoTasks(); }
            });
    }


}

function submitRejectDecisionMemo() {
    var item = { __metadata: { type: 'SP.Data.Decision_x0020_Memo_x0020_ListListItem' } };

    if ( user.access.length === 0 ) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }

    item = moreinfo(item, 'Rejected');

    updateJson(psitelst + '(\'Decision Memo\')/items(' + decmemo.id + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Decision Memo rejected successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadDecMemoTasks(); }
      });
    }



}

function saveRequestAddDecisionMemo() {
    if ( $('#comment').val() === "" ) {
        swal(
            "Error", 
            "Kindly provide in the comment box the additional info you require for the Decision memo", 
            "error");
        $("#comment").focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'Request Additional information?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        requestAddDecisionMemo();
    });
}

function requestAddDecisionMemo() {
    var item = {
        __metadata: { type: 'SP.Data.Decision_x0020_Memo_x0020_ListListItem' },
        Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
        Start_x0020_Date: moment($('#startdate').val() + ' ' + moment().format('hh:mm:ss'), 
            'DD/MM/YYYY hh:mm:ss').toISOString(),
        End_x0020_Date: moment($('#enddate').val() + ' ' + moment().format('hh:mm:ss'), 
            'DD/MM/YYYY hh:mm:ss').toISOString(),
        Email: $('#email').val(),
        Phone: $('#phone').val(),
        Address: $('#address').val(),
        Background: $('#background').val(),
        Key_x0020_Activities: $('#activities').val(),
        Value_x0020_for_x0020_money: $('#valuemoney').val(),
        Risk_x0020_Summary: $('#risksummary').val(),
        Sustainability: $('#sustain').val(),
        Conflict_x0020_Sensitivity: $('#conflictsens').val(),
        Problem_x0020_Description: $('#prob_desc').val(),
        Proposed_x0020_Action: $('#proposed_action').val(),
        Strategy_x0020_Link: $('#strategy_link').val(),
        Coordination: $('#coordination').val(),
        Project_x0020_Assets: $('#project_assets').val(),
        Recommendation: $('#recommend').val()
    };
    item = moreinfo(item,'More Info');
    updateJson(psitelst + '(\'Decision Memo\')/items(' + decmemo.id + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Additional information request successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadDecMemoTasks(); }
      });
    }

}

function saveRejectDecisionMemo() {
    if ( $('#comment').val() === "" ) {
        swal(
            "Error", 
            "Kindly provide a reason in the comment box as to why the Decision memo is being rejected", 
            "error");
        $("#comment").focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'You are about to reject the Decision Memo/',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        submitRejectDecisionMemo();
    });
}

function saveApproveDecisionMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You are about to Approve the Decision Memo.',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        submitApproveDecisionMemo();
    });
}
