var users = [], ims = [], locations = [], focals = [], user = { access:[]},decmemos=[],modmemos=[],draftdecmemos=[], draftmemos = [], investments = [], allinvestments = [], decisions = [], fileArray = [];
var approver = false;
var current_inv = null;
var cache_active = true;
var leave_days = [];
var approved_leave_days = [];
var siteUrl = _spPageContextInfo.webAbsoluteUrl + '/_api/web/';
var psitelst = siteUrl + 'lists/GetByTitle';
var codepath = '/sites/mpf/SiteAssets/SSF/';
var load = 'init';
var leaveurl = psitelst + '(\'Leave Requests\')/items?$select=*,Author/Title,Supervisor/Title&$expand=Supervisor,Author&$filter=SupervisorId eq ' + _spPageContextInfo.userId;
var memourl = psitelst + '(\'Business Case Memo\')/items?$select=*,TL/Id,TL/Title,SO/Id,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=SO,TL,AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Approval_x0020_Status eq \'Approved\'';
var travelurl = psitelst + '(\'TR\')/items?$select=*,Focal_x0020_Person/Title,Author/Title,Focal_x0020_PersonId&$expand=Focal_x0020_Person,Author';
var mydrafturl = psitelst + '(\'Business Case Memo\')/items?$select=*,TL/Id,TL/Title,SO/Id,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=SO,TL,AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Approval_x0020_Status ne \'Approved\') and (IM_x0028_1_x0029_Id eq ' + _spPageContextInfo.userId + ' or IM_x0028_2_x0029_Id eq ' + _spPageContextInfo.userId + ')';
var draftdecmemourl = psitelst + '(\'Decision Memo\')/items?$select=*,TL/Id,TL/Title,SO/Id,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=SO,TL,AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Approval_x0020_Status ne \'Approved\') and (IM_x0028_1_x0029_Id eq ' + _spPageContextInfo.userId + ' or IM_x0028_2_x0029_Id eq ' + _spPageContextInfo.userId + ')';
var tbtravel = null, tbdec = null, tbcase = null, tbdraftmemo=null,travels = [], leaves = [];
var travform = { id: null, purpose: '', dest1: '', dest2: '', date1: '', date2: '', destinations: '', hostdetails: '', flights: '', accomodation: '', transport: '', luggage: '', otherinfo: '', focalid: null, focal: '', status: '' };
var districts = [{ index: 17, name: 'Abudwak', state: 'GS', state_id: 2 }, { index: 16, name: 'Adado', state: 'GS', state_id: 2 }, { index: 3, name: 'Afmadow', state: 'JS', state_id: 3 }, { index: 10, name: 'Aluula', state: 'PL', state_id: 1 }, { index: 25, name: 'Baidoa', state: 'SW', state_id: 4 }, { index: 27, name: 'Baki', state: 'SL', state_id: 6 }, { index: 41, name: 'Balacad', state: 'HM', state_id: 5 }, { index: 33, name: 'Baraawe', state: 'HM', state_id: 5 }, { index: 2, name: 'Bardera', state: 'JS', state_id: 3 }, { index: 39, name: 'Bardheere', state: 'JS', state_id: 3 }, { index: 6, name: 'Beled Haawo', state: 'JS', state_id: 3 }, { index: 40, name: 'Bevarweyne', state: 'HM', state_id: 5 }, { index: 8, name: 'Bosaso', state: 'PL', state_id: 1 }, { index: 4, name: 'Buaale', state: 'JS', state_id: 3 }, { index: 29, name: 'Burao', state: 'SL', state_id: 6 }, { index: 7, name: 'Buuhoodle', state: 'PL', state_id: 1 }, { index: 30, name: 'Buuhoodle', state: 'SL', state_id: 6 }, { index: 43, name: 'Ceel Barde', state: 'SW', state_id: 4 }, { index: 20, name: 'Ceeldheer', state: 'GS', state_id: 2 }, { index: 11, name: 'Dhahar', state: 'PL', state_id: 1 }, { index: 18, name: 'Dhusamareb', state: 'GS', state_id: 2 }, { index: 45, name: 'Dinsoor', state: 'SW', state_id: 4 }, { index: 19, name: 'El Buur', state: 'GS', state_id: 2 }, { index: 15, name: 'Galkayo', state: 'GS', state_id: 2 }, { index: 37, name: 'Garbaharey', state: 'JS', state_id: 3 }, { index: 12, name: 'Garowe', state: 'PL', state_id: 1 }, { index: 24, name: 'Godinlabe', state: 'GS', state_id: 2 }, { index: 23, name: 'Guriel', state: 'GS', state_id: 2 }, { index: 22, name: 'Harardhere', state: 'GS', state_id: 2 }, { index: 28, name: 'Hargeisa', state: 'SL', state_id: 6 }, { index: 21, name: 'Hobyo', state: 'GS', state_id: 2 }, { index: 26, name: 'Jowhar', state: 'HM', state_id: 5 }, { index: 1, name: 'Kismayo', state: 'JS', state_id: 3 }, { index: 14, name: 'Las Anod', state: 'SS', state_id: 7 }, { index: 13, name: 'Las Qorey', state: 'SS', state_id: 7 }, { index: 5, name: 'Luuq', state: 'JS', state_id: 3 }, { index: 31, name: 'Mogadishu', state: 'BR', state_id: 8 }, { index: 35, name: 'Nationwide', state: 'NW', state_id: 9 }, { index: 36, name: 'Non-Specific', state: 'NS', state_id: 11 }, { index: 9, name: 'Qardho', state: 'PL', state_id: 1 }, { index: 38, name: 'Raskamboni', state: 'JS', state_id: 3 }, { index: 32, name: 'Warsheikh', state: 'HM', state_id: 5 }, { index: 44, name: 'Xudul', state: 'SW', state_id: 4 }];
var trform = { id: null, title: '', refcode: '', districts: '', attachments: [], state: '', im1: '', im2: '', im1_id: null, im2_id: null, approval_status: '', proposal: '', objective: '', output: '', est_fund: '', inv_type: '', proc_cycle: '', proc_type: '', prob_stat: '', just: '', rec: '', disabled: true, pillar: '', socomments: '', tlcomments: '', tldate: '', sodate: '', tl_approval: '', so_approval: ''};
var decmemo = { id: null, title: '', refcode: '', district: '', state: '', im1: '', im2: '', im1_id: null, im2_id: null, status: '',output2: '', output3: '', output: '', est_fund: '', inv_type: '', startdate: '', enddate: '', email: '', rec: '', phone: '',address: '', background: '', activities: '', valuemoney: '', sustainability: '', conflict: '', disabled: true, comments: '', proposal:'',so_comment: '', tl_comment: '',risk: '',tl_approval:'',so_approval:'',approval_status:'',attachments:null,tl_id:null,tl:'',so:'',so_id:null};
var draftdecmemo = decmemo;


function getCache(key) {
    if (localStorage.getItem(key) !== null) {
        return isJson(localStorage.getItem(key));
    }
    else {
        return null;
    }
}

function isJson(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
        var json = JSON.parse(item); if (typeof json === 'object' && json !== null) {
            return json;
        }
        return item;
    } catch (e) {
        return item;
    }
}

function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    var x = parseFloat(value);
    return (x | 0) === x;
}

function RestCalls(u, f) {
    return $.ajax({
        url: siteUrl + u,
        method: 'GET',
        headers: { Accept: 'application/json; odata=verbose' },
        success: function (data) { f(data.d); },
        error: onError
    });
}

function onError(e) {
    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
    console.log(e.responseText);
    swal('Error', e.responseText, 'error');
}

function postJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: { 'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val() },
        success: success,
        error: onError
    });
}

function updateJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: {
            'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val(),
            'X-HTTP-Method': 'MERGE', 'If-Match': '*'
        },
        success: success,
        error: onError
    });
}


function renewDigest() {
    var item = {};
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/contextinfo', item, success);
    function success(d) {
        $('#__REQUESTDIGEST').val(d.d.GetContextWebInformation.FormDigestValue);
        setTimeout(function () {
            renewDigest();
        }, 1770000);
    }
}

function cache(key, value) {
    if (typeof value === 'object') {
        localStorage.setItem(key, JSON.stringify(value));
    }
    else {
        localStorage.setItem(key, value);
    }
}

var addCommas = function (input) {
    if (!input) { return input; } 
    // If the regex doesn't match, `replace` returns the string unmodified
    return (input.toString()).replace(
        // Each parentheses group (or 'capture') in this regex becomes an argument
        // to the function; in this case, every argument after 'match'
        /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function (match, sign, zeros, before, decimal, after) {

            // Less obtrusive than adding 'reverse' method on all strings
            var reverseString = function (string) { return string.split('').reverse().join(''); };

            // Insert commas every three characters from the right
            var insertCommas = function (string) {

                // Reverse, because it's easier to do things from the left
                var reversed = reverseString(string);

                // Add commas every three characters
                var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

                // Reverse again (back to normal)
                return reverseString(reversedWithCommas);
            };

            // If there was no decimal, the last capture grabs the final digit, so
            // we have to put it back together with the 'before' substring
            return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
        }
        );
};

var removeCommas = function (input) {
    return input.replace(/,/g, '');
};

function goto(url) {
    window.location.href = url;
}
var Tawk_API = Tawk_API || {},
Tawk_LoadStart = new Date();
(function () {
    var s1 = document.createElement('script'),
    s0 = document.getElementsByTagName('script')[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/59de1db4c28eca75e4625715/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})();
Tawk_API.visitor = {
    name: _spPageContextInfo.userDisplayName,
    email: _spPageContextInfo.userEmail,
};

function changeActiveMenu(el) {
    $('nav ul li').removeClass('active');
    $(el).addClass('active');
}

function fixIframe() {
    $('.MainIframe').contents().find('body').addClass('ms-fullscreenmode');
    $('.MainIframe')
        .contents()
        .find("#s4-ribbonrow,.od-SuiteNav,.Files-leftNav,.od-TopBar-header.od-Files-header,.pageHeader,.ql-editor,#titlerow .ms-table,.sitePage-uservoice-button,.footer")
        .hide()
        .css('display', 'none');
    $('.MainIframe').contents().find('#titlerow').css('background', 'none !important');
    $('.MainIframe').contents().find('.Files-mainColumn').css('left', '0');
    $('.MainIframe').contents().find('.Files-belowSuiteNav').css('top', '0px');
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function loadTabs() {
    $('ul.tabs li').click(function () {
        fixIframe();
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
    });
}

function getItemFromArrayById(arr, id) {
    var i = $.grep(arr, function (e, index) {
        return e.id === id;
    });
    if (i.length === 1) {
         return i; 
    } else { 
        return null;
    } 
}

function getIndexFromArrayByAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function getAttachmentLinks(Attachments) {
    var links = '';
    $.each(Attachments, function (i, j) {
        if (j.ServerRelativeUrl !== null) {
            links += '<a target=\'_blank\' href=\'' + j.ServerRelativeUrl + 
            '\'><span class=\'fa fa-paperclip\'></span>' + j.FileName + '</a>, ';
        }
    });
    return links;
}

function getDelAttachmentLinks(listname,d) {
    var links = '';
    $.each(d.AttachmentFiles, function (i, j) {
        if (j.ServerRelativeUrl !== null){
            links += '<a target=\'_blank\' href=\'' + j.ServerRelativeUrl +'\'>'+
             '<span class=\'fa fa-paperclip\'></span>' + j.FileName +'</a> &nbsp;&nbsp;'+
             '<i class="fa fa-trash text-danger" style="cursor:pointer" onclick="deleteAttachment(\'' + 
              listname + '\', '+d.Id+', \'' + j.FileName + '\')"></i>';
        }          
    });
    return links;
}

function getIM(id) {

    var im = getItemFromArrayById(ims, id);
   
    if (im) {
        return im[0].name;
    }
    else {
        return '';
    }
}

function loopFileUpload(listName, id, listValues, fileCountCheck) {
    var dfd = $.Deferred();
    uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment).then(
        function (data) {
            var objcontext = new SP.ClientContext();
            var targetList = objcontext.get_web().get_lists().getByTitle(listName);
            var listItem = targetList.getItemById(id);
            objcontext.load(listItem);
            objcontext.executeQueryAsync(
                function () {
                    fileCountCheck++;
                    if (fileCountCheck <= listValues[0].Files.length - 1) {
                        loopFileUpload(listName, id, listValues, fileCountCheck);
                    } else {
                        swal({
                            title: "Success!",
                            text: fileCountCheck + ' file(s) uploaded successfully.Reloading page...',
                            type: "success",
                        },
                       function (isConfirm) {
                           if (isConfirm) { location.reload(); }
                       });                        
                    }
                    dfd.resolve(fileCountCheck);
                },
            function (sender, args) {
                swal('Error', 'Error occured' + args.get_message(), 'error');
            });

        },
        function (sender, args) {
            console.log('Not uploaded');
            dfd.reject(sender, args);
        });
    return dfd.promise();
}


function uploadFile(listName, id, file) {
    var deferred = $.Deferred();
    var fileName = file.name;
    if (fileArray.length > 0) {
        if (fileArray[0].hasOwnProperty('fname')) {
            fileName = fileArray[0].fname + '_' + fileName;
            fileArray.shift();
        }
    }  

    getFileBuffer(file).then(
        function (buffer) {
            var bytes = new Uint8Array(buffer);
            var binary = '';
            for (var b = 0; b < bytes.length; b++) {
                binary += String.fromCharCode(bytes[b]);
            }
            var scriptbase = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/';
            console.log(' File size:' + bytes.length);
            $.getScript(scriptbase + 'SP.RequestExecutor.js', function () {
                var createitem = new SP.RequestExecutor(_spPageContextInfo.webServerRelativeUrl);
                createitem.executeAsync({
                    url: _spPageContextInfo.webServerRelativeUrl + 
                    '/_api/web/lists/GetByTitle(\'' + listName + '\')/items(' + 
                    id + ')/AttachmentFiles/add(FileName=\'' + file.name + '\')',
                    method: 'POST',
                    binaryStringRequestBody: true,
                    body: binary,
                    success: fsucc,
                    error: ferr,
                    state: 'Update'
                });
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    swal('error', fileName + ' not uploaded error', 'error');
                    deferred.reject(data);
                }
            });

        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}

function uploadFileSP(listName, id, file, fileName) {
    var deferred = $.Deferred();  

    getFileBuffer(file).then(
        function (buffer) {
            var bytes = new Uint8Array(buffer);
            var binary = '';
            for (var b = 0; b < bytes.length; b++) {
                binary += String.fromCharCode(bytes[b]);
            }
            var scriptbase = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/';
            console.log(' File size:' + bytes.length);
            $.getScript(scriptbase + 'SP.RequestExecutor.js', function () {
                var createitem = new SP.RequestExecutor(_spPageContextInfo.webServerRelativeUrl);
                createitem.executeAsync({
                    url: _spPageContextInfo.webServerRelativeUrl + '/_api/web/lists/GetByTitle(\'' +
                     listName + '\')/items(' + id + ')/AttachmentFiles/add(FileName=\'' + file.name + '\')',
                    method: 'POST',
                    binaryStringRequestBody: true,
                    body: binary,
                    success: fsucc,
                    error: ferr,
                    state: 'Update'
                });
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    swal('error', fileName + ' not uploaded error', 'error');
                    deferred.reject(data);
                }
            });

        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}

function uploadFileToLib(listName, file, fileName) {
    var deferred = $.Deferred();

    getFileBuffer(file).then(
        function (arrayBuffer) {
            var fileCollectionEndpoint = String.format(
               "{0}_api/web/getfolderbyserverrelativeurl('{1}')/files" +
               "/add(overwrite=true, url='{2}')",
               _spPageContextInfo.webAbsoluteUrl + '/', 
               _spPageContextInfo.webServerRelativeUrl + '/'+listName, fileName);

            $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                },
                success: fsucc,
                error:ferr
            });
           
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    swal('error', fileName + ' not uploaded error', 'error');
                    deferred.reject(data);
                }
            

        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}


function getFileBuffer(file) {
    var deferred = $.Deferred();
    var reader = new FileReader();
    reader.onload = function (e) {
        deferred.resolve(e.target.result);
    };
    reader.onerror = function (e) {
        deferred.reject(e.target.error);
    };
    reader.readAsArrayBuffer(file);
    return deferred.promise();
}


function getListItem(metaurl) {
    return $.ajax({
        url: metaurl,
        type: "GET",
        headers: { accept: "application/json;odata=verbose" }
    });
}

function setLink(category, link) {
    switch (category) {
       case 'Advert':
           $('.c_advert').find('span.text-danger').remove();
           $('.c_advert').append(link)
           break;
       case 'Bid Evaluation Report':
           $('.c_evaluation_rpt').find('span.text-danger').remove();
           $('.c_evaluation_rpt').append(link)
           break;
       case 'Bid Evaluation Panel':
           $('.c_evaluation_panel').find('span.text-danger').remove();
           $('.c_evaluation_panel').append(link)
           break;
       case 'Budget':
           $('.c_budget').find('span.text-danger').remove();
           $('.c_budget').append(link)
           break;
       case 'Call For Concept':
           $('.c_concept').find('span.text-danger').remove();
           $('.c_concept').append(link)
           break;
       case 'Call For Proposal':
           $('.c_proposal').find('span.text-danger').remove();
           $('.c_proposal').append(link)
           break;
       case 'Conflict of interest report':
           $('.c_conflict').find('span.text-danger').remove();
           $('.c_conflict').append(link)
           break;
       case 'Contract':
           $('.c_contract').find('span.text-danger').remove();
           $('.c_contract').append(link)
           break;
       case 'DDCA':
           $('.c_ddca').find('span.text-danger').remove();
           $('.c_ddca').append(link)
           break;
       case 'Other':
           $('.proc_other').find('span.text-danger').remove();
           $('.proc_other').append(link)
   }
}

function setPLink(category, link) {
   switch (category) {
       case 'Logframe':
           $('.c_logframe').find('span.text-danger').remove();
           $('.c_logframe').append(link)
           break;
       case 'Budget':
           $('.c_p_budget').find('span.text-danger').remove();
           $('.c_p_budget').append(link)
           break;
       case 'Other':
           $('.perf_other').find('span.text-danger').remove();
           $('.perf_other').append(link)
           break;
       case 'Risk Register':
           $('.c_risk_register').find('span.text-danger').remove();
           $('.c_risk_register').append(link)
           break;
       case 'Quarterly Report':
           $('.c_quarterly_report').find('span.text-danger').remove();
           $('.c_quarterly_report').append(link)
           break;
       case 'Investee Report':
           $('.c_investee_report').find('span.text-danger').remove();
           $('.c_investee_report').append(link)
           break;
       case 'Monthly Report':
           $('.c_monthly_report').find('span.text-danger').remove();
           $('.c_monthly_report').append(link)
   }
}
function loadHome() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('home_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'home.html', function (response, status, xhr) {
    cache('home_html', response);
    loadTabs();
    getMemoCards();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('home_html'));
  //    processHome();
  //}
}

function loadForms() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('forms_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'forms.html', function (response, status, xhr) {
    cache('forms_html', response);
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('forms_html'));
  //    processForms();
  //}
}

function loadPending() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('forms_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'pending.html', function (response, status, xhr) {
    // cache('pending_html', response);

  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('forms_html'));
  //    processForms();
  //}
}

function loadTasks() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('tasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'tasks.html', function (response, status, xhr) {
    cache('tasks_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('tasks_html'));
  //    processTasks();
  //}
}

function loadDocs() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('docs_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'docs.html', function (response, status, xhr) {
    cache('docs_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('docs_html'));
  //    processModForm();
  //}
}

function getReference() {
  var c_pillar_val = $('#pillar').val();
  var c_state_val = $('#state').val();
  if (c_state_val && c_pillar_val) {
    RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=Id&$filter=Federal_x0020_State eq \'' + 
    encodeURIComponent(c_state_val) + '\'', function (d) {
      var id = pad((d.results.length + 1) + '', 3);     
      var ct = 0;
      $.each(d.results,function(i,j){
        if(j.Pillar === c_pillar_val) {
          ct++;
        }
      });
      ct++;
      ct = pad(ct + '', 2);
      var ref = 'SSF-' + $('#state option:selected').attr('data-initial') +'-' +
        id +'-' + $('#pillar option:selected').attr('data-initial') + ct;
      $('#refno').text(ref);
      $('#refno-h').val(ref);
    });
  }
}

function editMemo() {
  $('#tab-2 select').prop('disabled', false).trigger('chosen:updated');
  $('.bbs_case_table .read').hide();
  $('.bbs_case_table .edit').show();
  $('#tab-2 input,#tab-2 textarea ').prop('disabled', false);
}

function prepDecModal() {

  $('.date').datetimepicker({ format: 'DD/MM/YYYY' });

  $('#datetimepicker1').on('dp.change', function (e) {
    $('#datetimepicker2').data('DateTimePicker').minDate(e.date);
  });

  $('.btn-next-second-form').click(function () {
    $('.first-form,.third-form, .fourth-form').hide();
    $('.second-form').fadeIn();
  });

  $('.btn-prev-first-form').click(function () {
    $('.second-form, .third-form, .fourth-form').hide();
    $('.first-form').fadeIn();
  });

  $('.btn-next-third-form').click(function () {
    $('.first-form,.second-form, .fourth-form').hide();
    $('.third-form').fadeIn();
  });

  $('.btn-prev-second-form').click(function () {
    $('.first-form,.third-form, .fourth-form').hide();
    $('.second-form').fadeIn();
  });


  $('.btn-next-fourth-form').click(function () {
    $('.first-form,.second-form,.third-form').hide();
    $('.fourth-form').fadeIn();
  });

  $('.btn-prev-third-form').click(function () {
    $('.second-form,.third-form, .fourth-form').hide();
    $('.third-form').fadeIn();
  });

  $('select').chosen();

  $('#fundceiling').bind('textchange', function (event, previousText) {
    const value = removeCommas($(this).val());
    $(this).val(addCommas(value));
  });


  $('input.upload').on('change', function () {
    const path = $(this).val(), filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });

  $('#decision-memo-form').on('hidden.bs.modal', function () {
    $('#decision-memo-form input,#decision-memo-form textarea').val('');
  });

  $('#decision-memo-form').modal('show');
}

function hideActionButtons() {
  var show = false;
  if ($.inArray('TL', user.access) !== -1 ||
        $.inArray('SO', user.access) !== -1 ||
        current_inv.im1_id === _spPageContextInfo.userId ||
        current_inv.im2_id === _spPageContextInfo.userId) {
    show = true;
  }
  if (!show) {
    $('.btn-primary').hide();
  }
}


function moreinfo(item, status) {
  if (status === 'More Info') {
    item.Status = status;
    status = 'Pending';
  }
  if (status === 'Rejected') {
    item.Approval_x0020_Status = status;
  }
  if (status === 'Approved' && $.inArray("SO", user.access) !== -1) {
    item.Approval_x0020_Status = status;
  }
  if ($.inArray("TL", user.access) !== -1) {
    item.TL_x0020_Comments = $('#comment').val();
    item.TLId = _spPageContextInfo.userId;
    item.TL_x0020_Date = moment().toISOString();
    item.TL_x0020_Approval = status;
  }
  if ($.inArray("SO", user.access) !== -1) {
    item.SO_x0020_Comments = $('#comment').val();
    item.SOId = _spPageContextInfo.userId;
    item.SO_x0020_Date = moment().toISOString();
    item.SO_x0020_Approval = status;
  }
  return item;
}

function successConfirm(msg, postFn) {
  swal({
    title: "Success!",
    text: msg,
    type: "success",
  },
  function (isConfirm) {
    if (isConfirm) {
      postFn();
      swal.close();
    }
  });
}


function getMemoCards() {
    validateDMemo();
  if (load === 'init') {
    load = 'loaded';
    $('.loaderwrapper').fadeOut();
    loadInvestments();
    loadDraftBMemos();
    loadDraftDecMemos();
  } else {
    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, 
      { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    var batchRequest = new BatchRequest();
    batchRequest.endpoint = memourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = mydrafturl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = draftdecmemourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDraftDecMemos' });
    batchExecutor.executeAsync().done(function (result) {
      $.each(result, function (k, v) {
        var command = $.grep(commands, function (command) {
          return v.id === command.id;
        });
        if (command[0].title === 'getMyDraftMemos') {
          getMyDraftMemos(v.result.result.value);
        } else if (command[0].title === 'getAprInvestments') {
          getHomeAprInvestments(v.result.result.value);
        } else if (command[0].title === 'getDraftDecMemos') {
          getDraftDecMemos(v.result.result.value);
        }
      });
    }).fail(function (err) {
      onError(err);
    });
  }
}

function getHomeAprInvestments(d) {
  investments = [];
  $.each(d, function (i, j) {
    var tlname = "", soname = "", tldate = "", sodate = "";
    if (j.SO) {
      soname = j.SO.Title;     
    }
    if(j.SO_x0020_Date){
      sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
    }
    if (j.TL) {
      tlname = j.TL.Title;       
    }
    if(j.TL_x0020_Date){
      tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
    }
    investments.push({
      refcode: j.Title,
      title: j.Title0,
      id: j.Id,
      im1: getIM(j.IM_x0028_1_x0029_Id),
      im2: getIM(j.IM_x0028_2_x0029_Id),
      im1_id: parseInt(j.IM_x0028_1_x0029_Id),
      im2_id: parseInt(j.IM_x0028_2_x0029_Id),
      pillar: j.Pillar,
      state: j.Federal_x0020_State,
      district: j.Districts || '',
      proposal: j.Source_x0020_of_x0020_Proposal,
      output2: j.Output2,
      output3: j.Output3,
      output: j.Output || '',
      est_fund: j.Est_x002e__x0020_Fund_x0020_ceil || '',
      inv_type: j.Investee_x0020_Type,
      proc_type: j.Proc_x002e__x0020_Type,
      proc_cycle: j.Proc_x002e__x0020_Cycle,
      prob_stat: j.Problem_x0020_Statement,
      just: j.Justification,
      rec: j.Recommendation,
      approval_status: j.Approval_x0020_Status,
      attachments: getAttachmentLinks(j.AttachmentFiles),
      socomments: j.SO_x0020_Comments,
      tlcomments: j.TL_x0020_Comments,
      so: soname,
      tl: tlname,
      tl_date: tldate,
      so_date: sodate,
      tl_approval: j.TL_x0020_Approval,
      so_approval: j.SO_x0020_Approval
    });
  });
  loadInvestments();
}

function loadInvestments() {
  var invContent = '';
  for (i = 0; i < investments.length; i++) {
    var inv = investments[i];
    if (i % 3 === 0) {
      invContent += '<div class="row">';
    }
    var title = inv.title;
    if(title){
      if(title.length>120){
        title = title.substring(0, 120) + " ...";
      }
    }
    invContent += '<div class="col-md-4">';
    invContent += '<div class="box-height" onclick="loadInvestment(\'' + inv.refcode + '\')">';
    invContent += '<div class="col-md-4 h-full div-counter">';
    invContent += '<p class="p-count">' + pad((i + 1) + '', 2) + '</p>';
    invContent += '</div>';
    invContent += '<div class="col-md-8 h-full div-inv">';
    invContent += '<p class="p-ref mb5">' + inv.refcode + '</p>';
    invContent += '<p class="p-title mb5">' + inv.title + '</p>';
    invContent += '<p class="p-im mb5"><b>IM(1):</b>   ' + inv.im1 +
         '</p><p class="m0 p-im"><b>IM(2):</b>   ' + inv.im2 + '</p></p>';
    invContent += '</div>';
    invContent += '</div>';
    invContent += '</div>';
    if ((i - 2) % 3 === 0) {
      invContent += '</div>';
    }
    else if (investments.length - 1 === i) {
      invContent += '</div>';
    }
  }
  $('.loaderwrapper').fadeOut();
  $('#inv-contain').empty().append(invContent);

  var uniqueim1 = [];
  for (var i= 0; i<investments.length; i++) {
    if(investments[i].im1){
      uniqueim1.push(investments[i].im1);
    }
    if(investments[i].im2){
      uniqueim1.push(investments[i].im2);
    }
    }

  uniqueim1 = $.unique(uniqueim1);
  uniqueim1.sort();
  $('#imfilter').append('<option val=""></option>');

  $(uniqueim1).each(function (i, j) {
    $('#imfilter').append('<option>' + j + '</option>');
  });

  $('select#statefilter').change(function () {
    var state = $(this);
    var selectedDistricts = $.grep(districts, function (element, index) {
      if ($(state).find('option:selected').attr('data-initial') === 'MR') {
        return true;
      } else {
        return element.state === $(state).find('option:selected').attr('data-initial');
      }
    });
    $('#districtfilter').empty();
    $('#districtfilter').append('<option val=""></option>');
    var content = '';
    for (var i = 0; i < selectedDistricts.length; i++) {
      content += '<option value="' + selectedDistricts[i].name + '">' + selectedDistricts[i].name + '</option>';
    }
    if(state === ''){
      for (var i = 0; i < districts.length; i++) {
          content += '<option value="' + districts[i].name + '">' + districts[i].name + '</option>';
      } 
    }
    $('#districtfilter').append(content).trigger('chosen:updated');
  });

  $('#attachFilesContainer input:file').MultiFile({ max: 4 });

  $("#invfilter select").chosen({ allow_single_deselect: true }).change(function () {
    filterInvestments();
  });
}

function filterInvestments() {
  var result = investments;
  if ($("#imfilter").val()) {
    result = investments.filter((el) =>
      typeof el.im1 === 'string' && el.im1.indexOf($("#imfilter").val()) > -1 ||
      typeof el.im2 === 'string' && el.im2.indexOf($("#imfilter").val()) > -1);
  }
  if ($("#statefilter").val()) {
    result = result.filter((el) =>
     typeof el.state === 'string' && el.state.indexOf($("#statefilter").val()) > -1);
  }
  if ($("#districtfilter").val()) {
    result = result.filter((el) =>
      typeof el.district === 'string' && el.district.indexOf($("#districtfilter").val()) > -1);
  }
  
  if ( $("#outputfilter").val() ) {
    result = result.filter((el) =>
      typeof el.output === 'string' && el.output.indexOf($("#outputfilter").val()) > -1);
  }
  loadfiltered(result);
}


function loadfiltered(results) {
  var invContent = '';
  for ( i = 0; i < results.length; i++ ) {
    var inv = results[i], title = inv.title;
    if(title){
      if(title.length>120){
        title = title.substring(0, 120) + " ...";
      }
    }
    if (i % 3 === 0) {
      invContent += '<div class="row">';
    }
    invContent += '<div class="col-md-4">';
    invContent += '<div class="box-height" onclick="loadInvestment(\'' + inv.refcode + '\')">';
    invContent += '<div class="col-md-4 h-full div-counter">';
    invContent += '<p class="p-count">' + pad((i + 1) + '', 2) + '</p>';
    invContent += '</div>';
    invContent += '<div class="col-md-8 h-full div-inv">';
    invContent += '<p class="p-ref mb5">' + inv.refcode + '</p>';
    invContent += '<p class="p-title mb5">' + title + '</p>';
    invContent += '<p class="p-im mb5"><b>IM(1):</b>   ' + inv.im1 + '</p><p class="m0 p-im"><b>IM(2):</b>   ' + inv.im2 + '</p></p>';
    invContent += '</div>';
    invContent += '</div>';
    invContent += '</div>';
    if ((i - 2) % 3 === 0) {
      invContent += '</div>';
    }
    else if (results.length - 1 === i) {
      invContent += '</div>';
    }
  }
  $('.loaderwrapper').fadeOut();
  $('#inv-contain').empty().append(invContent);

}

function getMemoForm() {
  $('#memo-form .modal-title').html('New Business Case Memo');
  $('#memo-form .save-memo').show();
  $('#memo-form .editatt').hide();

  $('#nextbcmemo').click(function () {
    $('#bcmemo').hide();
    $('#bcmemonext').removeClass('hidden').fadeIn();
  });
  $('#bcprevmemo').click(function () {
    $('#bcmemonext').hide();
    $('#bcmemo').removeClass('hidden').fadeIn();
  });
  $('.save-memo-draft').click(function () {
    $('#submit-type').val('Draft');
    $('#memo-form').formValidation('enableFieldValidators', 'investee_type', false);
    $('#memo-form').formValidation('enableFieldValidators', 'source_of_proposal', false);
    $('#memo-form').formValidation('enableFieldValidators', 'secondary_im', false);
    $('#memo-form').formValidation('enableFieldValidators', 'procurement_cycle', false);
    $('#memo-form').formValidation('enableFieldValidators', 'problem_statement', false);
    $('#memo-form').formValidation('enableFieldValidators', 'justification', false);
    $('#memo-form').formValidation('enableFieldValidators', 'recommendation_to_the_head_of_ssf', false);
    $("#memo-form").data('formValidation').validate();
  });

  $('.save-memo').click(function () {
    $('#submit-type').val('Final');
    $('#memo-form').formValidation('enableFieldValidators', 'investee_type', true);
    $('#memo-form').formValidation('enableFieldValidators', 'source_of_proposal', true);
    $('#memo-form').formValidation('enableFieldValidators', 'secondary_im', true);
    $('#memo-form').formValidation('enableFieldValidators', 'procurement_cycle', true);
    $('#memo-form').formValidation('enableFieldValidators', 'problem_statement', true);
    $('#memo-form').formValidation('enableFieldValidators', 'justification', true);
    $('#memo-form').formValidation('enableFieldValidators', 'recommendation_to_the_head_of_ssf', true);
    $("#memo-form").data('formValidation').validate();
  });


  $('#fundceiling').bind('textchange', function (event, previousText) {
    var value = removeCommas($(this).val());
    if ($.isNumeric(value)) {
      $(this).val(addCommas(value));
    } else {
      $(this).val('');
    }
  });

  $('#memo-form').modal('show');
    $("#memo-form").formValidation({
        framework: "bootstrap",
        excluded: ':disabled',
        err: { container: '.messages' },
        icon: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            title: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Title is required" }
                }
            },
            pillar: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Contract Type is required" }
                }
            },
            state: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Federal State is required" }
                }
            },
            district: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The District is required" },
                }
            },
            output: {
                row: '.col-md-3', validators: {
                    notEmpty: { message: "The Output is required" },
                }
            },
            estimated_fund_ceiling: { row: '.col-md-3', validators: { 
                notEmpty: { message: "The Estimated Fund is required" } } },
            investee_type: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Investee Type is required" }
                }
            },
            source_of_proposal: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Source Of Proposal is required" },
                }
            },
            secondary_im: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Secondary IM is required" },
                }
            },
            procurement_cycle: {
                row: '.col-md-3', enabled: false, validators: {
                    notEmpty: { message: "The Procurement Cycle is required" },
                }
            },
            procurement_type: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Procurement Type is required" },
                }
            },
            problem_statement: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Problem Statement is required" },
                }
            },
            justification: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Justification is required" },
                }
            },
            recommendation_to_the_head_of_ssf: {
                row: '.col-md-6', enabled: false, validators: {
                    notEmpty: { message: "The Recommendation to the head of SSF is required" },
                }
            },
        }
    }).on('success.form.fv', function (e) {
    e.preventDefault();
    if ($('#submit-type').val() === 'Draft') {
      saveMemoDraft();
    } else {
      saveMemo();
    }
  });
  commonBMemoDraftMemo();
  $("#primaryIM").val(_spPageContextInfo.userId).trigger("chosen:updated");
}

function saveMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit business case memo for review and approval?',
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    submitMemo();
  });
}

function saveMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: '<p>Only the Primary and Secondary IM can view</p>',
    html: true,
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Save as Draft',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    submitMemo();
  });
}

function submitMemo() {
  var data = [], fileArray = [];
  $('#attachFilesContainer input:file').each(function () {
    if ($(this)[0].files[0]) {
      fileArray.push({ Attachment: $(this)[0].files[0] });
    }
  });

  var dist = $('#district').val();

  if (dist !== null) {
    dist = dist.join(', ');
  } else {
    dist = '';
  }

  data.push({
    Title: $('#refno-h').val() || '',
    Pillar: $('#pillar').val(),
    Title0: $('#bCasetitle').val(),
    Federal_x0020_State: $('#state').val(),
    Districts: dist,
    Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
    Output2: $('#output2').val(),
    Output3: $('#output3').val(),
    Output: $('#output').val(),
    Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
    Investee_x0020_Type: $('#invtype').val(),
    Proc_x002e__x0020_Cycle: $('#procurement_cycle').val(),
    Proc_x002e__x0020_Type: $('.procurement_type:checked').val(),
    Problem_x0020_Statement: $('#probstat').val(),
    Justification: $('#justification').val(),
    Recommendation: $('#recomhead').val(),
    IM_x0028_1_x0029_: parseInt($('#primaryIM').val()),
    IM_x0028_2_x0029_: parseInt($('#secondaryIM').val()),
    Status: $('#submit-type').val(),
    Approval_x0020_Status: 'Pending',
    Files: fileArray
  });

  createBMemoWithAttachments('Business Case Memo', data).then(
    function () {},
    function (sender, args) {
      swal('Error', 'Error occured' + args.get_message());
    });


}

var createBMemoWithAttachments = function (listName, listValues) {
  var fileCountCheck = 0;
  var context = new SP.ClientContext.get_current();
  var dfd = $.Deferred();
  var targetList = context.get_web().get_lists().getByTitle(listName);
  context.load(targetList);
  var itemCreateInfo = new SP.ListItemCreationInformation();
  var listItem = targetList.addItem(itemCreateInfo);
  listItem.set_item('Title', listValues[0].Title);
  listItem.set_item('Pillar', listValues[0].Pillar);
  listItem.set_item('Title0', listValues[0].Title0);
  listItem.set_item('Federal_x0020_State', listValues[0].Federal_x0020_State);
  listItem.set_item('Districts', listValues[0].Districts);
  listItem.set_item('Source_x0020_of_x0020_Proposal', listValues[0].Source_x0020_of_x0020_Proposal);
  listItem.set_item('Output2', listValues[0].Output2);
  listItem.set_item('Output3', listValues[0].Output3);
  listItem.set_item('Output', listValues[0].Output);
  listItem.set_item('Est_x002e__x0020_Fund_x0020_ceil', listValues[0].Est_x002e__x0020_Fund_x0020_ceil);
  listItem.set_item('Investee_x0020_Type', listValues[0].Investee_x0020_Type);
  listItem.set_item('Proc_x002e__x0020_Cycle', listValues[0].Proc_x002e__x0020_Cycle);
  listItem.set_item('Proc_x002e__x0020_Type', listValues[0].Proc_x002e__x0020_Type);
  listItem.set_item('Problem_x0020_Statement', listValues[0].Problem_x0020_Statement);
  listItem.set_item('Justification', listValues[0].Justification);
  listItem.set_item('Recommendation', listValues[0].Recommendation);
  listItem.set_item('IM_x0028_1_x0029_', listValues[0].IM_x0028_1_x0029_);
  listItem.set_item('IM_x0028_2_x0029_', listValues[0].IM_x0028_2_x0029_);
  listItem.set_item('Status', listValues[0].Status);
  listItem.set_item('Approval_x0020_Status', listValues[0].Pending);
  listItem.update();
  context.executeQueryAsync(function () {
    var id = listItem.get_id();
    if (listValues[0].Files.length !== 0) {
      if (fileCountCheck <= listValues[0].Files.length - 1) {
        loopFileUpload(listName, id, listValues, fileCountCheck).then(
          function () { },
          function (sender, args) {
            console.log('Error uploading');
            dfd.reject(sender, args);
          });
      }
    } else {
      swal({
        title: "Success!",
        text: "Business Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) { location.reload(); }
      });
      dfd.resolve(fileCountCheck);
    }
  },
  function (sender, args) {
    swal('Error', 'Error occured' + args.get_message(), 'error');
  });
  return dfd.promise();
};

function updateSaveBMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: '<p>Only the Primary and Secondary IM can view</p>',
    html: true,
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Save as Draft',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#submit-type').val('Draft');
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    updateBMemo();
  });
}

function updateSaveBMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit business case memo for review and approval?',
    type: 'warning',
    showCancelButton: true,
    showLoaderOnConfirm: true,
    closeOnConfirm: false,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#submit-type').val('Final');
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    updateBMemo();
  });
}

function loadInvestment(refcode) {
  $('.loaderwrapper').fadeIn();
  //if(getCache('inv_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'investment.html', function (response, status, xhr) {
    cache('inv_html', response);
    processInvestment(refcode);
  });
}

function processInvestment(refcode) {
  $('.loaderwrapper').fadeOut();
  loadTabs();
  $('input.upload').on('change', function () {
    var path = $(this).val();
    var filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });
  var inv = $.grep(investments, function (element, index) {
    return element.refcode === refcode;
  })[0];
  current_inv = inv;
  loadbatchInv(current_inv);
  fixIframe();
}

function loadbatchInv(current_inv) {
  var commands = [];
  var batchExecutor = new RestBatchExecutor(
      _spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Decision Memo\')/items?$select=*,TL/Title,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_2_x0029_/Title&$expand=AttachmentFiles,SO,TL,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Ref_x002e_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'prepInvForms' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Procurement\')/items?$select=FileLeafRef,EncodedAbsUrl,Ref_x002e__x0020_Code,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getProcurement' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Modification Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName&$expand=AttachmentFiles&$filter=DMemoId eq \'' + current_inv.id + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getModMemos' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'Performance Management\')/items?$select=FileLeafRef,EncodedAbsUrl,Ref_x002e__x0020_Code,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPerformance' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = psitelst + '(\'CloseOut\')/items?$select=*,TL/Title,SO/Title&$expand=SO,TL&$filter=RefCode eq \'' + current_inv.refcode + '\'';
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getCloseOuts' });
  batchExecutor.executeAsync().done(function (result) {
    $.each(result, function (k, v) {
      var command = $.grep(commands, function (command) {
        return v.id === command.id;
      });
      if (command[0].title === 'prepInvForms') {
        prepInvForms(v.result.result.value);
      } else if (command[0].title === 'getProcurement') {
        getProcurement(v.result.result.value);
      } else if (command[0].title === 'getModMemos') {
        getModMemos(v.result.result.value);
      } else if (command[0].title === 'getPerformance') {
        getPerformance(v.result.result.value);
      } else if (command[0].title === 'getCloseOuts') {
        getCloseOuts(v.result.result.value);
      }
    });
  }).fail(function (err) {
    onError(err);
  });
}

function updateBMemo() {
  UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);

  var item = {
    __metadata: { type: 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
    Title: $('#refno-h').val(),
    Pillar: $('#pillar').val(),
    Title0: $('#bCasetitle').val(),
    Federal_x0020_State: $('#state').val(),
    Districts: $('#district').val().join(', '),
    Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
    Output2: $('#output2').val(),
    Output3: $('#output3').val(),
    Output: $('#output').val(),
    Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
    Investee_x0020_Type: $('#invtype').val(),
    Proc_x002e__x0020_Cycle: $('#procurement_cycle').val(),
    Proc_x002e__x0020_Type: $('.procurement_type:checked').val(),
    Problem_x0020_Statement: $('#probstat').val(),
    Justification: $('#justification').val(),
    Recommendation: $('#recomhead').val(),
    IM_x0028_1_x0029_Id: parseInt($('#primaryIM').val()),
    IM_x0028_2_x0029_Id: parseInt($('#secondaryIM').val()),
    Status: $('#submit-type').val(),
    Approval_x0020_Status: 'Pending',
  };
  fileArray = [];
  var listValues = [], listName = 'Business Case Memo', id = $('#bmemoid').val(), fileCountCheck = 0;

  $('#attachFilesContainer input:file').each(function () {
    if ($(this)[0].files[0]) {
      fileArray.push({ Attachment: $(this)[0].files[0] });
    }
  });

  updateJson(psitelst + '(\'' + listName + '\')/items(' + id + ')', item, success);
  function success(d) {
    delete item.__metadata;
    var dfd = $.Deferred();
    item.Files = fileArray;

    listValues.push(item);

    if (listValues[0].Files.length !== 0) {
      if (fileCountCheck <= listValues[0].Files.length - 1) {
        uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment).then(
          function (data) {
            fileCountCheck++;
            if (fileCountCheck <= listValues[0].Files.length - 1) {
              uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment);
            } else {
              swal({
                title: "Success!",
                text: "Business Memo submitted successfully.Refresh page",
                type: "success",
              },
              function (isConfirm) {
                if (isConfirm) { location.reload(); }
              });
            }
            dfd.resolve(fileCountCheck);
          },
          function (sender, args) {
            console.log('Not uploaded');
            dfd.reject(sender, args);
          });
      }
    }
    else {
      dfd.resolve(fileCountCheck);
      swal({
        title: "Success!",
        text: "Business Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) { location.reload(); }
      });
    }
    return dfd.promise();
  }
}

function loadDraftBMemos() {
  var s = '';
  $.each(draftmemos, function (i, j) {
    var btntext = 'Edit', btnclass = 'btn-primary';
    if (j.status === 'Final') {
      btntext = 'View';
      btnclass = 'btn-default';
    }
    s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' + 
    j.im1 + '</td><td>' + j.im2 + '</td><td>' + j.status + '</td><td>' + j.tl_approval + '</td><td>' +
        j.so_approval + '</td><td><a href="#" onclick="loadDraftBMemo(' + j.id + ')" class="btn ' + 
        btnclass + '">' + btntext + '</a></td></tr>';
  });
  tbdraftmemo = null;
  $('#tbdraftmemo>tbody').html(s);
  tbdraftmemo = $('#tbdraftmemo').DataTable({ responsive: true });
}

function validateDMemo(){
      
  $('#decision-memo-form .btn-success').click(function () {
    $('#submit-type').val('Final');
    $('#decision-memo-form').formValidation('enableFieldValidators', 'contact', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'introduction', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'key_activities', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'value_for_money', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'risk_assessment', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'sustainability_summary', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'conflict_sensitivity', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'recommendation', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'proposed_action', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'strategy_link', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'prob_desc', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'coordination', true);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'project_assets', true);
    $("#decision-memo-form").data('formValidation').validate();
  });

  $('#decision-memo-form .btn-primary').click(function () {
    $('#submit-type').val('Draft');
    $('#decision-memo-form').formValidation('enableFieldValidators', 'contact', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'introduction', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'key_activities', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'value_for_money', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'risk_assessment', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'sustainability_summary', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'conflict_sensitivity', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'recommendation', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'proposed_action', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'strategy_link', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'prob_desc', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'coordination', false);
    $('#decision-memo-form').formValidation('enableFieldValidators', 'project_assets', false);
    $("#decision-memo-form").data('formValidation').validate();
  });

    $("#decision-memo-form").formValidation({
        framework: "bootstrap",
        excluded: ':disabled',
        err: { container: '.messages' },
        icon: {
            valid: "glyphicon glyphicon-ok",
            invalid: "glyphicon glyphicon-remove",
            validating: "glyphicon glyphicon-refresh"
        },
        fields: {
            estimated_fund: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Actual Fund is required" },
                }
            },
            start_date: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The field is required" },
                    date: { format: 'DD/MM/YYYY', message: 'The value is not a valid date' }
                }
            },
            end_date: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The field is required" },
                    date: { format: 'DD/MM/YYYY', message: 'The value is not a valid date' }
                }
            },
            d_email: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Investee Email is required" },
                    emailAddress: { message: 'The value is not a valid email address' }
                }
            },
            phone_number: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Phone number is required" },
                    regexp: {
                        message: 'The phone number can only contain the digits, spaces, -, (, ), + and .',
                        regexp: /^[0-9\s\-()+\.]+$/
                    }
                }
            },
            contact: {
                row: '.col-md-4', validators: {
                    notEmpty: { message: "The Investee Address is required" }
                }
            },
            introduction: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The Introduction/Background is required" },
                }
            },
            key_activities: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            value_for_money: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            risk_assessment: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            sustainability_summary: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            prob_desc: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            conflict_sensitivity: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            proposed_action: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            coordination: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            strategy_link: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            project_assets: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
            recommendation: {
                row: '.col-md-12', enabled: false, validators: {
                    notEmpty: { message: "The field is required" }
                }
            },
        },
    }).on('success.form.fv', (e) => {
        e.preventDefault();
        if ($('#submit-type').val() === 'Final') {
            updateSaveDecMemo();
        } else {
            updateSaveDecMemoDraft();
        }
    });

}

function loadDraftDecMemos() {
  var s = '';
  $.each(draftdecmemos, function (i, j) {
    var btntext = 'Edit', btnclass = 'btn-primary';
    if (j.status === 'Final') {
      btntext = 'View';
      btnclass = 'btn-default';
    }
    s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' +
         j.startdate + '</td><td>' + j.enddate + '</td><td>' + j.im1 + '</td><td>' + j.im2 + '</td><td>' +
        j.status + '</td><td>' + j.tl_approval + '</td><td>' + j.so_approval + 
        '</td><td><a href="#" onclick="loadDraftDecMemo(' + j.id + ')" class="btn ' + btnclass + '">' +
         btntext + '</a></td></tr>';
  });
  $('#tbdraftdecmemo>tbody').html(s);
  $('#tbdraftdecmemo').DataTable({ responsive: true });  
}



function loadDraftBMemo(id) {
  commonBMemoDraftMemo();
  var d = getItemFromArrayById(draftmemos, id);
  if (d === null) { return;}
  d = d[0];

  $('#refno').text(d.refcode);
  $('#refno-h').val(d.refcode);
  $('#bmemoid').val(id);
  $('#bCasetitle').val(d.title);
  $('#state').val(d.state.split(','));
  $('#district').val(d.district.split(','));
  $('#proposalSrc').val(d.proposal);
  $('#output').val(d.output);
  $('#output2').val(d.output2);
  $('#output3').val(d.output3);
  $('#pillar').val(d.pillar);
  $('#fundceiling').val(d.est_fund);
  if ($.isNumeric(d.est_fund)) {
    $('#fundceiling').val(addCommas(d.est_fund));
  }
  $('#invtype').val(d.inv_type);
  $('#primaryIM').val(d.im1_id);
  $('#secondaryIM').val(d.im2_id);
  $('#procurement_cycle').val(d.proc_cycle);
  $('#b_tl_comment').html(d.tlcomments);
  $('#b_so_comment').html(d.socomments);
  $('input[name=procurement_type][value=\'' + d.proc_type + '\']').prop('checked', 'true');
  $('#hiddentext').html(d.prob_stat);
  var prob_stat = $('#hiddentext').text();
  $('#probstat').val(prob_stat);
  $('#hiddentext').html(d.just);
  var just = $('#hiddentext').text();
  $('#justification').val(just);
  $('#hiddentext').html(d.rec);
  var rec = $('#hiddentext').text();
  $('#recomhead').val(rec);
  createAttachmentLink(d);
  $('.update-memo').click(function () {
    updateSaveBMemo();
  });
  $('.save-memo-draft').click(function () {
    updateSaveBMemoDraft();
  });
  if (d.tldate !== '') {
    $(".editbmemo").show();
  } else {
    $(".editbmemo").hide();
  }

  $("body").on('click', '.delatt', function () {
    deleteAttachment('Business Case Memo', $('#bmemoid').val(), $(this).data('file'), $(this));
  });

  $('#memo-form select').trigger('chosen:updated');
  $('#memo-form .modal-title').html('Edit Business Case Memo');
  $('#memo-form .save-memo').hide();
  $('#memo-form .editatt').show();
  if (d.status === 'Final') {
    $("#memo-form input,#memo-form textarea,#memo-form select").prop("disabled", true);
    $('#memo-form .save-memo-draft,#memo-form .update-memo,#memo-form .delatt').hide();
  } else {
    $("#memo-form input,#memo-form textarea,#memo-form select").prop("disabled", false);
    $('#memo-form .save-memo-draft,#memo-form .update-memo,#memo-form .delatt').show();
  }

  $('#memo-form').modal('show');
}

function deleteAttachment(listname, id, filename, elem) {
  swal({
    title: 'Delete Attachment',
    text: 'Are you sure you want to delete this attachment?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
  },
  function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url: psitelst + '(\'' + listname + '\')/GetItemById(' + id +
         ')/AttachmentFiles/getByFileName(\'' + filename.trim() + '\')',
        type: 'Delete',
        contentType: 'application/json;odata=verbose',
        headers: {
          'X-RequestDigest': $('#__REQUESTDIGEST').val(),
          'X-HTTP-Method': 'Delete',
          'Accept': 'application/json;odata=verbose'
        },
        success: function (data) {
          swal({
            title: "Success!",
            text: 'Attachment removed successfully.',
            type: "success",
          },
          function (isConfirm) {
            if (isConfirm) { location.reload(); }
          });
        },
        error: onError
      });
    }
  });
}

function createAttachmentLink(d) {
  $('.editatt div').html(d.attachments);
  if (d.attachments.length > 0) {
    $('.editatt div a').html();
    $('.editatt div a').each(function (i, obj) {
      var p = $(obj).html();
      p = p.split('</span>');
      p = p[1].trim();
      $(this).after('<i class="fa fa-trash delatt text-danger" style="cursor:pointer" data-file="' + p + '"></i>');
    });
  }
}

function createDeleteAttachmentLink(d) {
  var link = "", att = d.attachments;
  if (att.length > 0) {
    var linkArr = att.split(","), newarr = [];
    if (linkArr[linkArr.length - 1] === 0) { linkArr.pop(); } 
    $.each(linkArr, function (i, j) {
      var linkbefore = j;
      j = $.parseHTML(j);
      var filename = $(j).text();
      linkbefore += '&nbsp;&nbsp;<i class="fa fa-trash delatt text-danger" style="cursor:pointer" data-file="' +
         filename + '"></i>';
      newarr.push(linkbefore);
    });
    link = newarr.join(' ');
    return link;
  }
}

function removeAttachment(filename, id) {
  var dmemo = getDraftBMemo(id);
  if (dmemo === null) { return; } 
  var d = dmemo[0];
  var links = d.attachments.split(",");
  for (var i = links.length - 1; i >= 0; i--) {
    var ind = links[i].indexOf(filename);
    if (ind !== -1 || links[i].trim() === '') {
      links.splice(i, 1);
    }
  }
  var datt = "", newdmemo = dmemo[0];
  if (links.length > 0) {
    datt = links.join(", ");
    newdmemo.attachments = datt;
  }

  var index = draftmemos.indexOf(dmemo);

  if (index !== -1) {
    draftmemos[index] = newdmemo;
  }

}

function loadDraftDecMemo(id) {
    var d = getItemFromArrayById(draftdecmemos, id);
    if (d === null) { return; }
    d = d[0];
    draftdecmemo = d;
    $('.bbs-ref').html(d.refcode);
    $('.bbs-title').html(d.title);
    $('.bbs-state').html(d.state);
    $('.bbs-districts').html(d.district);
    $('.bbs-im1').html(d.im1);
    $('.bbs-im2').html(d.im2);
    $('.bbs-pillar').html(d.pillar);
    $('.bbs-prop').html(d.proposal);
    $('.bbs-out').html(d.output);
    $('.bbs-fund').html(d.est_fund);
    $('.bbs-inv_type').html(d.inv_type);
    $('.bbs-proc_type').html(d.proc_type);
    $('.bbs-proc_cycle').html(d.proc_cycle);
    $('.bbs-prob').html(d.prob_stat);
    $('.bbs-just').html(d.just);
    $('.bbs-rec').html(d.rec);
    $('#tl_date').html(d.tldate);
    $('#so_date').html(d.sodate);
    $('.tlcomment_d').html(d.tl_comment);
    $('.socomment_d').html(d.so_comment);
    $('#phone').val(d.phone);
    $('.bbs-fund').val(d.est_fund);
    if ($.isNumeric(d.est_fund)) {
      $('.bbs-fund').val(addCommas(d.est_fund));
    }
    $('#email').val(d.email);
    $('#address').val(d.address);
    $('#risksummary').val(d.risk);
    $('#startdate').val(d.startdate);
    $('#enddate').val(d.enddate);
    $('#background').val(d.background);
    $('#keyact').val(d.activities);
    $('#valuemoney').val(d.valuemoney);
    $('#sustain').val(d.sustainability);
    $('#conflictsens').val(d.conflict);  
    $('#prob_desc').val($(d.prob_desc).text());
    $('#proposed_action').val($(d.proposed_action).text());
    $('#strategy_link').val($(d.strategy_link).text());
    $('#coordination').val($(d.coordination).text());
    $('#project_assets').val($(d.project_assets).text());
    $('#recomm').val(d.rec);
    $("#d_attachments").html(createDeleteAttachmentLink(d));
    //#TODO fix getting html 
  
    if (d.status === 'Final') {
      $("#decision-memo-form input,#decision-memo-form textarea,#decision-memo-form select").prop("disabled", true);
      $('#decision-memo-form .btn-primary,#decision-memo-form .btn-success,#decision-memo-form .delatt').hide();
    } else {
      $("#decision-memo-form input,#decision-memo-form textarea,#decision-memo-form select").prop("disabled", false);
      $('#decision-memo-form .btn-primary,#decision-memo-form .btn-success,#decision-memo-form .delatt').show();
    }
  
    $("body").on('click', '#d_attachments .delatt', function () {
      deleteAttachment('Decision Memo', draftdecmemo.id, $(this).data('file'), $(this));
    });
  
    prepDecModal();
  
  }


  function updateSaveDecMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    $('#submit-type').val('Final');
    submitUpdatedDecMemo();
  });
}

function updateSaveDecMemo() {
  swal({
    title: 'Are you sure?',
    text: 'You want to submit the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
    $('#submit-type').val('Final');
    submitUpdatedDecMemo();
  });
}

function updateSaveDecMemoDraft() {
  swal({
    title: 'Are you sure?',
    text: 'You want to save the decision case memo?',
    type: 'warning',
    showCancelButton: true,
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
    confirmButtonText: 'Submit',
    confirmButtonColor: '#5cb85c'
  }, function () {
    $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        
    if (draftdecmemo.status === "More Info") {
      $('#submit-type').val('More Info');
    }           
    else{
      $('#submit-type').val('Draft');
    }
    submitUpdatedDecMemo();
  });
}

function submitUpdatedDecMemo() {
  var item = {
    __metadata: { type: 'SP.Data.Decision_x0020_Memo_x0020_ListListItem' },
    Start_x0020_Date: moment($('#startdate').val() + ' ' + moment().format('hh:mm:ss'),
     'DD/MM/YYYY hh:mm:ss').toISOString(),
    End_x0020_Date: moment($('#enddate').val() + ' ' + moment().format('hh:mm:ss'),
     'DD/MM/YYYY hh:mm:ss').toISOString(),
    Email: $('#email').val(),
    Phone: $('#phone').val(),
    Address: $('#address').val(),
    Background: $('#background').val(),
    Key_x0020_Activities: $('#keyact').val(),
    Value_x0020_for_x0020_money: $('#valuemoney').val(),
    Risk_x0020_Summary: $('#risksummary').val(),
    Sustainability: $('#sustain').val(),
    Conflict_x0020_Sensitivity: $('#conflictsens').val(),
    Recommendation: $('#recomm').val(),
    Problem_x0020_Description: $('#prob_desc').val(),
    Proposed_x0020_Action: $('#proposed_action').val(),
    Strategy_x0020_Link: $('#strategy_link').val(),
    Coordination: $('#coordination').val(),
    Project_x0020_Assets: $('#project_assets').val(),
    Approval_x0020_Status: 'Pending',
    Status: $('#submit-type').val()
  };
   
  var listName = 'Decision Memo', fileArray = [];

  if ($('#evaluation_report').val() !== '') {
    fileArray.push({ Attachment: $('#evaluation_report')[0].files[0], fname: 'evaluation_report' });
  }

  updateJson(psitelst + '(\'' + listName + '\')/items(' + draftdecmemo.id + ')', item, success);
  var id = draftdecmemo.id;
  function success(d) {
    if (fileArray.length === 0) {

      swal({
        title: "Success!",
        text: "Decision Memo submitted successfully.Refresh page",
        type: "success",
      },
      function (isConfirm) {
        if (isConfirm) {
          $("#decision-memo-form").modal('hide');
          setTimeout(function () { loadHome(); }, 1000);
        }
      });

    } else {

      var dfd = $.Deferred().resolve();

      fileArray.forEach(function (f) {
        dfd = dfd.then(function () {
          return uploadFileSP(listName, id, f.Attachment, draftdecmemo.refcode + '_' + f.fname);
        });
      });

      dfd.done(function () {
        swal({
          title: "Success!",
          text: "Decision Memo submitted successfully.Refresh page",
          type: "success",
        },
        function (isConfirm) {
          if (isConfirm) {
            $("#decision-memo-form").modal('hide');
            setTimeout(function () { loadHome(); }, 1000);
          }
        });
      });
    }
  }
}

function commonBMemoDraftMemo() {
  $('#primaryIM,#secondaryIM').empty();
   $(ims).each(function (index, value) {
    $('#primaryIM,#secondaryIM').append('<option value="' + value.id + '">' + value.name + '</option>');
  }); 
  $('select#state').change(function () {
    var state = $(this);
    var selectedDistricts = $.grep(districts, function (element, index) {
      if ($(state).find('option:selected').attr('data-initial') === 'MR') {
        return true;
      } else {
        return element.state === $(state).find('option:selected').attr('data-initial');
      }
    });
    $('select#district').empty();
    var content = '';
    for (var i = 0; i < selectedDistricts.length; i++) {
      content += '<option value="' + selectedDistricts[i].name + '">' + selectedDistricts[i].name + '</option>';
    }
    $('#district').append(content).trigger('chosen:updated');
  });
  $('#nextbcmemo').click(function () {
    $('#bcmemo').hide();
    $('#bcmemonext').removeClass('hidden').fadeIn();
  });
  $('#bcprevmemo').click(function () {
    $('#bcmemonext').hide();
    $('#bcmemo').removeClass('hidden').fadeIn();
  });
  $('#state').change(function () {
    getReference();
  });
  $('#pillar').change(function () {
    getReference();
  });

  $('input.upload').on('change', function () {
    var path = $(this).val();
    var filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });
  $('select').chosen();
  $('#memo-form').on('hidden.bs.modal', function () {
    $(this).data('modal', null);
    $("#memo-form input,#memo-form textarea").val("");
    $("#memo-form select").val("").trigger('chosen:updated');
    $("#refno").html("");
    $("#bcmemo").show();
    $('#bcmemonext').hide();
    $('#memo-form input:file').MultiFile('reset');
  });
}



function loadbatch() {
    var commands = [];
    var batchExecutor = new RestBatchExecutor(
            _spPageContextInfo.webAbsoluteUrl, {
            'X-RequestDigest': $('#__REQUESTDIGEST').val() 
        });
    var batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Approvers List\')/items?$select=Title&$filter=UserId eq ' + 
        _spPageContextInfo.userId;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApproverlst' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(7)/users'; //49
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getIMs' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(8)/users';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getUsers' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Focal Person\')/items?$select=User/Id,User/Title&$expand=User';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getFocals' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = mydrafturl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = draftdecmemourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDraftDecMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'TR Locations\')/items?$select=Title';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getLocations' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = memourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Leave Requests\')/items?$filter=(Status eq \'Approved\') and (AuthorId eq ' +
         _spPageContextInfo.userId + ')';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApprovedLeave' });

    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });
            if (command[0].title === 'getApproverlst') {
                getApproverlst(v.result.result.value);
            } else if (command[0].title === 'getIMs') {
                getIMs(v.result.result.value);
            } else if (command[0].title === 'getUsers') {
                getUsers(v.result.result.value);
            } else if (command[0].title === 'getMyDraftMemos') {
                getMyDraftMemos(v.result.result.value);
            } else if (command[0].title === 'getDraftDecMemos') {
                getDraftDecMemos(v.result.result.value);
            } else if (command[0].title === 'getFocals') {
                getFocals(v.result.result.value);
            } else if (command[0].title === 'getLocations') {
                getLocations(v.result.result.value);
            } else if (command[0].title === 'getAprInvestments') {
                getAprInvestments(v.result.result.value);
            } else if (command[0].title === 'getApprovedLeave') {
                getApprovedLeave(v.result.result.value);
            }
        });
    }).fail(function (err) {
        onError(err);
    });
}

function getApproverlst(d) {
    $.each(d, function (i, j) {
        if (j.Title) {
            approver = true;
            user.access.push(j.Title);
        }
    });
    if (approver) {
        $('.menu-tasks').removeClass('hidden');
    }
    else {
        $('.menu-tasks').addClass('hidden');
    }
}

function getMyDraftMemos(d) {
    draftmemos = [];
    $.each(d, function (i, j) {
        var soid = null, tlid = null, tlname = "", soname = "", tldate = "", sodate = "";

        if (j.SO) {
            soid = j.SO.Id;
            soname = j.SO.Title;
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }

        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TL.Title;
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
        }

        draftmemos.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            status: j.Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            so: soname,
            so_id: soid,
            tl: tlname,
            tl_id: tlid,
            tl_date: tldate,
            so_date: sodate,
            so_approval: j.SO_x0020_Approval,
            tl_approval: j.TL_x0020_Approval
        });
    });
    loadDraftBMemos();
}

function getDraftDecMemos(d) {
    draftdecmemos = [];
    $.each(d, function (i, j) {        
        draftdecmemos.push({
            refcode: j.Ref_x002e_Code,
            title: j.Title,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            background: j.Background,
            state: j.Federal_x0020_State,
            district: j.District,
            proposal: j.Source_x0020_of_x0020_Proposal,
            activities: j.Key_x0020_Activities,
            email: j.Email,
            phone: j.Phone,
            address: j.Address,
            valuemoney: j.Value_x0020_for_x0020_money,
            risk: j.Risk_x0020_Summary,
            sustainability: j.Sustainability,
            conflict: j.Conflict_x0020_Sensitivity,
            status: j.Status,
            output: j.Output,
            output2: j.Output2,
            output3: j.Output3,
            startdate: moment(j.Start_x0020_Date).format('DD/MM/YYYY'),
            enddate: moment(j.End_x0020_Date).format('DD/MM/YYYY'),
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            tl_approval: j.TL_x0020_Approval,
            tl_comment: j.TL_x0020_Comments,
            so_approval: j.SO_x0020_Approval,
            so_comment: j.SO_x0020_Comments,
            proposed_action:j.Proposed_x0020_Action,
            strategy_link: j.Strategy_x0020_Link,
            prob_desc: j.Problem_x0020_Description,
            coordination: j.Coordination,
            project_assets: j.Project_x0020_Assets,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            comments: j.Comments
        });
    });
    loadDraftDecMemos();
}

function getUsers(d) {
    $.each(d, function (i, j) {
        users.push({ name: j.Title, id: j.Id });
    });
}

function getFocals(d) {
    $.each(d, function (i, j) {
        focals.push({ name: j.User.Title, id: j.User.Id });
    });
}

function getIMs(d) {
    ims = [];
    $.each(d, function (i, j) {
        ims.push({ id: j.Id, name: j.Title });
    });
    ims = ims.sort(function(a, b){
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
    }); 
}

function getLocations(d) {
    locations = [];
    $.each(d, function (i, j) {
        locations.push(j.Title);
    });
}

function getAprInvestments(d) {
    investments = [];
    $.each(d, function (i, j) {
        var tlname = "", soname = "", sodate="", tldate="",tlid =null, soid=null;
        if (j.SO) {
            soid = j.SO.Id;
            soname = j.SO.Title;
        }
        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TLTitle;
        }
        if(j.SO_x0020_Date){
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }

        if(j.TL_x0020_Date){
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
        }
        investments.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            so_id: soid,
            tl_id: tlid,
            so: soname,
            tl: tlname,
            tl_date: tldate,
            so_date: sodate,
            tl_approval: j.TL_x0020_Approval,
            so_approval: j.SO_x0020_Approval
        });

    });
    loadHome();
}

function getApprovedLeave(d) {
    $.each(d, function (i, j) {
        var app_days = j.Dates.split(', ');
        var app_day = null;
        $(app_days).each(function (idx, val) {
            app_day = moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD');
            approved_leave_days.push({
                id: app_day,
                title: 'Leave',
                allDay: true,
                start: app_day,
                className: 'hidden'
            });
        });
    });
}

$(document).ready(function () {
    $('br').remove();
    $('.menu-home').click(function () {
        loadHome();
    });
    $('.menu-forms').click(function () {
        loadForms();
    });
    $('.menu-tasks').click(function () {
        loadTasks();
    });
    $('.menu-pending').click(function () {
        loadPending();
    });
    $('.menu-doc-colab').click(function () {
        loadDocs();
    });

    $('nav ul li').click(function () {
        changeActiveMenu($(this));
    });
    loadbatch();
    setTimeout(function(){renewDigest();},177000);
});

function processForms() {
    loadTabs();
    $('.loaderwrapper').fadeOut();
    $(users).each(function (index, value) {
        if (value.id !== _spPageContextInfo.userId) {
            $('#supervisor').append('<option value="' + value.id + '">' + value.name + '</option>');
        }
    });

    $(focals).each(function (index, value) {
        $('#focal').append('<option value="' + value.id + '">' + value.name + '</option>');
    });

    $(locations).each(function (i, v) {
        $('select.dest').append('<option>' + v + '</option>');
    });

    var approved_leave_days_current_month = $.grep(approved_leave_days, function (a_day) {
        return moment(a_day.start, 'YYYY-MM-DD').format('M') === moment().format('M');
    });

    $('.days-leave-taken-current-month').text(pad(approved_leave_days_current_month.length + '', 2));
    $('.days-leave-taken').text(pad(approved_leave_days.length + '', 2));

    $('.btn-submit-leave').click(function () {
        $('#leave-form').data('formValidation').validate();
        if ($('#leave-form').data('formValidation').isValid()) {
            if (leave_days.length > 0) {
                saveLeave();
            }
            else {
                swal('Error!', 'You must select at least one day on the calendar', 'error');
            }
        }
    });

    $('.btn-submit-travel').click(function () {
        $('#travel-form').data('formValidation').validate();
        var fv = $('#travel-form').data('formValidation');
        if ($('#dest3').val()) {
            fv.enableFieldValidators('date3', true).revalidateField('date3');
        }
        else {
            fv.enableFieldValidators('date3', false).revalidateField('date3');
        }
        if ($('#dest4').val()) {
            fv.enableFieldValidators('date4', true).revalidateField('date4');
        }
        else {
            fv.enableFieldValidators('date4', false).revalidateField('date4');
        }

        if ($('#travel-form').data('formValidation').isValid()) {
            saveTravel();
        }
    });

    validateLeaveForm();
    validateTravelForm();

    $('select').chosen({
        allow_single_deselect: true
    });


    $('#datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change', function (e) {
        $('#datetimepicker1').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker2').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker3').data('DateTimePicker').minDate(e.date);
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    $('#datetimepicker1,#datetimepicker2,#datetimepicker3').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false,
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change dp.show', function (e) {
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    loadCalendar();
}

processForms();

function saveTravel() {
    swal({
        title: 'Travel Request',
        text: 'You want to submit the travel request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitTravel();
    });
}

function submitTravel(){

    var item = {
        __metadata: { type: 'SP.Data.TRListItem' },
        Title: 'Travel Request',
        Purpose: $('#purpose').val(),
        Dest_x0020_1: $('#dest1').val(),
        Date_x0020_1: moment($('#date1').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        Dest_x0020_2: $('#dest2').val(),
        Date_x0020_2: moment($('#date2').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        Destinations: $('#dests').val(),
        Details_x0020_of_x0020_host: $('#host-details').val(),
        Flights: $('#flights').val(),
        Accommodation: $('#accommodation').val(),
        Internal_x0020_Transport: $('#transport').val(),
        Exta_x0020_Luggage: $('#luggage').val(),
        Other_x0020_info: $('#other-info').val(),
        Focal_x0020_PersonId: $('#focal').val()
    };

    if($('#dest3').val()){
        item.Dest_x0020_3 =  $('#dest3').val();
        item.Date_x0020_3 =  moment($('#date3').val()+' '+moment().format('hh:mm:ss'),
            'DD/MM/YYYY hh:mm:ss').toISOString();
    }
    if($('#dest4').val()){
        item.Dest_x0020_4 =  $('#dest4').val();
        item.Date_x0020_4 =  moment($('#date4').val()+' '+moment().format('hh:mm:ss'),
            'DD/MM/YYYY hh:mm:ss').toISOString();
    }

    postJson(psitelst + '(\'TR\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('input').val('');
        $('#travel-form').data('formValidation').resetForm();
        swal('Success','Travel Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function validateLeaveForm() {
    $('#leave-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },
            leaveType: {
                validators: {
                    callback: {
                        message: 'The leave type field is required',
                        callback: function(value, validator, $field) {
                            return $('#leave-type').val() !== '';
                        }
                    }
                }
            },
            supervisor: {
                validators: {
                    callback: {
                        message: 'The supervisor field is required',
                        callback: function(value, validator, $field) {
                            return $('#supervisor').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function validateTravelForm() {
    $('#travel-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            purpose: {
                validators: {
                    notEmpty: {
                        message: 'The purpose is required'
                    }
                }
            },
            dest1: {
                validators: {
                    callback: {
                        message: 'The destination 1 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date1: {
                validators: {
                    notEmpty: {
                        message: 'The date 1 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 1 field is not a valid date'
                    }
                }
            },
            dest2: {
                validators: {
                    callback: {
                        message: 'The destination 2 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date2: {
                validators: {
                    notEmpty: {
                        message: 'The date 2 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 2 field is not a valid date'
                    }
                }
            },
            date3: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 3 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 3 field is not a valid date'
                    }
                }
            },
            date4: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 4 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 4 field is not a valid date'
                    }
                }
            },
            dests: {
                validators: {
                    notEmpty: {
                        message: 'The destinations are required'
                    }
                }
            },
            hostDetails: {
                validators: {
                    notEmpty: {
                        message: 'The host details are required'
                    }
                }
            },
            flights: {
                validators: {
                    notEmpty: {
                        message: 'The flights required field is required'
                    }
                }
            },
            accommodation: {
                validators: {
                    notEmpty: {
                        message: 'The accommodation required field is required'
                    }
                }
            },
            transport: {
                validators: {
                    notEmpty: {
                        message: 'The internal transport required field is required'
                    }
                }
            },
            luggage: {
                validators: {
                    notEmpty: {
                        message: 'The extra luggage to be carried field is required'
                    }
                }
            },
            focal: {
                validators: {
                    callback: {
                        message: 'The focal person is required',
                        callback: function(value, validator, $field) {
                            return $('#focal').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function loadCalendar(){
    $('#calendar').fullCalendar({
        header: {
            left: '',
            center: 'title',
            right: 'prev,next'
        },
        events: approved_leave_days,
        height: 500,
        fixedWeekCount:false,
        dayClick: function(date, jsEvent, view) {
            var check = moment(date);
            var today = moment.now();
            if(check > today && !isLeaveDay(date.format()))
            {
                if(leave_days.indexOf(date.format()) === -1){
                    leave_days.push(date.format());
                    $('#calendar').fullCalendar( 'addEventSource',[
                    {
                        id: date.format(),
                        title: 'Leave',
                        allDay: true,
                        start: date.format(),
                        className: 'hidden'
                    }
                    ]
                    );
                }
                else{
                    leave_days.splice(leave_days.indexOf(date.format()), 1);
                    $(this).removeClass('leave-day-selected');
                    var index = $(this).index();
                    $(this).closest('.fc-bg').next().find('table thead td:eq('+index+')').css('color','#000');
                    $('#calendar').fullCalendar( 'removeEvents', date.format());
                }
                $('.days-leave').text(pad(leave_days.length+'',2));
            }
        },
        eventAfterRender: function(event, element) {
            var index = element.parent().index(); 
            element
                .parent()
                .closest('.fc-content-skeleton')
                .prev()
                .find('table tbody tr td:eq('+index+')')
                .addClass('leave-day-selected');
            element.parent().closest('tbody').prev().find('tr td:eq('+index+')').css('color','#fff');
        }
    });
}


function saveLeave() {
    swal({
        title: 'Leave Request',
        text: 'You want to submit the leave request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitLeave();
    });
}

function submitLeave(){
    leave_days.sort(function(a, b){
        return moment(a,'YYYY-MM-DD') - moment(b,'YYYY-MM-DD');
    });

    leave_days = $.map(leave_days, function(val,i){
        return [moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY')];
    });

    var item = {
        __metadata: { type: 'SP.Data.Leave_x0020_RequestsListItem' },
        SupervisorId: parseInt($('#supervisor').val()),
        Days: leave_days.length,
        Dates: leave_days.join(', '),
        Description: $('#description').val()
    };
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/web/lists/getbytitle(\'Leave Requests\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('#leave-form').data('formValidation').resetForm();
        reloadCalendar();
        swal('Success','Leave Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function reloadCalendar(){
    $('.days-leave').text('00');
    leave_days = [];
    $('#calendar').fullCalendar('destroy');
    loadCalendar();
}

function isLeaveDay(l_day){
    var is_leave_day = $.grep(approved_leave_days, function( a_day ) {
        return a_day.start === l_day;
    });
    return is_leave_day.length > 0;
}