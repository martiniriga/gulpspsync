function getMemoList(){    
    investments = [],allinvestments = [];
    $('.loaderwrapper').fadeIn();
    var bctaskurl = 'lists/GetByTitle(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Status eq \'Final\'';
    if ( $.inArray("SO", user.access) !== -1 ) {
        bctaskurl += " and SO_x0020_Approval eq 'Pending' and TL_x0020_Approval eq 'Approved'";
    }
    if ( $.inArray("TL", user.access) !== -1 ) {
        bctaskurl += "and TL_x0020_Approval eq 'Pending'";
    }

    RestCalls(bctaskurl,
    	function(d){
    	    $.each(d.results, function (i, j) {
    	        var attachments = "",tlcomments="",socomments="";
    	        if (j.Attachments) {
    	            attachments = getAttachmentLinks(j.AttachmentFiles.results);
                }
                if(j.SO_x0020_Comments){
                    socomments = j.SO_x0020_Comments;
                }
                if(j.TL_x0020_Comments){
                    tlcomments = j.TL_x0020_Comments;
                }
    	        allinvestments.push({
		            refcode: j.Title,
		            title: j.Title0,
		            id: j.Id,
		            im1: getIM(j.IM_x0028_1_x0029_Id),
		            im2: getIM(j.IM_x0028_2_x0029_Id),
		            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
		            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
		            pillar: j.Pillar,
		            state: j.Federal_x0020_State,
		            district: j.Districts,
		            proposal: j.Source_x0020_of_x0020_Proposal,
		            output2: j.Output2,
		            output3: j.Output3,
		            output: j.Output,
		            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
		            inv_type: j.Investee_x0020_Type,
		            proc_type: j.Proc_x002e__x0020_Type,
		            proc_cycle: j.Proc_x002e__x0020_Cycle,
		            prob_stat: j.Problem_x0020_Statement,
		            just: j.Justification,
		            rec: j.Recommendation,
		            approval_status:j.Approval_x0020_Status,
		            attachments: attachments,
		            tl_approval: j.TL_x0020_Approval,
                    so_approval: j.SO_x0020_Approval,
                    socomments: socomments,
                    tlcomments: tlcomments
    	        });
		    });
             populateMemo();  
        });    
}

getMemoList();

function populateMemo(){
    $('.loaderwrapper').fadeOut();    
    var s='';
    $.each(allinvestments, function (i, j) {        
        s += '<tr><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td>'+j.output+'</td><td><a href="#" data-id="'+j.id+'" class="btn btn-orange">Review</a></td></tr>';
    });
    $('#tbcase>tbody').html(s);
    tbcase=null;
    tbcase= $('#tbcase').DataTable({responsive: true});

    $('body').on('click', '#tbcase a', function () {
        var id=$(this).data('id');
        trform= getInvestment(id)[0]; 

        if(trform.district.indexOf(",") === -1){
            $('#district').val(trform.district);
        }else{            
            $('#district').val($.map(trform.district.split(','), $.trim));
        }

        $('.bbs-ref').text(trform.refcode);
        $('#bbs-title').val(trform.title);
        $('#bbs-title1').text(trform.title);
        $('#state').val(trform.state);
        $('#primaryIM').val(trform.im1_id);
        $('#secondaryIM').val(trform.im2_id);
        $('#proposalSrc').val(trform.proposal);
        $('#output3').val(trform.output3);
        $('#output2').val(trform.output2);
        $('#output').val(trform.output);
        $('#fundceiling').val(addCommas(trform.est_fund));
        $('#inv_type').val(trform.inv_type);
        $('input[name=procurement_type][value=\'' + trform.proc_type + '\']').prop('checked', true);
        $('#proc_cycle').val(trform.proc_cycle);
        $('#tl').html(trform.tl);
        $('#tl_date').html('n/a');
        $('#bbs-tlcomment').html(trform.tlcomments);
        $('#so').html(trform.so);
        $('#so_date').html('n/a');
        $('#bbs-socomment').html(trform.socomments);
        $('#hiddentext').html(trform.prob_stat);
        var prob_stat =$('#hiddentext').text();
        $('#prob_stat').val(prob_stat);
        $('#hiddentext').html(trform.just);
        var just= $('#hiddentext').text();
        $('#just').val(just);
        $('#hiddentext').html(trform.rec);      
        var rec =$('#hiddentext').text();
        $('#rec').val(rec);
        $('#attachments').html(trform.attachments);
        $('.bbs_case_table select').chosen();
        $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click'); 
        $('.bbs_case_table textarea').each(function() {
            $(this).height($(this).prop('scrollHeight'));
        });   
        if ( trform.status === 'Approved' ) {
            $('.bbs_case_table button.btn').hide();
        }          
    });
}

function getInvestment(id){
    
    var i =  $.grep(allinvestments, function (e, index) {
        return e.id === id;
    });

    if(i.length === 1) {
        return i;
    }    
    else { 
        return null;
    }    
}

function submitApproveMemo() {
    if ( user.access.length === 0 ) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }
    var item = {
        __metadata: { type: 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        Title0: $('#bbs-title').val(),
        Federal_x0020_State: $('#state').val(),
        Districts: $('#district').val().join(', '),
        Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
        Output: $('#output').val(),
        Output2: $('#output2').val(),
        Output3: $('#output3').val(),
        Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
        Investee_x0020_Type: $('#inv_type').val(),
        Proc_x002e__x0020_Cycle: $('#proc_cycle').val(),
        Proc_x002e__x0020_Type: $('.proc_type:checked').val(),
        Problem_x0020_Statement: $('#prob_stat').val(),
        Justification: $('#just').val(),
        Recommendation: $('#rec').val(),
        IM_x0028_1_x0029_Id: parseInt($('#primaryIM').val()),
        IM_x0028_2_x0029_Id: parseInt($('#secondaryIM').val()),
    };

    item = moreinfo(item, 'Approved');

    updateJson(psitelst + '(\'Business Case Memo\')/items(' + trform.id + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Business Case Memo approved successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadBMemoTasks(); }
      });
    }

}

function submitRejectMemo() {
    if ( user.access.length === 0 ) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }
    var item = { __metadata: { type: 'SP.Data.Business_x0020_Case_x0020_MemoListItem' } };
    item = moreinfo(item, 'Rejected');

    updateJson(psitelst + '(\'Business Case Memo\')/items(' + trform.id + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Business Case Memo rejected successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadBMemoTasks(); }
      });
    }

}

function savemoreInfoBMemo() {
    if ( $('#comment').val() === "" ) {
        swal("Error", "Kindly comment on what additional info you need in the comment box", "error");
        $("#comment").focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'Do you want to request for additional info regarding this business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        moreInfoBMemo();
    });
}

function moreInfoBMemo() {
    var item = {
        __metadata: { type: 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        Title0: $('#bbs-title').val(),
        Federal_x0020_State: $('#state').val(),
        Districts: $('#district').val().join(', '),
        Source_x0020_of_x0020_Proposal: $('#proposalSrc').val(),
        Output: $('#output').val(),
        Output2: $('#output2').val(),
        Output3: $('#output3').val(),
        Est_x002e__x0020_Fund_x0020_ceil: $('#fundceiling').val(),
        Investee_x0020_Type: $('#inv_type').val(),
        Proc_x002e__x0020_Cycle: $('#proc_cycle').val(),
        Proc_x002e__x0020_Type: $('.proc_type:checked').val(),
        Problem_x0020_Statement: $('#prob_stat').val(),
        Justification: $('#just').val(),
        Recommendation: $('#rec').val(),
        IM_x0028_1_x0029_Id: parseInt($('#primaryIM').val()),
        IM_x0028_2_x0029_Id: parseInt($('#secondaryIM').val()),
    };

    item = moreinfo(item, 'More Info');
    updateJson(psitelst + '(\'Business Case Memo\')/items(' + trform.id + ')', item, success);

    function success(data) {
        swal({
            title: "Success!",
            text: "Business Case Memo additional information requested",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadBMemoTasks(); }
      });
    }

    
}

function saveRejectMemo() {
    if ( $('#comment').val() === "" ) {
        swal("Error", "Kindly provide a reason in the comment box as to why the business case memo is being rejected", "error");
        $("#comment").focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'You want to reject the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitRejectMemo();
    });
}

function saveApproveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to Approve the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitApproveMemo();
    });
}



