function loadbatchTaskcount() {
  var deccturl = psitelst + '(\'Decision Memo\')/items?$select=Id&$filter=(Status eq \'Final\')';
  var bccturl = psitelst + '(\'Business Case Memo\')/items?$select=Id&$filter=Status eq \'Final\'';
  var modurl = psitelst + '(\'Modification Memo\')/items?$select=Id&$filter=';
  var closurl = psitelst + '(\'CloseOut\')/items?$select=Id&$filter=Approval_x0020_Status ne \'More Info\' and';
  var addurl = "",ourl="";
  if ( $.inArray("SO", user.access)!== -1 ) {
    addurl = ' and SO_x0020_Approval eq \'Pending\' and TL_x0020_Approval eq \'Approved\'';
    ourl = ' SO_x0020_Approval eq \'Pending\' and TL_x0020_Approval eq \'Approved\'';
  }
  if ( $.inArray("TL", user.access) !== -1 || $.inArray("KMCU", user.access) !== -1 ) {
    addurl = ' and TL_x0020_Approval eq \'Pending\'';
    ourl = ' TL_x0020_Approval eq \'Pending\'';
  }
  bccturl = bccturl + addurl;
  deccturl = deccturl + addurl;
  modurl = modurl + ourl;
  closurl = closurl + ourl;
  var commands = [];
  var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = deccturl;
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDecCount' });
  batchRequest = new BatchRequest();    
  batchRequest.endpoint = bccturl;
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMemoCount' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = modurl;
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getModMemoCount' });
  batchRequest = new BatchRequest();
  batchRequest.endpoint = closurl;
  batchRequest.headers = { accept: 'application/json;odata=nometadata' };
  commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getCloseOutCount' });

  batchExecutor.executeAsync().done(function (result) {
    $.each(result, function (k, v) {
      var command = $.grep(commands, function (command) {
        return v.id === command.id;
      });            
      if (command[0].title === 'getDecCount') {
        getDecCount(v.result.result.value);
      }else if (command[0].title === 'getMemoCount') {
        getMemoCount(v.result.result.value);
      }else if (command[0].title === 'getModMemoCount') {
        getModMemoCount(v.result.result.value);
      }else if (command[0].title === 'getCloseOutCount') {
        getCloseOutCount(v.result.result.value);
      }           
    });              
  }).fail(function (err) {
    onError(err);
  });
}
loadbatchTaskcount();

function getDecCount(d){
  $('#decmemo .info h4').text(d.length);
}

function getMemoCount(d){
  $('.loaderwrapper').fadeOut(); 
  $('#bcmemo .info h4').text(d.length);
}

function getModMemoCount(d){
  $('.loaderwrapper').fadeOut(); 
  $('#modmemo .info h4').text(d.length);
}

function getCloseOutCount(d){
  $('.loaderwrapper').fadeOut();
  if (d) {  $('#closeout .info h4').text(d.length);  }
}


function loadBMemoTasks(){
  $('.loaderwrapper').fadeIn();
  //if(getCache('bmtasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath+'bmemo.html',function(response, status, xhr) {
    cache('bmtasks_html', response);           
    loadTabs();
    readyForms();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('bmtasks_html'));
  //    processBForm();
  //}
}


function readyForms(){
  $(ims).each(function(index,value){
    $('#primaryIM,#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
  });   

  $('select#state').change(function() {
    var state = $(this);
    var selected_districts = $.grep(districts, function(element, index) {
      if ($(state).find('option:selected').attr('data-initial') === 'MR') {
        return true;
      } else {
        return element.state === $(state).find('option:selected').attr('data-initial');
      }
    });
    $('select#district').empty();
    var content = '';
    for (var i = 0; i < selected_districts.length; i++) {
      content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
    }
    $('#district').append(content).trigger('chosen:updated');
  });   

  $('#state,#pillar').change(function() {
    getReference();
  });    
}

function loadDecMemoTasks() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('dmtasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath+'dmemo.html',function(response, status, xhr) {
    cache('dmtasks_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('dmtasks_html'));
  //    loadTabs();
  //}
}

function loadModMemoTasks(){
  $('.loaderwrapper').fadeIn();
  //if(getCache('mmtasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath+'modmemo.html',function(response, status, xhr) {
    cache('mmtasks_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('mmtasks_html'));
  //    processModForm();
  //}
}

function loadCloseOutTasks() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('mmtasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'closeout.html', function (response, status, xhr) {
    cache('closeout_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('mmtasks_html'));
  //    processModForm();
  //}
}





