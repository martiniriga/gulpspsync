﻿var pdmemos = [], pbmemos = [], tbpendingbmemo = null, tbpendingdmemo = null;
UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
loadPendingBatch();
loadTabs();
nav();

function nav() {
    $('.tab-link').click(function () {
        if ($(this).data('tab') === "tab-1" || $(this).data('tab') === "tab-3") {
            $('[data-tab=\'tab-2\'],[data-tab=\'tab-4\']').addClass('hidden');
        }
    });
}

function loadPendingBatch() {
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Business Case Memo\')/items?$select=*,TL/Id,TL/Title,SO/Id,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=SO,TL,AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Status eq \'Final\' or Status eq \'More Info\')  and Approval_x0020_Status eq \'Pending\'';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPendingBMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Decision Memo\')/items?$select=*,TL/Id,TL/Title,SO/Id,SO/Title,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=SO,TL,AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Status eq \'Final\' or Status eq \'More Info\') and Approval_x0020_Status eq \'Pending\'';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPendingDMemos' });
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });
            if (command[0].title === 'getPendingBMemos') {
                getPendingBMemos(v.result.result.value);
            } else if (command[0].title === 'getPendingDMemos') {
                getPendingDMemos(v.result.result.value);
            }
        });
    }).fail(function (err) {
        onError(err);
    });
}

function getPendingBMemos(d) {
    $('.loaderwrapper').fadeOut();
    pbmemos = [];
    $.each(d, function (i, j) {
        var soid = null, tlid = null, tlname = "", soname = "", tldate = "", sodate = "";
        if (j.SO) {           
            soid = j.SO.Id;           
            soname = j.SO.Title;
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }
        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TL.Title;
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');           
        }
        pbmemos.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            tl_approval: j.TL_x0020_Approval,
            so_approval: j.SO_x0020_Approval,
            so_id: soid,
            so: soname,
            tl_id: tlid,
            tl: tlname,
            tl_date: tldate,
            so_date: sodate,
        });
    });
    loadpendingBMemoTable();
}

function loadpendingBMemoTable() {
    var s = '';
    $.each(pbmemos, function (i, j) {
        s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' + j.im1 + '</td><td>' + j.im2 + '</td><td>' + j.tl_approval + '</td><td>' + j.so_approval + '</td><td><a href="#" onclick="loadpendingBMemo(' + j.id + ')" class="btn btn-orange">View</a></td></tr>';
    });
    tbpendingbmemo = null;
    $('#tbpendingbmemo>tbody').html(s);
    tbpendingbmemo = $('#tbpendingbmemo').DataTable({ responsive: true });
}

function getPendingDMemos(d) {
    pdmemos = [];
    $.each(d, function (i, j) {
        var soid = null, tlid = null, tlname = "", soname = "", tldate = "", sodate = "",startdate="",enddate="";
        if (j.SO) {
            soid = j.SO.Id;
            soname = j.SO.Title;
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }
        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TL.Title;
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
        }
        if (j.Start_x0020_Date) {
            startdate = moment(j.Start_x0020_Date).format('DD/MM/YYYY');
        }
        if (j.End_x0020_Date) {
            enddate = moment(j.End_x0020_Date).format('DD/MM/YYYY');
        }
        pdmemos.push({
            refcode: j.Ref_x002e_Code,
            title: j.Title,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            address: j.Address,
            state: j.Federal_x0020_State,
            district: j.District,
            email: j.Email,
            phone: j.Phone,
            sustainability: j.Sustainability,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            risk: j.Risk_x0020_Summary,
            valuemoney: j.Value_x0020_for_x0020_money,
            conflict: j.Conflict_x0020_Sensitivity,
            background: j.Background,
            activities:j.Key_x0020_Activities,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            tl_approval: j.TL_x0020_Approval,
            so_approval: j.SO_x0020_Approval,
            so_id: soid,
            so: soname,
            tl_id: tlid,
            tl: tlname,
            tl_date: tldate,
            so_date: sodate,
            startdate: startdate,
            enddate:enddate
        });
    });
    loadpendingDMemoTable();
}

function loadpendingDMemoTable() {
    var s = '';
    $.each(pdmemos, function (i, j) {
        s += '<tr><td>' + j.title + '</td><td>' + j.district + '</td><td>' + j.state + '</td><td>' + j.startdate + '</td><td>' + j.enddate + '</td><td>' + j.im1 + '</td><td>' + j.im2 + '</td><td>' + j.tl_approval + '</td><td>' + j.so_approval + '</td><td><a href="#" onclick="loadpendingDMemo(' + j.id + ')" class="btn btn-default">View</a></td></tr>';
    });
    tbpendingdmemo = null;
    $('#tbpendingdmemo>tbody').html(s);
    tbpendingdmemo = $('#tbpendingdmemo').DataTable({ responsive: true });

}

function loadpendingBMemo(id) {
    var d = getItemFromArrayById(pbmemos, id);
    if (d === null) {return;}
    d = d[0];
    console.log(d);
    $('.bbs-ref').html(d.refcode);
    $('.bbs-title').html(d.title);
    $('.bbs-state').html(d.state);
    $('.bbs-districts').html(d.district);
    $('.bbs-im1').html(d.im1);
    $('.bbs-im2').html(d.im2);
    $('.bbs-pillar').html(d.pillar);
    $('.bbs-prop').html(d.proposal);
    $('.bbs-output').html(d.output);
    $('.bbs-output2').html(d.output2);
    $('.bbs-output3').html(d.output3);
    $('.bbs-fund').html(d.est_fund);
    $('.bbs-inv_type').html(d.inv_type);
    $('.bbs-proc_type').html(d.proc_type);
    $('.bbs-proc_cycle').html(d.proc_cycle);
    $('.bbs-prob').html(d.prob_stat);
    $('.bbs-just').html(d.just);
    $('.bbs-rec').html(d.rec);
    $('.tl_date').html(d.tl_date);
    $('.so_date').html(d.so_date);
    $('.tl').html(d.tl);
    $('.so').html(d.so);
    $('.tlcomment').html(d.tlcomments);
    $('.socomment').html(d.socomments);
    $('.bbs-fund').val(d.est_fund);   
    $('#recomm').val(d.rec);
    $(".b_attachments").html(d.attachments);
    $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click');
}

function loadpendingDMemo(id) {
    var d = getItemFromArrayById(pdmemos, id);
    if (d === null) {return;}
    d = d[0];
    $('.bbs-ref').html(d.refcode);
    $('.bbs-title').html(d.title);
    $('.bbs-state').html(d.state);
    $('.bbs-districts').html(d.district);
    $('.bbs-im1').html(d.im1);
    $('.bbs-im2').html(d.im2);
    $('.phone').html(d.phone);
    $('.email').html(d.email);
    $('.bbs-output').html(d.output);
    $('.address').html(d.address);
    $('.background').html(d.background);
    $('.bbs-fund').html(d.est_fund);
    $('.valuemoney').html(d.valuemoney);
    $('.risk').html(d.risk);
    $('.sustainability').html(d.sustainability);
    $('.conflict').html(d.conflict);
    $('.activities').html(d.activities);
    $('.startdate').html(d.startdate);
    $('.enddate').html(d.enddate);
    $('.tl_date').html(d.tl_date);
    $('.so_date').html(d.so_date);
    $('.tl').html(d.tl);
    $('.so').html(d.so);
    $('.tlcomment').html(d.tlcomments);
    $('.socomment').html(d.socomments);
    $(".b_attachments").html(d.attachments);
    $('[data-tab=\'tab-4\']').removeClass('hidden').trigger('click');
}
