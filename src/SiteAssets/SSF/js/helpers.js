
function getCache(key) {
    if (localStorage.getItem(key) !== null) {
        return isJson(localStorage.getItem(key));
    }
    else {
        return null;
    }
}

function isJson(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
        var json = JSON.parse(item); if (typeof json === 'object' && json !== null) {
            return json;
        }
        return item;
    } catch (e) {
        return item;
    }
}

function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    var x = parseFloat(value);
    return (x | 0) === x;
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}


function RestCalls(u, f) {
    return $.ajax({
        url: siteUrl + u,
        method: 'GET',
        headers: { Accept: 'application/json; odata=verbose' },
        success: function (data) { f(data.d); },
        error: onError
    });
}

function onError(e) {
    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
    console.log(e.responseText);
    swal('Error', e.responseText, 'error');
}

function postJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: { 'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val() },
        success: success,
        error: onError
    });
}

function updateJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: {
            'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val(),
            'X-HTTP-Method': 'MERGE', 'If-Match': '*'
        },
        success: success,
        error: onError
    });
}


function renewDigest() {
    var item = {};
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/contextinfo', item, success);
    function success(d) {
        $('#__REQUESTDIGEST').val(d.d.GetContextWebInformation.FormDigestValue);
        setTimeout(function () {
            renewDigest();
        }, 1770000);
    }
}

function cache(key, value) {
    if (typeof value === 'object') {
        localStorage.setItem(key, JSON.stringify(value));
    }
    else {
        localStorage.setItem(key, value);
    }
}

var addCommas = function (input) {
    if (!input) { return input; }
    // If the regex doesn't match, `replace` returns the string unmodified
    return (input.toString()).replace(
        // Each parentheses group (or 'capture') in this regex becomes an argument
        // to the function; in this case, every argument after 'match'
        /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function (match, sign, zeros, before, decimal, after) {

            // Less obtrusive than adding 'reverse' method on all strings
            var reverseString = function (string) { return string.split('').reverse().join(''); };

            // Insert commas every three characters from the right
            var insertCommas = function (string) {

                // Reverse, because it's easier to do things from the left
                var reversed = reverseString(string);

                // Add commas every three characters
                var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

                // Reverse again (back to normal)
                return reverseString(reversedWithCommas);
            };

            // If there was no decimal, the last capture grabs the final digit, so
            // we have to put it back together with the 'before' substring
            return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
        }
    );
};

var removeCommas = function (input) {
    return input.replace(/,/g, '');
};

function goto(url) {
    window.location.href = url;
}
var Tawk_API = Tawk_API || {},
    Tawk_LoadStart = new Date();
(function () {
    var s1 = document.createElement('script'),
        s0 = document.getElementsByTagName('script')[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/59de1db4c28eca75e4625715/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})();
Tawk_API.visitor = {
    name: _spPageContextInfo.userDisplayName,
    email: _spPageContextInfo.userEmail,
};

function changeActiveMenu(el) {
    $('nav ul li').removeClass('active');
    $(el).addClass('active');
}

function fixIframe() {
    $('.MainIframe').contents().find('body').addClass('ms-fullscreenmode');
    $('.MainIframe')
        .contents()
        .find("#s4-ribbonrow,.od-SuiteNav,.Files-leftNav,.od-TopBar-header.od-Files-header,.pageHeader,.ql-editor,#titlerow .ms-table,.sitePage-uservoice-button,.footer")
        .hide()
        .css('display', 'none');
    $('.MainIframe').contents().find('#titlerow').css('background', 'none !important');
    $('.MainIframe').contents().find('.Files-mainColumn').css('left', '0');
    $('.MainIframe').contents().find('.Files-belowSuiteNav').css('top', '0px');
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function loadTabs() {
    $('ul.tabs li').click(function () {
        fixIframe();
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
    });
}

function getItemFromArrayById(arr, id) {
    var i = $.grep(arr, function (e, index) {
        return e.id === id;
    });
    if (i.length === 1) {
        return i;
    } else {
        return null;
    }
}

function getIndexFromArrayByAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function getAttachmentLinks(Attachments) {
    var links = '';
    $.each(Attachments, function (i, j) {
        if (j.ServerRelativeUrl !== null) {
            links += '<a target=\'_blank\' href=\'' + j.ServerRelativeUrl +
                '\'><span class=\'fa fa-paperclip\'></span>' + j.FileName + '</a>, ';
        }
    });
    return links;
}

function getDelAttachmentLinks(listname, d) {
    var links = '';
    $.each(d.AttachmentFiles, function (i, j) {
        if (j.ServerRelativeUrl !== null) {
            links += '<a target=\'_blank\' href=\'' + j.ServerRelativeUrl + '\'>' +
                '<span class=\'fa fa-paperclip\'></span>' + j.FileName + '</a> &nbsp;&nbsp;' +
                '<i class="fa fa-trash text-danger" style="cursor:pointer" onclick="deleteAttachment(\'' +
                listname + '\', ' + d.Id + ', \'' + j.FileName + '\')"></i>';
        }
    });
    return links;
}

function getIM(id) {

    var im = getItemFromArrayById(ims, id);

    if (im) {
        return im[0].name;
    }
    else {
        return '';
    }
}

function loopFileUpload(listName, id, listValues, fileCountCheck) {
    var dfd = $.Deferred();
    uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment).then(
        function (data) {
            var objcontext = new SP.ClientContext();
            var targetList = objcontext.get_web().get_lists().getByTitle(listName);
            var listItem = targetList.getItemById(id);
            objcontext.load(listItem);
            objcontext.executeQueryAsync(
                function () {
                    fileCountCheck++;
                    if (fileCountCheck <= listValues[0].Files.length - 1) {
                        loopFileUpload(listName, id, listValues, fileCountCheck);
                    } else {
                        swal({
                            title: "Success!",
                            text: fileCountCheck + ' file(s) uploaded successfully.Reloading page...',
                            type: "success",
                        },
                            function (isConfirm) {
                                if (isConfirm) { location.reload(); }
                            });
                    }
                    dfd.resolve(fileCountCheck);
                },
                function (sender, args) {
                    swal('Error', 'Error occured' + args.get_message(), 'error');
                });

        },
        function (sender, args) {
            console.log('Not uploaded');
            dfd.reject(sender, args);
        });
    return dfd.promise();
}


function uploadFile(listName, id, file) {
    var deferred = $.Deferred();
    var fileName = file.name;
    if (fileArray.length > 0) {
        if (fileArray[0].hasOwnProperty('fname')) {
            fileName = fileArray[0].fname + '_' + fileName;
            fileArray.shift();
        }
    }

    getFileBuffer(file).then(
        function (buffer) {
            var bytes = new Uint8Array(buffer);
            var binary = '';
            for (var b = 0; b < bytes.length; b++) {
                binary += String.fromCharCode(bytes[b]);
            }
            var scriptbase = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/';
            console.log(' File size:' + bytes.length);
            $.getScript(scriptbase + 'SP.RequestExecutor.js', function () {
                var createitem = new SP.RequestExecutor(_spPageContextInfo.webServerRelativeUrl);
                createitem.executeAsync({
                    url: _spPageContextInfo.webServerRelativeUrl +
                        '/_api/web/lists/GetByTitle(\'' + listName + '\')/items(' +
                        id + ')/AttachmentFiles/add(FileName=\'' + file.name + '\')',
                    method: 'POST',
                    binaryStringRequestBody: true,
                    body: binary,
                    success: fsucc,
                    error: ferr,
                    state: 'Update'
                });
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    swal('error', fileName + ' not uploaded error', 'error');
                    deferred.reject(data);
                }
            });

        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}

function uploadFileSP(listName, id, file, fileName) {
    var deferred = $.Deferred();

    getFileBuffer(file).then(
        function (buffer) {
            var bytes = new Uint8Array(buffer);
            var binary = '';
            for (var b = 0; b < bytes.length; b++) {
                binary += String.fromCharCode(bytes[b]);
            }
            var scriptbase = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/';
            console.log(' File size:' + bytes.length);
            $.getScript(scriptbase + 'SP.RequestExecutor.js', function () {
                var createitem = new SP.RequestExecutor(_spPageContextInfo.webServerRelativeUrl);
                createitem.executeAsync({
                    url: _spPageContextInfo.webServerRelativeUrl + '/_api/web/lists/GetByTitle(\'' +
                        listName + '\')/items(' + id + ')/AttachmentFiles/add(FileName=\'' + file.name + '\')',
                    method: 'POST',
                    binaryStringRequestBody: true,
                    body: binary,
                    success: fsucc,
                    error: ferr,
                    state: 'Update'
                });
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    swal('error', fileName + ' not uploaded error', 'error');
                    deferred.reject(data);
                }
            });

        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}

function uploadFileToLib(listName, file, fileName) {
    var deferred = $.Deferred();

    getFileBuffer(file).then(
        function (arrayBuffer) {
            var fileCollectionEndpoint = String.format(
                "{0}_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=true, url='{2}')",
                _spPageContextInfo.webAbsoluteUrl + '/',
                _spPageContextInfo.webServerRelativeUrl + '/' + listName, fileName);

            $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                },
                success: fsucc,
                error: ferr
            });

            function fsucc(data) {
                console.log(data + ' uploaded successfully');
                deferred.resolve(data);
            }
            function ferr(data) {
                swal('error', fileName + ' not uploaded error', 'error');
                deferred.reject(data);
            }


        },
        function (err) {
            deferred.reject(err);
        });
    return deferred.promise();
}


function getFileBuffer(file) {
    var deferred = $.Deferred();
    var reader = new FileReader();
    reader.onload = function (e) {
        deferred.resolve(e.target.result);
    };
    reader.onerror = function (e) {
        deferred.reject(e.target.error);
    };
    reader.readAsArrayBuffer(file);
    return deferred.promise();
}


function getListItem(metaurl) {
    return $.ajax({
        url: metaurl,
        type: "GET",
        headers: { accept: "application/json;odata=verbose" }
    });
}

function setLink(category, link) {
    switch (category) {
        case 'Advert':
            $('.c_advert').find('span.text-danger').remove();
            $('.c_advert').append(link)
            break;
        case 'Bid Evaluation Report':
            $('.c_evaluation_rpt').find('span.text-danger').remove();
            $('.c_evaluation_rpt').append(link)
            break;
        case 'Bid Evaluation Panel':
            $('.c_evaluation_panel').find('span.text-danger').remove();
            $('.c_evaluation_panel').append(link)
            break;
        case 'Budget':
            $('.c_budget').find('span.text-danger').remove();
            $('.c_budget').append(link)
            break;
        case 'Call For Concept':
            $('.c_concept').find('span.text-danger').remove();
            $('.c_concept').append(link)
            break;
        case 'Call For Proposal':
            $('.c_proposal').find('span.text-danger').remove();
            $('.c_proposal').append(link)
            break;
        case 'Conflict of interest report':
            $('.c_conflict').find('span.text-danger').remove();
            $('.c_conflict').append(link)
            break;
        case 'Contract':
            $('.c_contract').find('span.text-danger').remove();
            $('.c_contract').append(link)
            break;
        case 'DDCA':
            $('.c_ddca').find('span.text-danger').remove();
            $('.c_ddca').append(link)
            break;
        case 'Other':
            $('.proc_other').find('span.text-danger').remove();
            $('.proc_other').append(link)
    }
}

function setPLink(category, link) {
    switch (category) {
        case 'Logframe':
            $('.c_logframe').find('span.text-danger').remove();
            $('.c_logframe').append(link);
            break;
        case 'Budget':
            $('.c_p_budget').find('span.text-danger').remove();
            $('.c_p_budget').append(link);
            break;
        case 'Other':
            $('.perf_other').find('span.text-danger').remove();
            $('.perf_other').append(link);
            break;
        case 'Risk Register':
            $('.c_risk_register').find('span.text-danger').remove();
            $('.c_risk_register').append(link);
            break;
        case 'Quarterly Report':
            $('.c_quarterly_report').find('span.text-danger').remove();
            $('.c_quarterly_report').append(link);
            break;
        case 'Investee Report':
            $('.c_investee_report').find('span.text-danger').remove();
            $('.c_investee_report').append(link);
            break;
        case 'Monthly Report':
            $('.c_monthly_report').find('span.text-danger').remove();
            $('.c_monthly_report').append(link);
            break;
        case 'Finance Payment Documents':
            $('.fin_pay').find('span.text-danger').remove();
            $('.fin_pay').append(link);
            break;
        case 'Supporting M&E Documents':
            $('.mne').find('span.text-danger').remove();
            $('.mne').append(link);

    }
}

function navigateUrls() {

    var url_string = window.location.href;

    var url = new URL(url_string);

    var task = url.searchParams.get("task");

    switch (task) {
        case "BusinessMemo":
            loadBMemoTasks();
            break;
        case "DecisionMemo":
            loadDecMemoTasks();
            break;
        case "ModificationMemo":
            loadModMemoTasks();
            break;
        case "CloseOut":
            loadCloseOutTasks();
            break;
    }

}