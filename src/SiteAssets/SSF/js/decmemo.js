var fileName = "",
  modmemos = [];

//hideActionButtons();
//no need to hide as advised

function submitDecMemo() {
  var item = {
    __metadata: { type: "SP.Data.Decision_x0020_Memo_x0020_ListListItem" },
    Title: current_inv.title,
    Ref_x002e_Code: current_inv.refcode,
    Federal_x0020_State: current_inv.state,
    District: current_inv.district,
    Source_x0020_of_x0020_proposal: current_inv.proposal,
    Output: current_inv.output,
    Est_x002e__x0020_Fund_x0020_ceil: $("#fundceiling").val(),
    Investee_x0020_Type: current_inv.inv_type,
    Start_x0020_Date: moment(
      $("#startdate").val() + " " + moment().format("hh:mm:ss"),
      "DD/MM/YYYY hh:mm:ss"
    ).toISOString(),
    End_x0020_Date: moment(
      $("#enddate").val() + " " + moment().format("hh:mm:ss"),
      "DD/MM/YYYY hh:mm:ss"
    ).toISOString(),
    Email: $("#email").val(),
    Phone: $("#phone").val(),
    Address: $("#address").val(),
    Background: $("#background").val(),
    Key_x0020_Activities: $("#keyact").val(),
    Value_x0020_for_x0020_money: $("#valuemoney").val(),
    Risk_x0020_Summary: $("#risksummary").val(),
    Sustainability: $("#sustain").val(),
    Conflict_x0020_Sensitivity: $("#conflictsens").val(),
    Recommendation: $("#recomm").val(),
    Problem_x0020_Description: $("#prob_desc").val(),
    Proposed_x0020_Action: $("#proposed_action").val(),
    Strategy_x0020_Link: $("#strategy_link").val(),
    Coordination: $("#coordination").val(),
    Project_x0020_Assets: $("#project_assets").val(),
    IM_x0028_1_x0029_Id: current_inv.im1_id,
    IM_x0028_2_x0029_Id: current_inv.im2_id,
    Status: $("#submit-type").val()
  };
  fileArray = [];
  var listValues = [],
    listName = "Decision Memo",
    fileCountCheck = 0;

  if ($("#evaluation_report").val() !== "") {
    fileArray.push({
      Attachment: $("#evaluation_report")[0].files[0],
      fname: "evaluation_report"
    });
  }

  postJson(psitelst + "('" + listName + "')/items", item, success);
  var id = "";
  function success(d) {
    if (fileArray.length === 0) {
      swal(
        {
          title: "Success!",
          text: "Decision Memo submitted successfully",
          type: "success"
        },
        function(isConfirm) {
          if (isConfirm) {
            location.reload();
            return;
          }
        }
      );
    }
    id = d.d.Id;
    delete item.__metadata;
    item.Files = fileArray;
    listValues.push(item);
    // Create a new Deferred.
    var dfd = new $.Deferred();
    if (listValues[0].Files.length !== 0) {
      if (fileCountCheck <= listValues[0].Files.length - 1) {
        loopFileUpload(listName, id, listValues, fileCountCheck).then(
          function() {
            dfd.resolve("Finished fading out!");
            console.log("success filecount " + fileCountCheck);
          },
          function() {
            console.log("error filecount " + fileCountCheck);
            dfd.reject(sender, args);
          }
        );
      } else {
        dfd.resolve(fileCountCheck);
      }
    }
  }
}

function saveDecMemo() {
  swal(
    {
      title: "Are you sure?",
      text: "You are about to submit the decision case memo.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Submit",
      confirmButtonColor: "#5cb85c"
    },
    function() {
      $("#fundceiling").val(removeCommas($("input#fundceiling").val()));
      submitDecMemo();
    }
  );
}

function saveDecMemoDraft() {
  swal(
    {
      title: "Are you sure?",
      text: "You are about to save the Decision Memo as a draft.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Submit",
      confirmButtonColor: "#5cb85c"
    },
    function() {
      $("#fundceiling").val(removeCommas($("input#fundceiling").val()));
      submitDecMemo();
    }
  );
}

function getDecisionMemoForm() {
  $(".bbs-ref").html(current_inv.refcode);
  $(".bbs-title").html(current_inv.title);
  $(".bbs-state").html(current_inv.state);
  $(".bbs-districts").html(current_inv.district);
  $(".bbs-prop").html(current_inv.proposal);
  $(".bbs-out").html(current_inv.output);
  $(".bbs-inv_type").html(current_inv.inv_type);
  if (current_inv.est_fund) {
    $(".bbs-fund").val(addCommas(current_inv.est_fund));
  }

  prepDecModal();

  $("body").on("click", ".save-dec-memo-draft", function() {
    $("#submit-type").val("Draft");
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "contact",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "introduction",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "key_activities",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "value_for_money",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "risk_assessment",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "sustainability_summary",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "conflict_sensitivity",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "recommendation",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "proposed_action",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "strategy_link",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "prob_desc",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "coordination",
      false
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "project_assets",
      false
    );
    $("#decision-memo-form")
      .data("formValidation")
      .validate();
  });

  $("body").on("click", ".save-dec-memo", function() {
    $("#submit-type").val("Final");
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "contact",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "introduction",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "key_activities",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "value_for_money",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "risk_assessment",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "sustainability_summary",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "conflict_sensitivity",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "recommendation",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "proposed_action",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "strategy_link",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "prob_desc",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "coordination",
      true
    );
    $("#decision-memo-form").formValidation(
      "enableFieldValidators",
      "project_assets",
      true
    );
    $("#decision-memo-form")
      .data("formValidation")
      .validate();
  });

  $("#decision-memo-form")
    .formValidation({
      framework: "bootstrap",
      excluded: ":disabled",
      err: { container: ".messages" },
      icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove",
        validating: "glyphicon glyphicon-refresh"
      },
      fields: {
        estimated_fund: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Actual Fund is required" }
          }
        },
        start_date: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The field is required" },
            date: {
              format: "DD/MM/YYYY",
              message: "The value is not a valid date"
            }
          }
        },
        end_date: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The field is required" },
            date: {
              format: "DD/MM/YYYY",
              message: "The value is not a valid date"
            }
          }
        },
        d_email: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Investee Email is required" },
            emailAddress: { message: "The value is not a valid email address" }
          }
        },
        phone_number: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Phone number is required" },
            regexp: {
              message:
                "The phone number can only contain the digits, spaces, -, (, ), + and .",
              regexp: /^[0-9\s\-()+\.]+$/
            }
          }
        },
        contact: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Investee Address is required" }
          }
        },
        introduction: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The Introduction/Background is required" }
          }
        },
        key_activities: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        value_for_money: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        risk_assessment: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        sustainability_summary: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        prob_desc: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: {
              message: "The problem description field is required"
            }
          }
        },
        conflict_sensitivity: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The conflict sensitivity field is required" }
          }
        },
        proposed_action: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The proposed action field is required" }
          }
        },
        strategy_link: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: {
              message: "The link to SSF II strategy field is required"
            }
          }
        },
        coordination: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The coordination field is required" }
          }
        },
        project_assets: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The project assets field is required" }
          }
        },
        recommendation: {
          row: ".col-md-12",
          enabled: false,
          validators: {
            notEmpty: { message: "The recommendation field is required" }
          }
        }
      }
    })
    .on("success.form.fv", function(e) {
      e.preventDefault();
      if ($("#submit-type").val() === "Final") {
        saveDecMemo();
      } else {
        saveDecMemoDraft();
      }
    });
}

function uploadProc() {
  if ($("#proc_file").val() === "") {
    swal("Error", "No file selected for upload", "error");
    return;
  }
  swal(
    {
      title: "Are you sure?",
      text: "You want to submit the document for procurement?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Submit",
      confirmButtonColor: "#5cb85c"
    },
    function() {
      submitProc();
    }
  );
}

function submitProc() {
  var serverRelativeUrlToFolder =
    _spPageContextInfo.webServerRelativeUrl + "/Procurement";
  fileArray = [];

  if ($("#proc_file").val() !== "") {
    fileArray.push({
      Attachment: $("#proc_file")[0].files[0],
      ftype: $("#filetype").val()
    });
  }

  // Get the local file as an array buffer.
  var getFile = getFileBuffer(fileArray[0].Attachment);
  getFile.done(function(arrayBuffer) {
    uploadAdFile(arrayBuffer).done(function(data) {
      var getItem = getListItem(data.d.ListItemAllFields.__deferred.uri);
      var failct = 0;
      getItem.done(function(listItem, status, xhr) {
        updateMetadata(listItem.d.__metadata);
      });
      getItem.fail(function() {
        failct++;
        if (failct <= 5) {
          getListItem(data.d.ListItemAllFields.__deferred.uri);
        } else {
          swal("Error", "Failed to upload document", "error");
        }
      });
    });
  });
  getFile.fail(onError);

  // Add the file to the file collection in the folder.
  function uploadAdFile(arrayBuffer) {
    var parts = $("#proc_file")[0].value.split("\\");
    fileName = parts[parts.length - 1];

    var fileCollectionEndpoint = String.format(
      "{0}_api/web/getfolderbyserverrelativeurl('{1}')/files" +
        "/add(overwrite=true, url='{2}')",
      _spPageContextInfo.webAbsoluteUrl + "/",
      serverRelativeUrlToFolder,
      fileName
    );

    return $.ajax({
      url: fileCollectionEndpoint,
      type: "POST",
      data: arrayBuffer,
      processData: false,
      headers: {
        "accept": "application/json;odata=verbose",
        "X-RequestDigest": $("#__REQUESTDIGEST").val()
      }
    });
  }

  function updateMetadata(d) {
    var item = {
      __metadata: { type: d.type },
      Ref_x002e__x0020_Code: $(".bbs-ref").html(),
      Category: $("#filetype").val(),
      Description: $("#proc_desc").val(),
      Title: fileName
    };
    updateJson(d.uri, item, success);
    function success(data) {
      swal(
        {
          title: "Success!",
          text: "Procurement document uploaded successfully",
          type: "success"
        },
        function(isConfirm) {
          $("#proc_file,.inputFiles").val("");
          $("#uploadmodal").modal("hide");
          processInvestment($(".bbs-ref").html());
        }
      );
    }
  }
}

function prepInvForms(d) {
  $(".bbs-ref").html(current_inv.refcode);
  $(".bbs-title").html(current_inv.title);
  $(".bbs-state").html(current_inv.state);
  $(".bbs-districts").html(current_inv.district);
  $(".bbs-im1").html(current_inv.im1);
  $(".bbs-im2").html(current_inv.im2);
  $(".bbs-pillar").html(current_inv.pillar);
  $(".bbs-prop").html(current_inv.proposal);
  $(".bbs-output2").html(current_inv.output2);
  $(".bbs-output3").html(current_inv.output3);
  $(".bbs-output").html(current_inv.output);
  $(".bbs-fund").html(current_inv.est_fund);
  $(".bbs-inv_type").html(current_inv.inv_type);
  $(".bbs-proc_type").html(current_inv.proc_type);
  $(".bbs-proc_cycle").html(current_inv.proc_cycle);
  $(".bbs-prob").html(current_inv.prob_stat);
  $(".bbs-just").html(current_inv.just);
  $(".bbs-rec").html(current_inv.rec);
  $(".bbs-tlcomment").html(current_inv.tlcomments);
  $(".bbs-socomment").html(current_inv.socomments);
  $(".b_attachments").html(current_inv.attachments);
  $(".tl").html(current_inv.tl);
  $(".so").html(current_inv.so);
  $(".tl_date").html(current_inv.tl_date);
  $(".so_date").html(current_inv.so_date);
  if (d.length === 0) {
    $(".btn-create-memo").removeClass("hidden");
    $(".dec_memo_form").addClass("hidden");
  } else {
    $(".btn-create-memo").addClass("hidden");
    $.each(d, function(i, j) {
      var tlname = "",
        soname = "",
        tldate = "",
        sodate = "",
        startdate = "",
        enddate = "";
      if (j.SO) {
        soname = j.SO.Title;
      }
      if (j.SO_x0020_Date) {
        sodate = moment(j.SO_x0020_Date).format("DD/MM/YYYY");
      }
      if (j.TL) {
        tlname = j.TL.Title;
      }
      if (j.TL_x0020_Date) {
        tldate = moment(j.TL_x0020_Date).format("DD/MM/YYYY");
      }
      if (j.Start_x0020_Date) {
        startdate = moment(j.Start_x0020_Date).format("DD/MM/YYYY");
      }
      if (j.End_x0020_Date) {
        enddate = moment(j.End_x0020_Date).format("DD/MM/YYYY");
      }
      if (j.Est_x002e__x0020_Fund_x0020_ceil) {
        $(".bbs-actual").html(addCommas(j.Est_x002e__x0020_Fund_x0020_ceil));
      }
      $(".bbs-im2").html(d.im2);
      $(".phone").html(j.Phone);
      $(".email").html(j.Email);
      $(".bbs-output").html(j.Output);
      $(".address").html(j.Address);
      $(".background").html(j.Background);
      $(".valuemoney").html(j.Value_x0020_for_x0020_money);
      $(".risk").html(j.Risk_x0020_Summary);
      $(".sustainability").html(j.Sustainability);
      $(".conflict").html(j.Conflict_x0020_Sensitivity);
      $(".activities").html(j.Key_x0020_Activities);
      $(".dec_recommend").html(j.Recommendation);
      $(".startdate").html(startdate);
      $(".enddate").html(enddate);
      $(".prob_desc").html(j.Problem_x0020_Description);
      $(".proposed_action").html(j.Proposed_x0020_Action);
      $(".strategy_link").html(j.Strategy_x0020_Link);
      $(".coordination").html(j.Coordination);
      $(".project_assets").html(j.Project_x0020_Assets);
      $(".d_tl_date").html(tldate);
      $(".d_so_date").html(sodate);
      $(".d_tl").html(tlname);
      $(".d_so").html(soname);
      $(".d_tlcomment").html(j.TL_x0020_Comments);
      $(".d_socomment").html(j.SO_x0020_Comments);
      $(".d_attachments").html(getAttachmentLinks(j.AttachmentFiles));
      if (j.Approval_x0020_Status === "Approved") {
        $("#dmemo_status")
          .addClass("text-success")
          .html(j.Approval_x0020_Status + ' <span class="fa fa-check"></span>');
      } else {
        $("#dmemo_status")
          .addClass("text-danger")
          .html(j.Approval_x0020_Status + ' <span class="fa fa-info"></span>');
      }

      //prepopulate modmemo modal
      $("m_background").val(j.Background);
      $("#m_email").val(j.Email);
      $("#m_phone").val(j.Phone);
      $("#m_address").val(j.Address);
      $("#m_enddate").val(enddate);
    });
  }
}
var tbprocurement = null;

function getProcurement(d) {
  var s = "";
  validateCloseOut();
  $.each(d, function(i, j) {
    var desc = "";
    if (j.Description) {
      desc = j.Description;
    }
    var link =
      '<a target="_blank" href="' +
      j.EncodedAbsUrl +
      '"><span class="fa fa-paperclip"><span> ' +
      j.FileLeafRef +
      " </a>, ";
    s +=
      "<tr><td>" +
      desc +
      "</td><td>" +
      j.Category +
      "</td><td>" +
      link +
      "</td></tr>";
    setLink(j.Category, link);
  });
  if (tbprocurement) {
    tbprocurement.destroy();
  }
  $("#tbprocurement>tbody").html(s);
  tbprocurement = $("#tbprocurement").DataTable({ responsive: true });
}

function getCloseOuts(d) {
  $("#closeoutid").val("");
  if (
    current_inv.im1_id !== _spPageContextInfo.userId &&
    current_inv.im2_id !== _spPageContextInfo.userId
  ) {
    $("#close_comment").prop("disabled", true);
    $("#tab-15 .btn").hide();
  }
  if (d.length < 0) {
    return;
  }
  $.each(d, function(i, j) {
    $("#closeoutid").val(j.Id);
    var soname = "",
      sodate = "",
      tlname = "",
      tldate = "",
      socomments = j.SO_x0020_Comments || "",
      tlcomments = j.TL_x0020_Comments || "";

    if (j.SO) {
      soname = j.SO.Title;
      sodate = moment(j.SO_x0020_Date).format("DD/MM/YYYY");
    }

    if (j.TL) {
      tlname = j.TL.Title;
      tldate = moment(j.TL_x0020_Date).format("DD/MM/YYYY");
    }

    if (
      (current_inv.im1_id === _spPageContextInfo.userId ||
        current_inv.im2_id === _spPageContextInfo.userId) &&
      (j.Approval_x0020_Status === "Pending" ||
        j.Approval_x0020_Status === "Approved")
    ) {
      $(".inv .btn").hide();
      $("#close_comment,#tab-15 textarea").prop("disabled", true);
    }

    $("#mod_comment").val(j.ModMemo);
    $("#advert_comment").val(j.Advert);
    $("#BER_comment").val(j.BER);
    $("#BEP_comment").val(j.BEP);
    $("#proc_budget_comment").val(j.Procurement_x0020_Budget);
    $("#concept_comment").val(j.Concept);
    $("#proposal_comment").val(j.Proposal);
    $("#COI_comment").val(j.COI);
    $("#contract_comment").val(j.Contract);
    $("#DDCA_comment").val(j.DDCA);
    $("#proc_other_comment").val(j.Procurement_x0020_Other);
    $("#logframe_comment").val(j.LogFrame);
    $("#perf_budget_comment").val(j.Performance_x0020_Budget);
    $("#risk_comment").val(j.Risk);
    $("#quarterly_comment").val(j.Quarterly);
    $("#investee_report_comment").val(j.Investee_x0020_Report);
    $("#monthly_report_comment").val(j.Monthly);
    $("#perf_other_comment").val(j.Performance_x0020_Other);
    $("#monthly_report_comment").val(j.MNE);
    $("#fin_pay_comment").val(j.financialPayments);

    $("#close_comment").val(j.Comment);
    $(".c_tl").html(tlname);
    $(".c_tlcomment").html(tlcomments);
    $(".c_tl_date").html(tldate);
    $(".c_so").html(soname);
    $(".c_so_date").html(sodate);
    $(".c_socomment").html(socomments);
  });
}

var tbperf = null;
function getPerformance(d) {
  var s = "";
  $.each(d, function(i, j) {
    var desc = "";
    if (j.Description) {
      desc = j.Description;
    }
    var link =
      '<a target="_blank" href="' +
      j.EncodedAbsUrl +
      '"><span class="fa fa-paperclip"><span> ' +
      j.FileLeafRef +
      " </a>, ";
    s +=
      "<tr><td>" +
      j.Quarter +
      "</td><td>" +
      desc +
      "</td><td>" +
      j.Category +
      "</td><td>" +
      link +
      "</td></tr>";
    setPLink(j.Category, link);
  });
  if (tbperf) {
    tbperf.destroy();
  }
  $("#tbperformance>tbody").html(s);
  tbperf = $("#tbperformance").DataTable({ responsive: true });
}

function uploadPerf() {
  if ($("#perf_file").val() === "") {
    swal("Error", "No file selected for upload", "error");
    return;
  }
  swal(
    {
      title: "Are you sure?",
      text:
        "You want to upload the document to the Performance Management Library?",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Submit",
      confirmButtonColor: "#5cb85c"
    },
    function() {
      submitPerf();
    }
  );
}

function submitPerf() {
  var serverRelativeUrlToFolder =
    _spPageContextInfo.webServerRelativeUrl + "/Performance Management";
  fileArray = [];
  if ($("#perf_file").val() !== "") {
    fileArray.push({
      Attachment: $("#perf_file")[0].files[0],
      ftype: $("#filetype").val()
    });
  }
  // Get the local file as an array buffer.
  var getFile = getFileBuffer(fileArray[0].Attachment);
  getFile.done(function(arrayBuffer) {
    uploadAdFile(arrayBuffer).done(function(data) {
      var getItem = getListItem(data.d.ListItemAllFields.__deferred.uri);
      var failct = 0;
      getItem.done(function(listItem, status, xhr) {
        updateMetadata(listItem.d.__metadata);
      });
      getItem.fail(function() {
        failct++;
        if (failct <= 5) {
          getListItem(data.d.ListItemAllFields.__deferred.uri);
        } else {
          swal("Error", "Failed to upload document", "error");
        }
      });
    });
  });
  getFile.fail(onError);

  // Add the file to the file collection in the folder.
  function uploadAdFile(arrayBuffer) {
    var parts = $("#perf_file")[0].value.split("\\");
    fileName = parts[parts.length - 1];

    var fileCollectionEndpoint = String.format(
      "{0}_api/web/getfolderbyserverrelativeurl('{1}')/files" +
        "/add(overwrite=true, url='{2}')",
      _spPageContextInfo.webAbsoluteUrl + "/",
      serverRelativeUrlToFolder,
      fileName
    );

    return $.ajax({
      url: fileCollectionEndpoint,
      type: "POST",
      data: arrayBuffer,
      processData: false,
      headers: {
        "accept": "application/json;odata=verbose",
        "X-RequestDigest": $("#__REQUESTDIGEST").val()
      }
    });
  }

  function updateMetadata(d) {
    var item = {
      __metadata: { type: d.type },
      Ref_x002e__x0020_Code: $(".bbs-ref").html(),
      Category: $("#pfiletype").val(),
      Description: $("#perf_desc").val(),
      Quarter: $("#quarter").val(),
      Title: fileName
    };
    updateJson(d.uri, item, success);
    function success(data) {
      swal(
        {
          title: "Success!",
          text: "Perfomance Management document uploaded successfully",
          type: "success"
        },
        function(isConfirm) {
          $("#perf_file,.inputFiles").val("");
          $("#uploadmodal").modal("hide");
          processInvestment($(".bbs-ref").html());
        }
      );
    }
  }
}

var tbmodmemos = null;

function getModMemos(d) {
  var s = 1,
    r = "",
    modAttachments = "";
  $.each(d, function(i, j) {
    if (j.Id) {
      s++;
    }

    var enddate = "",
      soid = null,
      tlid = null,
      tlname = "",
      soname = "",
      tldate = "",
      sodate = "",
      tlapproval = j.TL_x0020_Approval || "",
      soapproval = j.SO_x0020_Approval || "";

    if (j.SO) {
      soid = j.SO.Id;
      soname = j.SO.Title;
    }
    if (j.SO_x0020_Date) {
      sodate = moment(j.SO_x0020_Date).format("DD/MM/YYYY");
    }
    if (j.TL) {
      tlid = j.TL.Id;
      tlname = j.TL.Title;
    }
    if (j.TL_x0020_Date) {
      tldate = moment(j.TL_x0020_Date).format("DD/MM/YYYY");
    }
    enddate = moment(j.End_x0020_Date, "YYYY/MM/DD").format("DD/MM/YYYY");
    r +=
      "<tr><td>" +
      enddate +
      "</td><td>" +
      j.Extension +
      "</td><td>" +
      j.Budget_x0020_Increase +
      "</td><td>" +
      j.Total_x0020_Value +
      "</td><td>" +
      j.Disbursed_x0020_To_x0020_Date +
      "</td><td>" +
      tlapproval +
      "</td><td>" +
      soapproval +
      '</td><td><a onclick="getModMemo(' +
      j.Id +
      ')" class="btn btn-orange" href="#">View</a>';
    if (j.Editable) {
      r +=
        '<br/><a href="#" class="btn btn-primary" style="padding:6px 40px;margin-top:3px" onclick="editModmemo(' +
        j.Id +
        ')">Edit</a>';
    }
    r += "</td></tr>";
    var attachments = "";
    if (j.Attachments) {
      attachments = getAttachmentLinks(j.AttachmentFiles);
      if (j.Approval_x0020_Status === "Approved") {
        modAttachments += attachments;
      }
    }

    modmemos.push({
      id: j.Id,
      bmemoId: j.BMemoId,
      enddate: enddate,
      extension: j.Extension,
      title: j.Title,
      budget_increase: j.Budget_x0020_Increase,
      total_value: j.Total_x0020_Value,
      disbursed_to_date: j.Disbursed_x0020_To_x0020_Date,
      disbus_percent: j.Disbursement_x0020_Percent,
      ip_reports: j.IP_x0020_Reports,
      investment_performance: j.Investment_x0020_Performance,
      justification: j.Justification,
      rec: j.Recommendation,
      attachments: attachments,
      email: j.Email,
      phone: j.Phone,
      address: j.Address,
      editable: j.Editable,
      background: j.Background,
      soid: soid,
      soname: soname,
      sodate: sodate,
      tlid: tlid,
      tlname: tlname,
      tldate: tldate,
      socomments: j.SO_x0020_Comments,
      tlcomments: j.TL_x0020_Comments
    });
  });
  $(".m_attachments").html(modAttachments);
  $(".modno").html(s);
  if (tbmodmemos) {
    tbmodmemos.destroy();
  }
  $("#tbmodmemos>tbody").html(r);
  tbmodmemos = $("#tbmodmemos").DataTable({ responsive: true });
}

function getModMemo(id) {
  var d = getItemFromArrayById(modmemos, id);
  if (d === null) {
    return;
  }
  d = d[0];
  $(".m_enddate").html(d.enddate);
  $(".m_email").html(d.email);
  $(".m_phone").html(d.phone);
  $(".m_address").html(d.address);
  $(".m_extension").html(d.extension);
  $(".m_budget").html(d.budget_increase);
  $(".m_total").html(d.total_value);
  $(".m_disbursed").html(d.disbursed_to_date);
  $(".m_disburse_percent").html(d.disbus_percent);
  $(".m_ip_reports").html(d.ip_reports);
  $(".m_background").html(d.background);
  $(".m_analysis").html(d.investment_performance);
  $(".m_justification").html(d.justification);
  $(".m_recommend").html(d.rec);
  $(".m_attachments").html(d.attachments);
  $(".m_tl").html(d.tlname);
  $(".m_tl_date").html(d.tldate);
  $(".m_tlcomment").html(d.tlcomments);
  $(".m_so").val(d.soname);
  $(".m_so_date").html(d.sodate);
  $(".m_socomment").html(d.socomments);
  $('#tab-14 .nav-tabs>li>a[href="#tabm2"]')
    .show()
    .trigger("click");
}

function editModmemo(id) {
  var d = getItemFromArrayById(modmemos, id);
  if (d === null) {
    return;
  }
  d = d[0];
  $("#m_enddate").val(d.enddate);
  $("#m_email").val(d.email);
  $("#m_phone").val(d.phone);
  $("#m_address").val(d.address);
  $("#extension").val(d.extension);
  $("#budget_increase").val(d.budget_increase);
  $("#total_value").val(d.total_value);
  $("#disbursed").val(d.disbursed_to_date);
  $("#disbus_percent").val(d.disbus_percent);
  $("#ip_reports").val(d.ip_reports);
  $("#m_background").val(d.background);
  $("#analysis").val(d.investment_performance);
  $("#m_justification").val(d.justification);
  $(".modatt div").html(createDeleteAttachmentLink(d));
  $("#m_recommend").val(d.rec);
  $("#modmemoid").val(d.id);
  $("#modmemo-form .btn-orange").hide();
  $("#modmemo-form .btn-primary,.modatt").show();
  $("#modmodal").modal("show");

  $("body").on("click", ".modatt div .delatt", function() {
    deleteAttachment("Modification Memo", d.id, $(this).data("file"), $(this));
  });
}

function validateMemoForm() {
  $("#modmodal").on("hidden.bs.modal", function() {
    $("#modmemo-form input,#modmemo-form textarea").val("");
    $("#modmemo-form .btn-orange").show();
    $("#modmemo-form .btn-primary,.modatt").hide();
  });
  $("#budget_increase,#total_value,#disbursed").bind("textchange", function(
    event,
    previousText
  ) {
    var value = removeCommas($(this).val());
    if ($.isNumeric(value)) {
      $(this).val(addCommas(value));
    } else {
      $(this).val("");
    }
  });
  $('#tab-14 .nav-tabs>li>a[href="#tabm1"]').click(function() {
    $('#tab-14 .nav-tabs>li>a[href="#tabm2"]').hide();
  });
  $("#modmemo-form .btn").click(function() {
    $("#modmemo-form")
      .data("formValidation")
      .validate();
  });
  $("#datetimepickerend").datetimepicker({ format: "DD/MM/YYYY" });

  $("#datetimepicker3")
    .datetimepicker({ format: "DD/MM/YYYY" })
    .on("dp.change", function(e) {
      $("#modmemo-form").formValidation("revalidateField", "m_enddate");
    });
  $("#m_file_container input:file").MultiFile();

  $("#modmemo-form")
    .formValidation({
      framework: "bootstrap",
      excluded: ":disabled",
      icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove",
        validating: "glyphicon glyphicon-refresh"
      },
      fields: {
        m_enddate: {
          validators: {
            notEmpty: {
              message: "The Revised End Date is required"
            },
            date: {
              format: "DD/MM/YYYY",
              message: "The Revised End Date is not a valid date"
            }
          }
        },
        m_email: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The email is required" },
            emailAddress: { message: "The email is not valid" }
          }
        },
        m_phone: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Phone number is required" },
            regexp: {
              message:
                "The phone number can only contain the digits, spaces, -, (, ), + and .",
              regexp: /^[0-9\s\-()+\.]+$/
            }
          }
        },
        m_address: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The Address is required" }
          }
        },
        extension: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The proposed extension is required" }
          }
        },
        budget_increase: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The budget increase is required" }
          }
        },
        total_value: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The revised total value is required" }
          }
        },
        disbursed: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The disbursed amount to date is required" }
          }
        },
        disbus_percent: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The disbursed % is required" }
          }
        },
        ip_reports: {
          row: ".col-md-4",
          validators: {
            notEmpty: { message: "The IP Reports received is required" }
          }
        },
        m_background: {
          row: ".col-md-6",
          validators: {
            notEmpty: { message: "The Introduction/Background is required" }
          }
        },
        analysis: {
          row: ".col-md-6",
          validators: {
            notEmpty: {
              message: "The Analysis of investment Performance is required"
            }
          }
        },
        m_justification: {
          row: ".col-md-6",
          validators: {
            notEmpty: { message: "The Justification is required" }
          }
        },
        m_recommend: {
          row: ".col-md-6",
          validators: {
            notEmpty: {
              message: "The Recommendation to the head of SSF is required"
            }
          }
        }
      }
    })
    .on("success.form.fv", function(e) {
      e.preventDefault();
      var mdata = [],
        fileArray = [];
      $("#m_file_container input:file").each(function() {
        if ($(this)[0].files[0]) {
          fileArray.push({ Attachment: $(this)[0].files[0] });
        }
      });
      var item = {
        Title: $(".bbs-title").html(),
        Count: $(".modno").html(),
        End_x0020_Date: moment($("#m_enddate").val(), "DD/MM/YYYY").format(
          "YYYY-MM-DD"
        ),
        Email: $("#m_email").val(),
        Phone: $("#m_phone").val(),
        Address: $("#m_address").val(),
        Extension: $("#extension").val(),
        Background: $("#m_background").val(),
        Budget_x0020_Increase: $("#budget_increase").val(),
        Total_x0020_Value: $("#total_value").val(),
        Disbursed_x0020_To_x0020_Date: $("#disbursed").val(),
        Disbursement_x0020_Percent: $("#disbus_percent").val(),
        IP_x0020_Reports: $("#ip_reports").val(),
        Investment_x0020_Performance: $("#analysis").val(),
        Justification: $("#m_justification").val(),
        Recommendation: $("#m_recommend").val(),
        BMemo: current_inv.id
      };
      if ($("#modmemoid").val() === "") {
        swal(
          {
            title: "Are you sure?",
            text: "You are about to submit the Modification Memo.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Submit",
            confirmButtonColor: "#5cb85c"
          },
          function() {
            postmodmemo();
          }
        );
      } else {
        swal(
          {
            title: "Are you sure?",
            text: "You want to update the Modification Memo?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Submit",
            confirmButtonColor: "#5cb85c"
          },
          function() {
            updatemodmemo();
          }
        );
      }

      function postmodmemo() {
        item.Files = fileArray;
        mdata.push(item);
        function succcessfn() {
          swal(
            {
              title: "Success!",
              text: "Modification Memo submitted successfully.",
              type: "success"
            },
            function(isConfirm) {
              if (isConfirm) {
                $("#modmemo-form").formValidation("resetForm", true);
                $("#modmodal").modal("close");
              }
            }
          );
        }
        console.log(mdata);
        createModMemo("Modification Memo", mdata, succcessfn).then(
          function() {
            console.log("ulifika");
          },
          function(sender, args) {
            swal("Error", "Error occured" + args.get_message());
          }
        );
      }

      function updatemodmemo() {
        item.__metadata = { type: "SP.Data.Modification_x0020_MemoListItem" };
        var listName = "Modification Memo";
        delete item.DMemo;
        var id = $("#modmemoid").val();
        updateJson(
          psitelst + "('" + listName + "')/items(" + id + ")",
          item,
          success
        );
        function success(d) {
          if (fileArray.length !== 0) {
            var dfd = $.Deferred().resolve();

            fileArray.forEach(function(f) {
              dfd = dfd.then(function() {
                return uploadFileSP(
                  listName,
                  id,
                  f.Attachment,
                  $(".bbs-ref").html() + "_" + f.fname
                );
              });
            });

            dfd.done(function() {
              swal(
                {
                  title: "Success!",
                  text: "Modification Memo updated successfully.Refresh page",
                  type: "success"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    $("#modmemo-form").modal("hide");
                    location.reload();
                  }
                }
              );
            });
          }
        }
      }
    });
}
validateMemoForm();

var createModMemo = function(listName, listValues, successfn) {
  var fileCountCheck = 0;
  var context = new SP.ClientContext.get_current();
  var dfd = $.Deferred();
  var targetList = context
    .get_web()
    .get_lists()
    .getByTitle(listName);
  context.load(targetList);
  var itemCreateInfo = new SP.ListItemCreationInformation();
  var listItem = targetList.addItem(itemCreateInfo);

  $.each(listValues[0], function(i, j) {
    if (i !== "Files") {
      listItem.set_item(i, j);
    }
  });
  listItem.update();
  context.executeQueryAsync(
    function() {
      var id = listItem.get_id();
      if (listValues[0].Files.length !== 0) {
        if (fileCountCheck <= listValues[0].Files.length - 1) {
          loopFileUpload(listName, id, listValues, fileCountCheck).then(
            function() {},
            function(sender, args) {
              console.log("Error uploading");
              dfd.reject(sender, args);
            }
          );
        }
      } else {
        succcessfn();
        dfd.resolve(fileCountCheck);
      }
    },
    function(sender, args) {
      swal("Error", "Error occured" + args.get_message(), "error");
    }
  );

  return dfd.promise();
};

function validateCloseOut() {
  $(".closeout-form .btn-orange").click(function() {
    if ($(".m_attachments").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "mod_comment",
        true
      );
    }
    if ($(".c_advert").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "advert_comment",
        true
      );
    }
    if ($(".c_evaluation_rpt").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "BER_comment",
        true
      );
    }
    if ($(".c_evaluation_panel").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "BEP_comment",
        true
      );
    }
    if ($(".c_concept").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "concept_comment",
        true
      );
    }
    if ($(".c_proposal").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "proposal_comment",
        true
      );
    }
    if ($(".c_conflict").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "COI_comment",
        true
      );
    }
    if ($(".c_contract").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "contract_comment",
        true
      );
    }
    if ($(".c_ddca").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "DDCA_comment",
        true
      );
    }
    if ($(".c_logframe").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "logframe_comment",
        true
      );
    }
    if ($(".c_p_budget").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "perf_budget_comment",
        true
      );
    }
    if ($(".c_budget").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "proc_budget_comment",
        true
      );
    }
    if ($(".c_risk_register").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "risk_comment",
        true
      );
    }
    if ($(".c_quarterly_report").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "quarterly_comment",
        true
      );
    }
    if ($(".c_investee_report").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "investee_report_comment",
        true
      );
    }
    if ($(".c_monthly_report").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "monthly_report_comment",
        true
      );
    }
    if ($(".fin_pay").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "fin_pay_comment",
        true
      );
    }
    if ($(".mne").find(".text-danger").length) {
      $(".closeout-form").formValidation(
        "enableFieldValidators",
        "mne_comment",
        true
      );
    }
    $(".closeout-form")
      .data("formValidation")
      .validate();
  });
  $(".closeout-form")
    .formValidation({
      framework: "bootstrap",
      excluded: ":disabled",
      icon: {
        valid: "glyphicon glyphicon-ok",
        invalid: "glyphicon glyphicon-remove",
        validating: "glyphicon glyphicon-refresh"
      },
      fields: {
        close_comment: {
          row: "td",
          validators: {
            notEmpty: { message: "The Close out comment is required" }
          }
        },
        mod_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        advert_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        BER_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The Close out comment is required" }
          }
        },
        BEP_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        concept_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        proposal_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The Close out comment is required" }
          }
        },
        COI_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        contract_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        DDCA_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        logframe_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The Close out comment is required" }
          }
        },
        perf_budget_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        risk_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        quarterly_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The Close out comment is required" }
          }
        },
        investee_report_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        },
        monthly_report_comment: {
          row: "td",
          enabled: false,
          validators: {
            notEmpty: { message: "The field is required" }
          }
        }
      }
    })
    .on("success.form.fv", function(e) {
      e.preventDefault();
      swal(
        {
          title: "Are you sure?",
          text: "You want to close out the project?",
          type: "warning",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          confirmButtonText: "Submit",
          confirmButtonColor: "#5cb85c"
        },
        function() {
          submitCloseOut();
        }
      );
    });
}

function submitCloseOut() {
  var item = {
    __metadata: { type: "SP.Data.CloseOutListItem" },
    Comment: $("#close_comment").val(),
    ModMemo: $("#mod_comment").val(),
    Advert: $("#mod_comment").val(),
    BEP: $("#BEP_comment").val(),
    BER: $("#BER_comment").val(),
    Procurement_x0020_Budget: $("#proc_budget_comment").val(),
    Concept: $("#concept_comment").val(),
    Proposal: $("#proposal_comment").val(),
    COI: $("#COI_comment").val(),
    Contract: $("#contract_comment").val(),
    DDCA: $("#DDCA_comment").val(),
    Procurement_x0020_Other: $("#proc_other_comment").val(),
    LogFrame: $("#logframe_comment").val(),
    Performance_x0020_Budget: $("#perf_budget_comment").val(),
    Risk: $("#risk_comment").val(),
    Quarterly: $("#quarterly_comment").val(),
    Investee_x0020_Report: $("#investee_report_comment").val(),
    Monthly: $("#monthly_report_comment").val(),
    Performance_x0020_Other: $("#perf_other_comment").val(),
    financialPayments: $("#fin_pay_comment").val(),
    MNE: $("#mne_comment").val()
  };
  function fixClose() {
    $("#tab-15 textarea").attr("disabled", "disabled");
    $("#tab-15 .btn").hide();
  }
  function success() {
    swal("success", "Close out request submitted successfully", "success");
    fixClose();
  }
  if ($("#closeoutid").val()) {
    item.Approval_x0020_Status = "Pending";
    updateJson(
      psitelst + "('CloseOut')/items(" + $("#closeoutid").val() + ")",
      item,
      success
    );
  } else {
    item.RefCode = $(".bbs-ref").html();
    item.BMemoId = current_inv.id;
    postJson(psitelst + "('CloseOut')/items", item, success);
  }
}
