﻿var closeout = {};
function getcloseoutList() {
    closeouts = [];
    $('.loaderwrapper').fadeIn();
    var dapurl = 'lists/GetByTitle(\'CloseOut\')/items?$select=*,BMemo/Title0,TL/Title,SO/Title&$expand=BMemo,SO,TL&$filter=Approval_x0020_Status eq \'Pending\'';
    if ($.inArray("TL", user.access) !== -1) {
        dapurl += " and TL_x0020_Approval eq 'Pending'";
    }
    if ($.inArray("SO", user.access) !== -1) {
        dapurl += " and SO_x0020_Approval eq 'Pending' and TL_x0020_Approval eq 'Approved'";
    }

    RestCalls(dapurl, function (d) {
        $.each(d.results, function (i, j) {
            var tlname = "", 
                soname = "", 
                tldate = "", 
                sodate = "", 
                tlapproval = j.TL_x0020_Approval || "", 
                soapproval = j.SO_x0020_Approval || "";

            if (j.SO) {
                soname = j.SO.Title;
                sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
            }
           
            if (j.TL) {
                tlname = j.TL.Title;
                tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
            }
            closeouts.push({
                soname,
                sodate,
                tlname,
                tldate,
                tlapproval,
                soapproval,                
                refcode:j.RefCode,
                id: j.Id,
                bmemoId: j.BMemoId,
                bmemo: j.BMemo.Title0,
                comment:j.Comment,
                status: j.Approval_x0020_Status,
                socomments: j.SO_x0020_Comments,
                tlcomments: j.TL_x0020_Comments,
                modmemo:j.ModMemo,
                advert:j.Advert,
                ber:j.BER,
                bep:j.BEP,
                procBudget:j.Procurement_x0020_Budget,
                concept:j.Concept,
                coi:j.COI,
                contract:j.Contract,
                ddca:j.DDCA,
                procOther:j.Procurement_x0020_Other,
                logframe:j.LogFrame,
                perfBudget:j.Performance_x0020_Budget,
                risk:j.Performance_x0020_Budget,
                quarterly:j.Quarterly,
                investeeReport:j.Investee_x0020_Report,
                monthly:j.Monthly,
                perfOther:j.Performance_x0020_Other,
                proposal:j.Proposal,
                mne:j.MNE,
                finPay:j.financialPayments
            });
        });
        populatecloseout();
    });
}

getcloseoutList();

function populatecloseout() {
    $('.loaderwrapper').fadeOut();
   
    var s = "";
    $.each(closeouts, function (i, j) {
        var status = "";
        if ( $.inArray("TL", user.access) !== -1 ) {
            status = j.tlapproval;
        }
        if ( $.inArray("SO", user.access) !== -1 ) {
            status = j.soapproval;
        }
        s += '<tr><td>' + j.bmemo + '</td><td>' + j.refcode + '</td><td>' + status +
            '</td><td><a href="#" onclick="loadbatchCloseOut(\'' + 
            j.refcode + '\')" class="btn btn-orange">Review</a></td></tr>';
    });
    $('#tbcloseout>tbody').html(s);
    tbcloseout = null;
    tbcloseout = $('#tbcloseout').DataTable({ responsive: true });

    $('.tabs [data-tab="tab-1"]').click(function () {
        $('.tabs [data-tab="tab-2"]').addClass('hidden');
        $('.bbs-title').html('');
    });


}

function loadbatchCloseOut(refcode) {
    $('.bbs-ref').html(refcode);
    $('.bbs-title').html(refcode);
    $('.loaderwrapper').fadeIn();
    var current_inv = $.grep(investments, function (element, index) {
        return element.refcode === refcode;
    })[0];
    var closeout = $.grep(closeouts, function (element, index) {
        return element.refcode === refcode;
    })[0];
    $('#closeoutid').val(closeout.id);
    $('#close_comment').html(closeout.comment);
    $('.c_tl').html(closeout.tlname);
    $('.c_tlcomment').html(closeout.tlcomments);
    $('.c_tl_date').html(closeout.tldate);
    $('.c_so').html(closeout.soname);
    $('.c_so_date').html(closeout.sodate);
    $('.c_socomment').html(closeout.socomments);
    $('.bbs-title').html(current_inv.title);
    $('#mod_comment').html(closeout.modmemo);
    $('#advert_comment').html(closeout.advert);
    $('#BER_comment').html(closeout.ber);
    $('#BEP_comment').html(closeout.bep);
    $('#proc_budget_comment').html(closeout.procBudget);
    $('#concept_comment').html(closeout.concept);
    $('#proposal_comment').html(closeout.proposal);
    $('#COI_comment').html(closeout.coi);
    $('#contract_comment').html(closeout.contract);
    $('#DDCA_comment').html(closeout.ddca);
    $('#proc_other_comment').html(closeout.procOther);
    $('#logframe_comment').html(closeout.logframe);
    $('#perf_budget_comment').html(closeout.perfBudget);
    $('#risk_comment').html(closeout.risk);
    $('#quarterly_comment').html(closeout.quarterly);
    $('#investee_report_comment').html(closeout.investeeReport);
    $('#monthly_report_comment').html(closeout.monthly);
    $('#perf_other_comment').html(closeout.perfOther);
    $('#fin_pay_comment').html(closeout.finPay);
    $('#mne_comment').html(closeout.mne);

    var commands = [];
    var batchExecutor = new RestBatchExecutor(
        _spPageContextInfo.webAbsoluteUrl, 
        { 'X-RequestDigest': $('#__REQUESTDIGEST').val()});
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Business Case Memo\')/items?$select=Id,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName&$expand=AttachmentFiles&$filter=Title eq \'' + current_inv.refcode + '\'';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'BMemoAttach' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Decision Memo\')/items?$select=Id,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName&$expand=AttachmentFiles&$filter=Ref_x002e_Code eq \'' + current_inv.refcode + '\'';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'DMemoAttach' });   
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Modification Memo\')/items?$select=Id,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName&$expand=AttachmentFiles&$filter=DMemoId eq ' + current_inv.id + ' &$orderby=Title desc&$Top=1';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'ModMemoAttach' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Performance Management\')/items?$select=Id,FileLeafRef,EncodedAbsUrl,Ref_x002e__x0020_Code,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPerformance' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Procurement\')/items?$select=Id,FileLeafRef,EncodedAbsUrl,Ref_x002e__x0020_Code,Category&$filter=Ref_x002e__x0020_Code eq \'' + current_inv.refcode + '\'';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getProcurement' });

    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });
            if ( command[0].title === 'BMemoAttach' ) {
                BMemoAttach(v.result.result.value);
            } else if ( command[0].title === 'DMemoAttach' ) {
                DMemoAttach(v.result.result.value);
            } else if ( command[0].title === 'ModMemoAttach' ) {
                ModMemoAttach(v.result.result.value);
            } else if ( command[0].title === 'getProcurement' ) {
                getProcurement(v.result.result.value);
            } else if ( command[0].title === 'getPerformance' ) {
                getPerformance(v.result.result.value);
            }  
        });
    }).fail(function (err) {
        onError(err);
    });
}

function BMemoAttach(d) {
    $(".b_attachments").html('');
    $.each(d, function (i, j) {
        $(".b_attachments").append(getDelAttachmentLinks('Business Case Memo', j));
    });
}

function DMemoAttach(d) {
    $(".d_attachments").html('');
    $.each(d, function (i, j) {
        $(".d_attachments").append(getDelAttachmentLinks('Decision Memo', j));
    });
}

function ModMemoAttach(d) {
    $(".m_attachments").html('');
    $.each(d, function (i, j) {
        $(".m_attachments").append(getDelAttachmentLinks('Modification Memo',j));
    });
}

function getProcurement(d) {
    $.each(d, function (i, j) {
        var link = '<a target="_blank" href=\'' + j.EncodedAbsUrl + 
            '\'><span class="fa fa-paperclip"></span>' + j.FileLeafRef + 
            '</a>&nbsp;&nbsp;<i class="fa fa-trash text-danger" style="cursor:pointer" data-id="' + 
            j.Id + '"></i>';
        setLink(j.Category, link);
    });
    $('.tabs [data-tab="tab-2"]').removeClass('hidden').trigger('click');
    $('.loaderwrapper').fadeOut();
}

function getPerformance(d) {
    $.each(d, function (i, j) {
        var link = '<a target="_blank" href=\'' + j.EncodedAbsUrl + 
            '\'><span class="fa fa-paperclip"></span>' + j.FileLeafRef + 
            ' </a>&nbsp;&nbsp;<i class="fa fa-trash text-danger" style="cursor:pointer" data-id="' + 
            j.Id + '"></i>';
        setPLink(j.Category, link);
    });
}
function saveRequestAddCloseOut() {
    if ( $('#c_comment').val() === '' ) {
        swal("Error", "Provide the additional information you require in the comment box", "error");
        $('#c_comment').focus();
    }
    swal({
        title: 'Are you sure?',
        text: 'Request Additional information?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        requestAddCloseOut();
    });
}

function requestAddCloseOut() {

    var item = {
        __metadata: { type: 'SP.Data.CloseOutListItem' },
        Approval_x0020_Status: 'More Info'
    };

    if ( $.inArray("TL", user.access) !== -1 ) {
        item.TL_x0020_Comments = $('#c_comment').val();
        item.TLId = _spPageContextInfo.userId;
        item.TL_x0020_Date = moment().toISOString();
        item.TL_x0020_Approval = "Pending";
    }
    if ( $.inArray("SO", user.access) !== -1 ) {
        item.SO_x0020_Comments = $('#c_comment').val();
        item.SOId = _spPageContextInfo.userId;
        item.SO_x0020_Date = moment().toISOString();
        item.SO_x0020_Approval = "Pending";
    }
    updateJson(psitelst + '(\'CloseOut\')/items(' + $("#closeoutid").val() + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "Additional information request successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadTasks(); }
      });
    }
}

function saveApproveCloseOut() {
    if ( $('#c_comment').val() === '' ) {
        swal("Error", "Provide a comment in the comment box", "error");
        $('#c_comment').focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'You want to Approve the CloseOut?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        submitApproveCloseOut();
    });
}

function submitApproveCloseOut() {
    if ( user.access.length === 0 ) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }

    var item = {
        __metadata: { type: 'SP.Data.CloseOutListItem' },
    };

    if ( $.inArray("TL", user.access) !== -1 ) {
        item.TL_x0020_Comments = $('#c_comment').val();
        item.TL_x0020_Approval = "Approved";
        item.Approval_x0020_Status = "Pending";
        item.TLId = _spPageContextInfo.userId;
        item.TL_x0020_Date = moment().toISOString();
    }

    if ($.inArray("SO", user.access) !== -1) {
        item.SO_x0020_Comments = $('#c_comment').val();
        item.SO_x0020_Approval = "Approved";
        item.SOId = _spPageContextInfo.userId;
        item.Approval_x0020_Status = "Approved";
        item.SO_x0020_Date = moment().toISOString();
    }

    updateJson(psitelst + '(\'CloseOut\')/items(' + $("#closeoutid").val() + ')', item, success);
    function success(data) {
        swal({
            title: "Success!",
            text: "CloseOut approved successfully",
            type: "success",
        },
        function (isConfirm) {
            if (isConfirm) { loadTasks(); }
        });
    }
}

