
function loadbatch() {
    var commands = [];
    var batchExecutor = new RestBatchExecutor(
            _spPageContextInfo.webAbsoluteUrl, {
            'X-RequestDigest': $('#__REQUESTDIGEST').val() 
        });
    var batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Approvers List\')/items?$select=Title&$filter=UserId eq ' + 
        _spPageContextInfo.userId;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApproverlst' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(7)/users'; //49
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getIMs' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(8)/users';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getUsers' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Focal Person\')/items?$select=User/Id,User/Title&$expand=User';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getFocals' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = mydrafturl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = draftdecmemourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDraftDecMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'TR Locations\')/items?$select=Title';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getLocations' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = memourl;
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Leave Requests\')/items?$filter=(Status eq \'Approved\') and (AuthorId eq ' +
         _spPageContextInfo.userId + ')';
    batchRequest.headers = { accept: 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApprovedLeave' });

    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });
            if (command[0].title === 'getApproverlst') {
                getApproverlst(v.result.result.value);
            } else if (command[0].title === 'getIMs') {
                getIMs(v.result.result.value);
            } else if (command[0].title === 'getUsers') {
                getUsers(v.result.result.value);
            } else if (command[0].title === 'getMyDraftMemos') {
                getMyDraftMemos(v.result.result.value);
            } else if (command[0].title === 'getDraftDecMemos') {
                getDraftDecMemos(v.result.result.value);
            } else if (command[0].title === 'getFocals') {
                getFocals(v.result.result.value);
            } else if (command[0].title === 'getLocations') {
                getLocations(v.result.result.value);
            } else if (command[0].title === 'getAprInvestments') {
                getAprInvestments(v.result.result.value);
            } else if (command[0].title === 'getApprovedLeave') {
                getApprovedLeave(v.result.result.value);
            }
        });
    }).fail(function (err) {
        onError(err);
    });
}

function getApproverlst(d) {
    $.each(d, function (i, j) {
        if (j.Title) {
            approver = true;
            user.access.push(j.Title);
        }
    });
    if (approver) {
        $('.menu-tasks').removeClass('hidden');
    }
    else {
        $('.menu-tasks').addClass('hidden');
    }
}

function getMyDraftMemos(d) {
    draftmemos = [];
    $.each(d, function (i, j) {
        var soid = null, tlid = null, tlname = "", soname = "", tldate = "", sodate = "";

        if (j.SO) {
            soid = j.SO.Id;
            soname = j.SO.Title;
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }

        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TL.Title;
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
        }

        draftmemos.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            status: j.Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            so: soname,
            so_id: soid,
            tl: tlname,
            tl_id: tlid,
            tl_date: tldate,
            so_date: sodate,
            so_approval: j.SO_x0020_Approval,
            tl_approval: j.TL_x0020_Approval
        });
    });
    loadDraftBMemos();
}

function getDraftDecMemos(d) {
    draftdecmemos = [];
    $.each(d, function (i, j) {        
        draftdecmemos.push({
            refcode: j.Ref_x002e_Code,
            title: j.Title,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            background: j.Background,
            state: j.Federal_x0020_State,
            district: j.District,
            proposal: j.Source_x0020_of_x0020_Proposal,
            activities: j.Key_x0020_Activities,
            email: j.Email,
            phone: j.Phone,
            address: j.Address,
            valuemoney: j.Value_x0020_for_x0020_money,
            risk: j.Risk_x0020_Summary,
            sustainability: j.Sustainability,
            conflict: j.Conflict_x0020_Sensitivity,
            status: j.Status,
            output: j.Output,
            output2: j.Output2,
            output3: j.Output3,
            startdate: moment(j.Start_x0020_Date).format('DD/MM/YYYY'),
            enddate: moment(j.End_x0020_Date).format('DD/MM/YYYY'),
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            tl_approval: j.TL_x0020_Approval,
            tl_comment: j.TL_x0020_Comments,
            so_approval: j.SO_x0020_Approval,
            so_comment: j.SO_x0020_Comments,
            proposed_action:j.Proposed_x0020_Action,
            strategy_link: j.Strategy_x0020_Link,
            prob_desc: j.Problem_x0020_Description,
            coordination: j.Coordination,
            project_assets: j.Project_x0020_Assets,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            comments: j.Comments
        });
    });
    loadDraftDecMemos();
}

function getUsers(d) {
    $.each(d, function (i, j) {
        users.push({ name: j.Title, id: j.Id });
    });
}

function getFocals(d) {
    $.each(d, function (i, j) {
        focals.push({ name: j.User.Title, id: j.User.Id });
    });
}

function getIMs(d) {
    ims = [];
    $.each(d, function (i, j) {
        ims.push({ id: j.Id, name: j.Title });
    });
    ims = ims.sort(function(a, b){
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
    }); 
}

function getLocations(d) {
    locations = [];
    $.each(d, function (i, j) {
        locations.push(j.Title);
    });
}

function getAprInvestments(d) {
    investments = [];
    $.each(d, function (i, j) {
        var tlname = "", soname = "", sodate="", tldate="",tlid =null, soid=null;
        if (j.SO) {
            soid = j.SO.Id;
            soname = j.SO.Title;
        }
        if (j.TL) {
            tlid = j.TL.Id;
            tlname = j.TL.Title;
        }
        if(j.SO_x0020_Date){
            sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
        }
        if(j.TL_x0020_Date){
            tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
        }
        investments.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status: j.Approval_x0020_Status,
            attachments: getAttachmentLinks(j.AttachmentFiles),
            socomments: j.SO_x0020_Comments,
            tlcomments: j.TL_x0020_Comments,
            so_id: soid,
            tl_id: tlid,
            so: soname,
            tl: tlname,
            tl_date: tldate,
            so_date: sodate,
            tl_approval: j.TL_x0020_Approval,
            so_approval: j.SO_x0020_Approval
        });

    });
    loadHome();
}

function getApprovedLeave(d) {
    $.each(d, function (i, j) {
        var app_days = j.Dates.split(', ');
        var app_day = null;
        $(app_days).each(function (idx, val) {
            app_day = moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD');
            approved_leave_days.push({
                id: app_day,
                title: 'Leave',
                allDay: true,
                start: app_day,
                className: 'hidden'
            });
        });
    });
}

$(document).ready(function () {
    $('br').remove();
    $('.menu-home').click(function () {
        loadHome();
    });
    $('.menu-forms').click(function () {
        loadForms();
    });
    $('.menu-tasks').click(function () {
        loadTasks();
    });
    $('.menu-pending').click(function () {
        loadPending();
    });
    $('.menu-doc-colab').click(function () {
        loadDocs();
    });

    $('.menu-reports').click(function () {
        loadReports();
    });

    $('nav ul li').click(function () {
        changeActiveMenu($(this));
    });
    loadbatch();
    setTimeout(function(){renewDigest();},177000);
});