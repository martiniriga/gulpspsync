﻿var modmemo = {};
function getModMemoList() {
    modmemos = [];
    $('.loaderwrapper').fadeIn();
    var dapurl = 'lists/GetByTitle(\'Modification Memo\')/items?$select=*,BMemo/Title0,AttachmentFiles&$expand=AttachmentFiles,BMemo&$filter=Approval_x0020_Status eq \'Pending\'';
    
    if ( $.inArray("TL", user.access) !== -1 ) {
        dapurl += " and TL_x0020_Approval eq 'Pending'";
    }
    
    if ( $.inArray("SO", user.access) !== -1 ) {
        dapurl += " and SO_x0020_Approval eq 'Pending' and TL_x0020_Approval eq 'Approved'";
    }

    RestCalls(dapurl, function (d) {
        $.each(d.results, function (i, j) {
            var attachments = "", soid = null, tlid = null, tlname = "", soname = "", tldate = "", sodate = "", tlapproval = j.TL_x0020_Approval || "", soapproval = j.SO_x0020_Approval || "";
            if ( j.SO ) {
                soid = j.SO.Id;
                soname = j.SO.Title;
                sodate = moment(j.SO_x0020_Date).format('DD/MM/YYYY');
            }
            if ( j.Attachments ) {
                attachments = getAttachmentLinks(j.AttachmentFiles.results);
            }
            if ( j.TL ) {
                tlid = j.TL.Id;
                tlname = j.TL.Title;
                tldate = moment(j.TL_x0020_Date).format('DD/MM/YYYY');
            }
            modmemos.push({
                id: j.Id, 
                bmemoId: j.BMemoId, 
                enddate: moment(j.End_x0020_Date).format('DD/MM/YYYY'), 
                extension: j.Extension, title: j.BMemo.Title0,
                budget_increase: j.Budget_x0020_Increase, 
                total_value: j.Total_x0020_Value, 
                tl_approval: j.TL_x0020_Approval, 
                so_approval: j.SO_x0020_Approval,
                disbursed_to_date: j.Disbursed_x0020_To_x0020_Date, 
                disbus_percent: j.Disbursement_x0020_Percent,
                ip_reports: j.IP_x0020_Reports, 
                investment_performance: j.Investment_x0020_Performance,
                status:j.Approval_x0020_Status,
                justification: j.Justification, 
                rec: j.Recommendation, 
                attachments: attachments, 
                email: j.Email,
                phone: j.Phone, 
                address: j.Address, 
                editable: j.Editable, 
                background: j.Background, 
                soid: soid, 
                soname: soname,
                sodate: sodate, 
                tlid: tlid, 
                tlname: tlname, 
                tldate: tldate, 
                socomments: j.SO_x0020_Comments, 
                tlcomments: j.TL_x0020_Comments
            });
        });
        populateModMemo();
    });
}

getModMemoList();

function populateModMemo() {
    $('.loaderwrapper').fadeOut();
    var s = "";
    $.each(modmemos, function (i, j) {
        var status = "";
        if ( $.inArray("TL", user.access) !== -1 ) {
            status = j.tl_approval;
        }
        if ( $.inArray("SO", user.access) !== -1 ) {
            status = j.so_approval;
        }
        s += '<tr><td>' + j.title + '</td><td>' + j.enddate + '</td><td>' + j.extension + '</td><td>' + j.budget_increase + '</td><td>' + j.total_value + '</td><td>' + j.disbursed_to_date + '</td><td>' + status + '</td><td><a href="#" data-id="' + j.id + '" class="btn btn-orange">Review</a></td></tr>';
    });
    $('#tbmodmemo>tbody').html(s);
    tbmodmemo = null;
    tbmodmemo = $('#tbmodmemo').DataTable({ responsive: true });
    $('.date').datetimepicker({ format: 'DD/MM/YYYY' });
    $('body').on('click', '#tbmodmemo a', function () {
        var id = $(this).data('id');
        modmemo = getItemFromArrayById(modmemos, id)[0];
        $('#bbs-title').text(modmemo.title);
        $('#m_enddate').val(modmemo.enddate);
        $('#m_phone').val(modmemo.phone);
        $('#m_email').val(modmemo.email);
        $('#m_address').val(modmemo.address);
        $('#extension').val(modmemo.extension);
        $('#budget_increase').val(addCommas(modmemo.budget_increase));
        $('#total_value').val(addCommas(modmemo.total_value));
        $('#disbursed').val(addCommas(modmemo.disbursed_to_date));
        $('#disbus_percent').val(modmemo.disbus_percent);
        $('#ip_reports').val(modmemo.ip_reports);
        $('#m_background').val(modmemo.background);
        $('#analysis').val(modmemo.investment_performance);
        $('#m_justification').val(modmemo.justification);
        $('#m_recommend').val(modmemo.rec);
        $('#attachments').html(modmemo.attachments);

        if ( $.inArray("TL", user.access) !== -1 ) {
            $('#comment').val(modmemo.tlcomments);
        }

        if ( $.inArray("SO", user.access) !== -1) {
            $('#comment').val(modmemo.socomments);
        }

        $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click');
        $('.bbs_case_table textarea').each(function () {
            $(this).height($(this).prop('scrollHeight'));
        });

        if ( modmemo.status === 'Approved' ) {
            $('.bbs_case_table button.btn').hide();
        }
    });
}

function submitApproveModificationMemo() {
    if ( user.access.length === 0 ) {
        swal( 'Error', 'Kindly ask the administrator to set up your user access rights', 'error' );
        return;
    }
    var item = {
        __metadata: { type: 'SP.Data.Modification_x0020_MemoListItem' },
        End_x0020_Date: moment($('#m_enddate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
        Email: $("#m_email").val(), 
        Phone: $('#m_phone').val(),
        Address: $("#m_address").val(), 
        Extension : $('#extension').val(),
        Background: $('#m_background').val(), 
        Budget_x0020_Increase: $("#budget_increase").val(),
        Total_x0020_Value : $('#total_value').val(), 
        Disbursed_x0020_To_x0020_Date: $("#disbursed").val(),
        Disbursement_x0020_Percent: $("#disbus_percent").val(), 
        IP_x0020_Reports: $('#ip_reports').val(),
        Investment_x0020_Performance: $('#analysis').val(),
        Justification: $('#m_justification').val(),
        Recommendation: $('#m_recommend').val()
    };

    item = moreinfo( item, 'Approved' );

    updateJson(psitelst + '(\'Modification Memo\')/items(' + modmemo.id + ')',item,success);

    function success(data) {
        swal({
            title: "Success!",
            text: "Modification Memo approved successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadModMemoTasks(); }
      });
    }
 
}

function submitRejectModificationMemo() {
    var item = { __metadata: { type: 'SP.Data.Modification_x0020_MemoListItem' } };

    if ( user.access.length === 0 ) {
        swal('Error', 'Kindly ask the administrator to set up your user access rights', 'error');
        return;
    }

    item = moreinfo(item, 'Rejected');

    updateJson( psitelst + '(\'Modification Memo\')/items(' + modmemo.id + ')', item, success );
    function success(data) {
        swal({
            title: "Success!",
            text: "Modification Memo rejected successfully",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadModMemoTasks(); }
      });
    }
}

function saveRequestAddModificationMemo() {
    if ( $('#comment').val() === '' ) {
        swal("Error", "Kindly provide in the comment box the additional info you require for the Modification memo", "error");
        $("#comment").focus();
        return;
    }

    swal({
        title: 'Are you sure?',
        text: 'Request Additional information?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        requestAddModificationMemo();
    });
}

function requestAddModificationMemo() {
    var item = {
        __metadata: { type: 'SP.Data.Modification_x0020_MemoListItem' },
        End_x0020_Date: moment($('#m_enddate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
        Email: $("#m_email").val(), Phone: $('#m_phone').val(),
        Address: $("#m_address").val(), Extension: $('#extension').val(),
        Background: $('#m_background').val(), Budget_x0020_Increase: $("#budget_increase").val(),
        Total_x0020_Value: $('#total_value').val(), Disbursed_x0020_To_x0020_Date: $("#disbursed").val(),
        Disbursement_x0020_Percent: $("#disbus_percent").val(), IP_x0020_Reports: $('#ip_reports').val(),
        Investment_x0020_Performance: $('#analysis').val(), Justification: $('#m_justification').val(),
        Recommendation: $('#m_recommend').val()
    };

    item = moreinfo(item);
    updateJson( psitelst + '(\'Modification Memo\')/items(' + modmemo.id + ')', item, success );

    function success(data) {
        swal({
            title: "Success!",
            text: "Additional information request successful",
            type: "success",
        },
      function (isConfirm) {
          if (isConfirm) { loadModMemoTasks(); }
      });
    }

}

function saveRejectModificationMemo() {
    if ($('#comment').val() === '') {
        swal("Error", "Kindly provide a reason in the comment box as to why the Modification memo is being rejected", "error");
        $("#comment").focus();
        return;
    }
    swal({
        title: 'Are you sure?',
        text: 'You are about to reject the Modification Memo.',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        submitRejectModificationMemo();
    });
}

function saveApproveModificationMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You are about to Approve the Modification Memo.',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function () {
        submitApproveModificationMemo();
    });
}
