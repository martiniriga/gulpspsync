function loadComplianceReport() {
    RestCalls("lists/GetByTitle('Track')/items?$select=" +
        "Title,RefCode,State,District,OData__x0049_M1/Title,OData__x0049_M2/Title,Advert,Bid_x0020_Evaluation_x0020_Repor" +
        "&$expand=OData__x0049_M1,OData__x0049_M2",
        function (d) {
            $('.loaderwrapper').fadeOut();
            var row = '';
            $.each(d.results, function (i, j) {
                var im1 = "", im2 = "";
                if (j.OData__x0049_M1) {
                    im1 = j.OData__x0049_M1.Title;
                }
                if (j.OData__x0049_M2) {
                    im2 = j.OData__x0049_M2.Title;
                }
                var advert = j.Advert || "False", ber = j.Bid_x0020_Evaluation_x0020_Repor || "False";
                var bep = j.BidEvaluationPanel || "False", procBudget = j.ProcurementBudget || "False";
                var coi = j.COI || "False", contract = j.Contract || "False", ddca = j.DDCA || "False";
                var logframe = j.LogFrame || "False", perf = j.PerformanceBudget || "False";
                var qr = j.QuarterlyReport || "False", concept = j.CallForConcept || "False";
                var proposal = j.CallForProposal || "False";
                var IR = j.InvesteeReport || "False", MR = j.MonthlyReport || "False";
                row += '<tr><td>' + j.RefCode + '</td><td>' + j.Title + '</td><td>' + j.State + '</td><td>' +
                    j.District + '</td><td>' + im1 + '</td><td>' + im2 + '</td><td>' +
                    advert + '</td><td>' + ber + '</td><td>' +
                    bep + '</td><td>' + procBudget + '</td><td>' +
                    concept + '</td><td>' + proposal + '</td><td>' +
                    coi + '</td><td>' + contract + '</td><td>' +
                    ddca + '</td><td>' + logframe + '</td><td>' +
                    perf + '</td><td>' + qr + '</td><td>' +
                    IR + '</td><td>' + MR + '</td></tr>';
            });
            $('#tbcompliance>tbody').html(row);
            $('#tbcompliance').DataTable({
                dom: 'Bfrtip',
                buttons: ['excel', 'print', 'colvis'],
                columnDefs: [{
                    targets: [9, 10, 11, 12, 13, 14, 15, 16],
                    visible: false,
                }],
                initComplete: function () {
                    this.api().column(0).every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>').appendTo($(column.footer()).empty()).on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                    this.api().column(2).every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty()).on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                    this.api().column(4).every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty()).on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                    this.api().column(5).every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty()).on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                column.search(val ? '^' + val + '$' : '', true, false).draw();
                            });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                },
            });
        });
}
loadComplianceReport();