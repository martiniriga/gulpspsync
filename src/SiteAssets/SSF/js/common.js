function loadHome() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('home_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'home.html', function (response, status, xhr) {
    cache('home_html', response);
    loadTabs();
    getMemoCards();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('home_html'));
  //    processHome();
  //}
}

function loadForms() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('forms_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'forms.html', function (response, status, xhr) {
    cache('forms_html', response);
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('forms_html'));
  //    processForms();
  //}
}

function loadPending() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('forms_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'pending.html', function (response, status, xhr) {
    // cache('pending_html', response);

  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('forms_html'));
  //    processForms();
  //}
}

function loadTasks() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('tasks_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'tasks.html', function (response, status, xhr) {
    cache('tasks_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('tasks_html'));
  //    processTasks();
  //}
}

function loadDocs() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('docs_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'docs.html', function (response, status, xhr) {
    cache('docs_html', response);
    loadTabs();
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('docs_html'));
  //    processModForm();
  //}
}

function loadReports() {
  $('.loaderwrapper').fadeIn();
  //if(getCache('report_html') === null || !cache_active){
  $('div.content-area.container').empty().load(codepath + 'report.html', function (response, status, xhr) {
    cache('report_html', response);
  });
  //}
  //else{
  //    $('div.content-area.container').empty().append(getCache('report_html'));
  //}
}

function getReference() {
  var c_pillar_val = $('#pillar').val();
  var c_state_val = $('#state').val();
  if (c_state_val && c_pillar_val) {
    RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=Id&$filter=Federal_x0020_State eq \'' +
      encodeURIComponent(c_state_val) + '\'', function (d) {
        var id = pad((d.results.length + 1) + '', 3);
        var ct = 0;
        $.each(d.results, function (i, j) {
          if (j.Pillar === c_pillar_val) {
            ct++;
          }
        });
        ct++;
        ct = pad(ct + '', 2);
        var ref = 'SSF-' + $('#state option:selected').attr('data-initial') + '-' +
          id + '-' + $('#pillar option:selected').attr('data-initial') + ct;
        $('#refno').text(ref);
        $('#refno-h').val(ref);
      });
  }
}

function editMemo() {
  $('#tab-2 select').prop('disabled', false).trigger('chosen:updated');
  $('.bbs_case_table .read').hide();
  $('.bbs_case_table .edit').show();
  $('#tab-2 input,#tab-2 textarea ').prop('disabled', false);
}

function prepDecModal() {

  $('.date').datetimepicker({ format: 'DD/MM/YYYY' });

  $('#datetimepicker1').on('dp.change', function (e) {
    $('#datetimepicker2').data('DateTimePicker').minDate(e.date);
    $('#decision-memo-form').formValidation('revalidateField', 'start_date');
  });
  $('#datetimepicker2').on('dp.change', function (e) {
    $('#decision-memo-form').formValidation('revalidateField', 'end_date');
  });

  $('.btn-next-second-form').click(function () {
    $('.first-form,.third-form, .fourth-form').hide();
    $('.second-form').fadeIn();
  });

  $('.btn-prev-first-form').click(function () {
    $('.second-form, .third-form, .fourth-form').hide();
    $('.first-form').fadeIn();
  });

  $('.btn-next-third-form').click(function () {
    $('.first-form,.second-form, .fourth-form').hide();
    $('.third-form').fadeIn();
  });

  $('.btn-prev-second-form').click(function () {
    $('.first-form,.third-form, .fourth-form').hide();
    $('.second-form').fadeIn();
  });


  $('.btn-next-fourth-form').click(function () {
    $('.first-form,.second-form,.third-form').hide();
    $('.fourth-form').fadeIn();
  });

  $('.btn-prev-third-form').click(function () {
    $('.second-form,.third-form, .fourth-form').hide();
    $('.third-form').fadeIn();
  });

  $('select').chosen();

  $('#fundceiling').bind('textchange', function (event, previousText) {
    const value = removeCommas($(this).val());
    $(this).val(addCommas(value));
  });


  $('input.upload').on('change', function () {
    const path = $(this).val(), filename = path.substr(path.lastIndexOf('\\') + 1);
    $(this).closest('.input-group').find('.inputFiles').val(filename);
  });

  $('#decision-memo-form').on('hidden.bs.modal', function () {
    $('#decision-memo-form input,#decision-memo-form textarea').val('');
  });

  $('#decision-memo-form').modal('show');
}

function hideActionButtons() {
  var show = false;
  if ($.inArray('TL', user.access) !== -1 ||
    $.inArray('SO', user.access) !== -1 ||
    current_inv.im1_id === _spPageContextInfo.userId ||
    current_inv.im2_id === _spPageContextInfo.userId) {
    show = true;
  }
  if (!show) {
    $('.btn-primary').hide();
  }
}


function moreinfo(item, status) {
  if (status === 'More Info') {
    item.Status = status;
    status = 'Pending';
  }
  if (status === 'Rejected') {
    item.Approval_x0020_Status = status;
  }
  if (status === 'Approved' && $.inArray("SO", user.access) !== -1) {
    item.Approval_x0020_Status = status;
  }
  if ($.inArray("TL", user.access) !== -1) {
    item.TL_x0020_Comments = $('#comment').val();
    item.TLId = _spPageContextInfo.userId;
    item.TL_x0020_Date = moment().toISOString();
    item.TL_x0020_Approval = status;
  }
  if ($.inArray("SO", user.access) !== -1) {
    item.SO_x0020_Comments = $('#comment').val();
    item.SOId = _spPageContextInfo.userId;
    item.SO_x0020_Date = moment().toISOString();
    item.SO_x0020_Approval = status;
  }
  return item;
}

function successConfirm(msg, postFn) {
  swal({
    title: "Success!",
    text: msg,
    type: "success",
  },
    function (isConfirm) {
      if (isConfirm) {
        postFn();
        swal.close();
      }
    });
}

