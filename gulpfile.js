﻿var gulp = require('gulp');
var notify = require('gulp-notify');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var sp = require('gulp-spsync');
var watch = require('gulp-wawat');
var concat = require('gulp-concat');
var del = require('del');
var jsfilepath ="src/SiteAssets/SSF/js/";
var jsfiles = [
      'src/SiteAssets/SSF/js/varinits.js',
      'src/SiteAssets/SSF/js/helpers.js',
      'src/SiteAssets/SSF/js/common.js',
      'src/SiteAssets/SSF/js/home.js',
      'src/SiteAssets/SSF/js/load.js',
      'src/SiteAssets/SSF/js/forms.js'];
      //'src/SiteAssets/SSF/js/*.js'];

var settings = {
    client_id: "cad71a58-4b9f-4fc7-ab4c-d507ef275a74",
    client_secret: "ELNJet9B52AO1Qh2vfal3bSnUG/2RVi1biZ1S8Jw3l0=",
    realm: "",
    site: "https://masterpiecenet.sharepoint.com/sites/mpf",
    verbose: "true"
};
gulp.task('default', function () { 
   return gulp.src('src/**/*.*')
        .pipe(watch('src/**/*.*'))
        .pipe(sp(settings))
        .pipe(gulp.dest('build'))
});

gulp.task('clean', function(){
    return del('src/SiteAssets/SSF/js/app.js', {force:true});
});

gulp.task('src',['clean'], function () {
    return gulp.src(jsfiles)     
      .pipe(concat('app.js'))
      .pipe(gulp.dest('src/SiteAssets/SSF/js'));
});

gulp.task('jscs', function () {
    gulp.src('src/SiteAssets/SSF/js/common.js')
        .pipe(jscs())
        .pipe(jscs.reporter())
        .pipe(notify({
            title: 'JSCS',
            message: 'JSCS Passed. Let it fly!'
        }))
});

gulp.task('lint', function () {
    gulp.src('src/SiteAssets/SSF/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
        // .pipe(notify({
        //     title: 'JSHint',
        //     message: 'JSHint Passed. Let it fly!',
        // }))
});

gulp.task('build', ['jscs', 'lint'], function () {
    gulp.src('/')
        //pipe through other tasks such as sass or coffee compile tasks
        .pipe(notify({
            title: 'Task Builder',
            message: 'Successfully built application'
        }))
});